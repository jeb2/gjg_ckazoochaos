﻿namespace CosmicKazooChaos.API.Listeners.Collectables
{
	public interface ICollectableListener
	{
		#region Messages

		/// <summary>
		/// 	Called when the collectable is collected.
		/// </summary>
		void OnCollected();

		#endregion
	}
}
