﻿using CosmicKazooChaos.Abstract.Enemies.States;
using CosmicKazooChaos.Abstract.StateMachine;
using UnityEngine;

namespace CosmicKazooChaos.API.Listeners.Enemies
{
	public interface IEnemyListener
	{
		void OnStateChange(StateChangeInfo<AbstractEnemyState> changeInfo);

		void OnAnimationEvent(AnimationEvent animationEvent);
	}
}
