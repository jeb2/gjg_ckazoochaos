﻿using CosmicKazooChaos.Abstract.Player.States;
using CosmicKazooChaos.Abstract.StateMachine;
using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.API.Listeners.Player
{
	public interface IPlayerListener
	{
		#region Messages

		/// <summary>
		/// 	Called when the player jumps.
		/// </summary>
		void OnJump();

		/// <summary>
		/// 	Called when the player is damaged.
		/// </summary>
		/// <param name="collision">The collision info.</param>
		void OnDamage(CharacterLocomotorCollisionData collision);

		/// <summary>
		/// 	Called when the state changes.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo);

		/// <summary>
		/// 	Called when we swap to the player.
		/// </summary>
		void OnSwapEnter();

		/// <summary>
		/// 	Called when an animation event is fired.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		void OnAnimationEvent(AnimationEvent animationEvent);

		#endregion
	}
}
