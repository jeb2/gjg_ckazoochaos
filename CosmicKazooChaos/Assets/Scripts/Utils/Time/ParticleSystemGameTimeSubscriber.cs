﻿using UnityEngine;

namespace CosmicKazooChaos.Utils.Time
{
	/// <summary>
	/// 	ParticleSystemGameTimeSubscriber handles the pausing and unpausing of ParticleSystems.
	/// </summary>
	[RequireComponent(typeof(ParticleSystem))]
	public class ParticleSystemGameTimeSubscriber : AbstractGameTimeSubscriber
	{
		private ParticleSystem m_CachedParticleSystem;
		private bool m_Paused;

		/// <summary>
		/// 	Gets the particle system.
		/// </summary>
		/// <value>The particle system.</value>
		public new ParticleSystem particleSystem
		{
			get { return m_CachedParticleSystem ?? (m_CachedParticleSystem = GetComponent<ParticleSystem>()); }
		}

		/// <summary>
		/// 	Called when the game is paused.
		/// </summary>
		protected override void Pause()
		{
			m_Paused = particleSystem.isPaused;
			particleSystem.Pause(true);
		}

		/// <summary>
		/// 	Called when the game is unpaused.
		/// </summary>
		protected override void Unpause()
		{
			if (!m_Paused)
				particleSystem.Play(true);
		}
	}
}
