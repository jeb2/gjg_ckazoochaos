﻿using UnityEngine;

namespace CosmicKazooChaos.Utils.Time
{
	/// <summary>
	/// 	NavMeshAgentGameTimeSubscriber handles the pausing and unpausing of NavMeshAgents.
	/// </summary>
	[RequireComponent(typeof(NavMeshAgent))]
	public class NavMeshAgentGameTimeSubscriber : AbstractGameTimeSubscriber
	{
		private NavMeshAgent m_CachedNavMeshAgent;
		private Vector3 m_Velocity;
		private float m_Speed;

		/// <summary>
		/// 	Gets the nav mesh agent.
		/// </summary>
		/// <value>The nav mesh agent.</value>
		public NavMeshAgent navMeshAgent
		{
			get { return m_CachedNavMeshAgent ?? (m_CachedNavMeshAgent = GetComponent<NavMeshAgent>()); }
		}

		/// <summary>
		/// 	Called when the game is paused.
		/// </summary>
		protected override void Pause()
		{
			m_Velocity = navMeshAgent.velocity;
			m_Speed = navMeshAgent.speed;

			navMeshAgent.velocity = Vector3.zero;
			navMeshAgent.speed = 0.0f;
		}

		/// <summary>
		/// 	Called when the game is unpaused.
		/// </summary>
		protected override void Unpause()
		{
			navMeshAgent.velocity = m_Velocity;
			navMeshAgent.speed = m_Speed;
		}
	}
}
