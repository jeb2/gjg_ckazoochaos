﻿using UnityEngine;

namespace CosmicKazooChaos.Utils.Time
{
	/// <summary>
	/// 	AnimatorGameTimeSubscriber handles the pausing and unpausing of Animators.
	/// </summary>
	[RequireComponent(typeof(Animator))]
	public class AnimatorGameTimeSubscriber : AbstractGameTimeSubscriber
	{
		private Animator m_CachedAnimator;
		private float m_Speed;

		/// <summary>
		/// 	Gets the animator.
		/// </summary>
		/// <value>The animator.</value>
		public Animator animator { get { return m_CachedAnimator ?? (m_CachedAnimator = GetComponent<Animator>()); } }

		/// <summary>
		/// 	Called when the game is paused.
		/// </summary>
		protected override void Pause()
		{
			m_Speed = animator.speed;
			animator.speed = 0.0f;
		}

		/// <summary>
		/// 	Called when the game is unpaused.
		/// </summary>
		protected override void Unpause()
		{
			animator.speed = m_Speed;
		}
	}
}
