﻿using UnityEngine;

namespace CosmicKazooChaos.Utils
{
	// A script to attach to empty objects to ensure they are visible in the editor
	public class ForceDrawGizmo : MonoBehaviour
	{
		private void OnDrawGizmos()
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawSphere(transform.position, 0.5f);
		}
	}
}
