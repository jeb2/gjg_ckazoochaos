﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour 
{
	private float _movementSpeed = 25f;

	private MeshRenderer _meshRenderer;

	//private ShootingScript _shottingScript;

	void Start ()
	{
		//_shottingScript = GetComponentInParent<ShootingScript>();
		_meshRenderer = GetComponent<MeshRenderer>();
	}

	void Update () 
	{
		if (_meshRenderer.isVisible == false)
		{
			//object deactivates itself when outside camera view (scene editor camera too)
			gameObject.SetActive (false);
		}

		//quick movement script to test out
		transform.Translate (Vector3.forward * Time.deltaTime * _movementSpeed);
	}

	void SetDirection (Transform Target)
	{
		transform.LookAt (Target);
	}
}