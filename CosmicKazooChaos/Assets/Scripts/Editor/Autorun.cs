﻿using Hydra.HydraCommon.Editor.Utils;
using UnityEditor;

namespace CosmicKazooChaos.Editor
{
	/// <summary>
	/// 	Autorun allows us to do some automatic maintenance.
	/// </summary>
	[InitializeOnLoad]
	public static class Autorun
	{
		/// <summary>
		/// 	Called when the editor starts, and every time the assembly is recompiled.
		/// </summary>
		static Autorun()
		{
			MaintenanceUtils.RemoveEmptyDirectories();
		}
	}
}
