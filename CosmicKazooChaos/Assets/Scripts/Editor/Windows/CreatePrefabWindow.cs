﻿using CosmicKazooChaos.Concrete.Prefabs;
using Hydra.HydraCommon.Editor.Utils;
using Hydra.HydraCommon.Utils;
using UnityEditor;
using UnityEngine;

namespace CosmicKazooChaos.Editor.Windows
{
	/// <summary>
	/// 	Create prefab window.
	/// </summary>
	public class CreatePrefabWindow : CKCEditorWindow
	{
		public const string TITLE = "Create Prefab";
		public const string PREFAB_EXT = "prefab";
		public const float LABEL_WIDTH = 64.0f;

		private static GUIContent s_Title = new GUIContent(TITLE);
		private static GUIContent s_AssetFieldLabel = new GUIContent("Asset");
		private static GUIContent s_PathFieldLabel = new GUIContent("Path");

		private static GameObject s_SelectedGameObject;
		private static string s_Path = "";

		/// <summary>
		/// 	Shows the window.
		/// </summary>
		[MenuItem(MENU + TITLE)]
		public static void Init()
		{
			CreatePrefabWindow window = GetWindow<CreatePrefabWindow>();
			window.titleContent = s_Title;
		}

		#region Messages

		/// <summary>
		/// 	Called to draw the window contents.
		/// </summary>
		protected override void OnGUI()
		{
			base.OnGUI();

			float oldLabelWidth = EditorGUIUtility.labelWidth;
			EditorGUIUtility.labelWidth = LABEL_WIDTH;

			Rect position = EditorGUILayout.GetControlRect(true);
			s_SelectedGameObject =
				(GameObject)EditorGUI.ObjectField(position, s_AssetFieldLabel, s_SelectedGameObject, typeof(GameObject), true);

			s_Path = HydraEditorLayoutUtils.SaveFileField(s_PathFieldLabel, s_Path, PREFAB_EXT);

			bool oldGuiEnabled = GUI.enabled;
			GUI.enabled = EnableCreateButton();

			if (GUILayout.Button("Create Prefab"))
			{
				GameObject gameObject = new GameObject();
				gameObject.hideFlags = HideFlags.HideAndDontSave;

				TransformAssetInstantiator instantiator = gameObject.AddComponent<TransformAssetInstantiator>();
				instantiator.asset = s_SelectedGameObject.transform;

				string localPath = FileUtil.GetProjectRelativePath(s_Path);
				PrefabUtility.CreatePrefab(localPath, gameObject);

				gameObject = ObjectUtils.SafeDestroy(gameObject);
			}

			EditorGUIUtility.labelWidth = oldLabelWidth;
			GUI.enabled = oldGuiEnabled;
		}

		#endregion

		/// <summary>
		/// 	Returns true if the create prefab button should be enabled.
		/// </summary>
		/// <returns><c>true</c>, if create button should be enabled, <c>false</c> otherwise.</returns>
		private bool EnableCreateButton()
		{
			return (s_SelectedGameObject != null) && !string.IsNullOrEmpty(s_Path);
		}
	}
}
