﻿using UnityEditor;

namespace CosmicKazooChaos.Editor.Windows
{
	/// <summary>
	/// 	CKC editor window is the base class for all CKC editor windows.
	/// </summary>
	public abstract class CKCEditorWindow : EditorWindow
	{
		public const string MENU = "Window/CosmicKazooChaos/";

		/// <summary>
		/// 	Called to draw the window contents.
		/// </summary>
		protected virtual void OnGUI() {}
	}
}
