﻿using UnityEngine;
using System.Collections;
using Hydra.HydraCommon.Abstract;
using CosmicKazooChaos.Abstract.Props;

public class TransitionSystem : HydraMonoBehaviour
{
    public static TransitionSystem transitionSystem;

    private bool m_active = true;
    private bool m_transOut = false;
    private float m_zoomPoint = 0f;

    [SerializeField] private Texture m_CutOut;
    private Rect m_cutOutRect;
    private Rect m_topRect;
    private Rect m_botRect;
    private Rect m_leftRect;
    private Rect m_rightRect;

    private AbstractTeleporter m_Teleporter;
    private CharacterController m_ToTeleport;

    protected override void Awake()
    {
        base.Awake();
        if (transitionSystem == null)
        {
            transitionSystem = this;
            transition();
            DontDestroyOnLoad(gameObject);
        }
        else if(transitionSystem != this)
        {
            Debug.Log("Additional TransitionSystem Detected");
            Destroy(this);
        }
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if (!m_active)
            return;

        if (!m_transOut)
        {
            m_zoomPoint += Time.unscaledDeltaTime * Screen.width / 8;
            if (m_zoomPoint*Screen.width/20 > Screen.width*5f)
            {
                m_active = false;
                //transition(true);
                return;
            }
        }
        else
        {
            if(m_zoomPoint == 0)
            {
                transition();
                Time.timeScale = 1;
                m_Teleporter.Teleport(m_ToTeleport);
                return;
            }
            m_zoomPoint -= Time.unscaledDeltaTime * Screen.width/8;
            if(m_zoomPoint < 0)
            {
                m_zoomPoint = 0f;
            }
        }

        m_cutOutRect = new Rect(Screen.width / 2 - Screen.width / 40 * m_zoomPoint, 
            Screen.height / 2 - Screen.width / 40 * m_zoomPoint, 
            Screen.width / 20 * m_zoomPoint, Screen.width / 20 * m_zoomPoint);

        m_topRect = new Rect(m_cutOutRect.x, 0, m_cutOutRect.width, m_cutOutRect.y);
        m_botRect = new Rect(m_cutOutRect.x, m_cutOutRect.y+m_cutOutRect.height, m_cutOutRect.width, m_topRect.height);

        m_leftRect = new Rect(0, 0, m_cutOutRect.x, Screen.height);
        m_rightRect = new Rect(m_cutOutRect.x+m_cutOutRect.width, 0, m_leftRect.width, Screen.height);


    }

    protected override void OnGUI()
    {
        base.OnGUI();
        if (!m_active)
            return;


        GUI.DrawTexture(m_cutOutRect, m_CutOut);

        GUI.color = Color.black;

        GUI.DrawTexture(m_topRect, Texture2D.whiteTexture);
        GUI.DrawTexture(m_botRect, Texture2D.whiteTexture);

        GUI.DrawTexture(m_leftRect, Texture2D.whiteTexture);
        GUI.DrawTexture(m_rightRect, Texture2D.whiteTexture);

        GUI.color = Color.white;
    }

    /// <summary>
    /// Requires Teleporter to activate at end of transition
    /// </summary>
    /// <param name="Teleporter"></param>
    public void transition(AbstractTeleporter teleporter, CharacterController controller)
    {
        m_active = true;
        m_transOut = true;
        m_ToTeleport = controller;
        m_Teleporter = teleporter;
        m_zoomPoint = (Screen.width * 5f) / (Screen.width / 20);
    }

    /// <summary>
    /// Transitions in.
    /// </summary>
    /// <param name="Teleporter"></param>
    public void transition()
    {
        m_active = true;
        m_transOut = false;
        m_zoomPoint = 0;
    }
}
