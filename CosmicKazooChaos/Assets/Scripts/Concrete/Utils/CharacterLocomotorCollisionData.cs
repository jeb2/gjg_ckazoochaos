﻿using UnityEngine;

namespace CosmicKazooChaos.Concrete.Utils
{
	public struct CharacterLocomotorCollisionData
	{
		#region Members

		private readonly CharacterController m_Controller;
		private readonly Vector3 m_Point;

		#endregion

		#region Properties

		/// <summary>
		/// The controller that hit the collider.
		/// </summary>
		/// <value>The originator controller.</value>
		public CharacterController Controller { get { return m_Controller; } }

		/// <summary>
		/// The point of the collision in world space.
		/// </summary>
		/// <value>The point.</value>
		public Vector3 point { get { return m_Point; } }

		#endregion

		public CharacterLocomotorCollisionData(CharacterController controller, Vector3 point)
		{
			m_Controller = controller;
			m_Point = point;
		}
	}
}
