﻿using System;
using CosmicKazooChaos.Abstract.Menus.States;
using CosmicKazooChaos.Concrete.Configuration.Controls.Mapping;
using CosmicKazooChaos.Concrete.Menus;
using CosmicKazooChaos.Concrete.Menus.DebugGUI;
using CosmicKazooChaos.Concrete.Scene;
using CosmicKazooChaos.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete
{
	/// <summary>
	/// 	Main prefabs.
	/// </summary>
	[Serializable]
	public class MainPrefabs
	{
		[SerializeField] private SceneManager m_SceneManagerPrefab;
		[SerializeField] private DebugGuiComponent m_DebugGuiPrefab;

		public SceneManager sceneManagerPrefab { get { return m_SceneManagerPrefab; } }
		public DebugGuiComponent debugGuiPrefab { get { return m_DebugGuiPrefab; } }
	}

	/// <summary>
	/// 	Main is the top-level class for the CosmicKazooChaos application.
	/// </summary>
	[ExecuteInEditMode]
	public class Main : SingletonHydraMonoBehaviour<Main>
	{
		[SerializeField] private MainPrefabs m_Prefabs;
		[SerializeField] private SceneManager m_SceneManager;
		[SerializeField] private DebugGuiComponent m_DebugGui;
		[SerializeField] private MenuController m_MenuController;
		[SerializeField] private AbstractMenuState m_PauseMenu;

		#region Properties

		/// <summary>
		/// 	Gets the prefab factory.
		/// </summary>
		/// <value>The prefab factory.</value>
		public MainPrefabs prefabs { get { return m_Prefabs ?? (m_Prefabs = new MainPrefabs()); } }

		/// <summary>
		/// 	Gets the scene manager.
		/// </summary>
		/// <value>The scene manager.</value>
		public SceneManager sceneManager { get { return m_SceneManager; } }

		/// <summary>
		/// 	Gets the debug GUI.
		/// </summary>
		/// <value>The debug GUI.</value>
		public DebugGuiComponent debugGui { get { return m_DebugGui; } }

		public MenuController menuController { get { return m_MenuController; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			InstantiateDependencies();
		}

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			if (Application.isPlaying)
				ShowDebugGui(true);
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (Application.isPlaying)
				UpdateDebugGuiVisibility();

			if (InputMapping.pause.GetButtonDown()) {
				menuController.NextMenu(m_PauseMenu);
				LockCursor(!GameTime.paused);
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Instantiates the dependencies.
		/// </summary>
		public void InstantiateDependencies()
		{
			ObjectUtils.LazyInstantiateOrFind(prefabs.sceneManagerPrefab, ref m_SceneManager);
			m_SceneManager.InstantiateDependencies();

			if (Application.isPlaying)
				ObjectUtils.LazyInstantiateOrFind(prefabs.debugGuiPrefab, ref m_DebugGui);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Updates the debug GUI visibility.
		/// </summary>
		private void UpdateDebugGuiVisibility()
		{
			if (InputMapping.toggleDebugUI.GetButtonDown())
			{
				ShowDebugGui(!m_DebugGui.IsVisible());
				LockCursor(false);
			}

			if (Input.GetMouseButtonDown(0))
			{
				if (m_DebugGui.IsVisible())
				{
					Rect debugGUIRect = debugGui.GetGUIRect();
					Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

					if (!debugGUIRect.Contains(mousePosition))
						ShowDebugGui(false);
				} 
				else if(!GameTime.paused)
				{
					LockCursor(true);
				}
			}
		}

		/// <summary>
		/// 	Locks the cursor.
		/// </summary>
		/// <param name="lockState">Locks if true.</param>
		private void LockCursor(bool lockState)
		{
			Cursor.lockState = lockState ? CursorLockMode.Locked : CursorLockMode.None;
			Cursor.visible = !lockState;
			m_SceneManager.playerCamera.orbitEnabled = lockState;
		}

		/// <summary>
		/// 	Show or hide the debug gui.
		/// </summary>
		/// <param name="shown">If set to <c>true</c>, show the debug gui.</param>
		private void ShowDebugGui(bool shown)
		{
			LockCursor(!shown);

			if (shown)
				m_DebugGui.Show();
			else
				m_DebugGui.Hide();
		}

		#endregion
	}
}
