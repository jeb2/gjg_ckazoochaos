﻿using CosmicKazooChaos.Abstract.Player.States;

namespace CosmicKazooChaos.Concrete.Player.States
{
	/// <summary>
	/// 	StandingPlayerState is the player's default state.
	/// </summary>
	public class StandingPlayerState : AbstractStandingState
	{
	}
}
