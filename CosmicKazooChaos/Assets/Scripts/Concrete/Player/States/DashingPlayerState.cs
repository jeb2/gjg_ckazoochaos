﻿using CosmicKazooChaos.Abstract.Player;
using CosmicKazooChaos.Abstract.Player.States;
using CosmicKazooChaos.Concrete.Configuration.Controls.Mapping;
using CosmicKazooChaos.Concrete.HitBoxes;
using CosmicKazooChaos.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Player.States
{
	/// <summary>
	/// 	DashingPlayerState is used while the player is dashing(running). 
	/// </summary>
	public class DashingPlayerState : AbstractDashingState
	{
		[SerializeField] private AbstractSwimmingState m_SwimmingState;
		[SerializeField] private AbstractStandingState m_StandingState;

		[Tweakable("Dash")] [SerializeField] private float m_MaxSpeedMultiplier = 3.0f;
		[Tweakable("Dash")] [SerializeField] private float m_AccelerationMultiplier = 2.0f;
		[Tweakable("Dash")] [SerializeField] private float m_RotateSpeedMultiplier = 0.5f;

		#region Properties

		protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }
		protected override float accelerationMultiplier { get { return m_AccelerationMultiplier; } }
		protected override float rotateSpeedMultiplier { get { return m_RotateSpeedMultiplier; } }

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			HitBox hb = GetHitBox(parent);
			AbstractPlayerController player = GetPlayerController(parent);
			hb.gameObject.SetActive(true);
			// Ignore our own collider so CP does not hit himself. Has to be done every time hb is reactivated
			Physics.IgnoreCollision(player.collider, hb.collider, true);
		}

		/// <summary>
		/// 	Called before another state becomes active.
		/// </summary>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			GetHitBox(parent).gameObject.SetActive(false);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (!player.characterLocomotor.isSoftGrounded && player.isInWater)
			{
				return m_SwimmingState;
			}

			//TODO: Add nicer slowdown
			if (!InputMapping.dashInput.GetButton())
				return m_StandingState;

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			if (InputMapping.jumpInput.GetButtonDown())
				Jump(player);
		}

		#endregion
	}
}