using CosmicKazooChaos.Abstract.Player.States;

namespace CosmicKazooChaos.Concrete.Player.States
{
	/// <summary>
	/// Sliding player state is used when the player is sliding on a slope
	/// </summary>
	public class SlidingPlayerState : AbstractSlidingState {}
}
