﻿using CosmicKazooChaos.Abstract.Player.States;
using CosmicKazooChaos.Concrete.Configuration.Controls.Mapping;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Player.States
{
    /// <summary>
    /// 	MidairPlayerState is used when the player is off the ground.
    /// </summary>
    public class MidairPlayerState : AbstractMidairState
    {
		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			return base.GetNextState(parent);
		}

		public override void OnExit(MonoBehaviour parent)
		{

			base.OnExit(parent);
		}
	}
}
