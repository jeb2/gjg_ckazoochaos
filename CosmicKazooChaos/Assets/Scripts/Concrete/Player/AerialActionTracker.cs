﻿namespace CosmicKazooChaos.Concrete.Player
{
	public class AerialActionTracker
	{
		private bool m_CanAirJump = false;
		private bool m_CanJumpCancel = false;
		private bool m_CanGlideRamp = false;
		private bool m_CanSpinHover = false;
		private float m_GlideTimeElapsed = 0.0f;

		public bool canAirJump { get { return m_CanAirJump; } set { m_CanAirJump = value; } }
		public bool canJumpCancel { get { return m_CanJumpCancel; } set { m_CanJumpCancel = value; } }
		public bool canGlideRamp { get { return m_CanGlideRamp; } set { m_CanGlideRamp = value; } }
		public bool canSpinHover { get { return m_CanSpinHover; } set { m_CanSpinHover = value; } }
		public float glideTimeElapsed { get { return m_GlideTimeElapsed; } set { m_GlideTimeElapsed = value; } }

		public void Reset()
		{
			m_CanAirJump = true;
			m_CanJumpCancel = false;
			m_CanGlideRamp = true;
			m_CanSpinHover = true;
			m_GlideTimeElapsed = 0.0f;
		}

		public void Set(AerialActionTracker other)
		{
			m_CanAirJump = other.canAirJump;
			m_CanJumpCancel = other.canJumpCancel;
			m_GlideTimeElapsed = other.glideTimeElapsed;
			m_CanGlideRamp = other.canGlideRamp;
			m_CanSpinHover = other.canSpinHover;
		}
	}
}
