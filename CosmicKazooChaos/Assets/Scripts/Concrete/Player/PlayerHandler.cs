﻿using CosmicKazooChaos.Abstract.Player;
using CosmicKazooChaos.Abstract.Player.States;
using CosmicKazooChaos.Concrete.Cameras;
using CosmicKazooChaos.Concrete.Configuration.Controls.Mapping;
using CosmicKazooChaos.Concrete.Messaging;
using CosmicKazooChaos.Concrete.Player;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Extensions;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Player
{
	/// <summary>
	/// 	Player handler script.
	/// </summary>
	public class PlayerHandler : SingletonHydraMonoBehaviour<PlayerHandler>
	{
		[SerializeField] private PlayerController m_PlayerPrefab;
		private CameraController m_PlayerCamera;
		private PlayerController m_PlayerInstance;
		
		private bool m_PlayerActive = true;
		
		#region Properties
		
		/// <summary>
		/// 	Gets or sets the player camera.
		/// </summary>
		/// <value>The player camera.</value>
		public CameraController playerCamera { get { return m_PlayerCamera; } set { m_PlayerCamera = value; } }
		
		/// <summary>
		/// 	Gets the player prefab.
		/// </summary>
		/// <value>The anton prefab.</value>
		public PlayerController playerPrefab { get { return m_PlayerPrefab; } }
		
		#endregion
		
		#region Messages
		
		/// <summary>
		/// 	Called when the object is destroyed.
		/// </summary>
		protected override void OnDestroy()
		{
			base.OnDestroy();
			
			m_PlayerInstance = ObjectUtils.SafeDestroyGameObject(m_PlayerInstance);
		}
		
		#endregion
		
		#region Methods
		
		/// <summary>
		/// 	Sets Player active.
		/// </summary>
		/// <returns>Anton.</returns>
		public PlayerController SetPlayerActive()
		{
			return SetPlayerActive(true) as PlayerController;
		}
		
		/// <summary>
		/// 	Sets Anton active.
		/// </summary>
		/// <returns>The new active player.</returns>
		/// <param name="active">If set to <c>true</c> active.</param>
		public AbstractPlayerController SetPlayerActive(bool active)
		{
			AbstractPlayerController old = GetActivePlayer();
			return old;
		}
		
		/// <summary>
		/// 	Gets the active player.
		/// </summary>
		/// <returns>The active player.</returns>
		public AbstractPlayerController GetActivePlayer()
		{
			return GetPlayerInstance();
		}
		
		/// <summary>
		/// Eagerly instantiate Anton and Coolpecker. Used primarily by the DebugGUI.
		/// We have to leave the ontherwise inactive character active until the DebugGUI is finished with it.
		/// </summary>
		public void PreWarm()
		{
			GetPlayerInstance();
		}
		
		#endregion
		
		#region Private Methods
		
		/// <summary>
		/// 	Gets the anton instance.
		/// </summary>
		/// <returns>The anton instance.</returns>
		private PlayerController GetPlayerInstance()
		{
			if (m_PlayerInstance == null)
			{
				m_PlayerInstance = Instantiate(m_PlayerPrefab);
				PostInstantiate(m_PlayerInstance);
			}
			
			return m_PlayerInstance;
		}
		
		/// <summary>
		/// 	Performs some operations on the player after being instantiated.
		/// </summary>
		/// <param name="player">Player.</param>
		private void PostInstantiate(AbstractPlayerController player)
		{
			player.transform.parent = transform.parent;
			player.playerCamera = m_PlayerCamera;
			
			if (m_PlayerCamera.target == null)
				m_PlayerCamera.target = player;
		}
		
		#endregion
	}
}
