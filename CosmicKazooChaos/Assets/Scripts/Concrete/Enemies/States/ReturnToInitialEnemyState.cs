﻿using CosmicKazooChaos.Abstract.Enemies.States;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.States
{
	public class ReturnToInitialEnemyState : AbstractEnemyState
	{
		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			return enemy.stateMachine.initialState;
		}
	}
}
