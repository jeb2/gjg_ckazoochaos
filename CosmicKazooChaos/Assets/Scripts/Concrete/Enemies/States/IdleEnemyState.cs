﻿using CosmicKazooChaos.Abstract.Enemies.States;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.States
{
	public class IdleEnemyState : AbstractEnemyState
	{
		[SerializeField] private AbstractEnemyState m_StateUponSeeingPlayer;

		protected override Vector3 GetMotionTarget(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			return enemy.wanderStartPosition;
		}

		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			if (enemy.canSeePlayer && m_StateUponSeeingPlayer != null)
				return m_StateUponSeeingPlayer;

			return base.GetNextState(parent);
		}
	}
}
