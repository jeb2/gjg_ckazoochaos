﻿using CosmicKazooChaos.Abstract.Enemies.States;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.States
{
	public class FacingPlayerEnemyState : AbstractEnemyState
	{
		[SerializeField] private AbstractEnemyState m_StateUponLosingPlayer;
		[SerializeField] private AbstractEnemyState m_StateWhenAimedAtPlayer;
		[SerializeField] private float m_AimingLeewayDegrees;

		public override LocomotionMode locomotionMode { get { return LocomotionMode.Locomotor; } }

		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
		}

		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);
		}

		protected override void ResolveRotation(EnemyController enemy, Vector3 movement)
		{
			Vector3 position = player.transform.position - enemy.transform.position;
			position.y = 0;
			RotateTowardsVector(enemy, position);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			if (!enemy.canSeePlayer && m_StateUponLosingPlayer != null)
				return m_StateUponLosingPlayer;

			if (enemy.IsAimedHorizontallyAt(player.transform.position, m_AimingLeewayDegrees) && m_StateWhenAimedAtPlayer != null)
				return m_StateWhenAimedAtPlayer;

			return base.GetNextState(parent);
		}
	}
}
