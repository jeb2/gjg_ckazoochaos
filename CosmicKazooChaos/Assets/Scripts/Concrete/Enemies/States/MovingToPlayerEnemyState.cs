﻿using CosmicKazooChaos.Abstract.Enemies.States;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.States
{
	public class MovingToPlayerEnemyState : AbstractEnemyState
	{
		[SerializeField] private AbstractEnemyState m_StateUponLosingPlayer;
		[SerializeField] private AbstractEnemyState m_StateUponReachingPlayer;
		[SerializeField] private Vector3 m_TargetOffset;
		[SerializeField] private float m_PlayerReachingDistance;
		[SerializeField] private float m_AimingLeewayDegrees = 10.0f;

		/// <summary>
		/// 	Gets the target towards which this enemy is trying to move.
		/// </summary>
		/// <returns>The motion target.</returns>
		/// <param name="parent">Parent.</param>
		protected override Vector3 GetMotionTarget(MonoBehaviour parent)
		{
			return player.transform.position + m_TargetOffset;
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			if (!enemy.canSeePlayer && m_StateUponLosingPlayer != null)
				return m_StateUponLosingPlayer;

			Vector3 playerpos = player.transform.position + m_TargetOffset;
			if (!enemy.movesIn3D)
				playerpos.y = enemy.transform.position.y;

			if (m_StateUponReachingPlayer != null &&
				Vector3.Distance(playerpos, enemy.transform.position) < m_PlayerReachingDistance &&
				enemy.IsAimedHorizontallyAt(playerpos, m_AimingLeewayDegrees))
				return m_StateUponReachingPlayer;

			return base.GetNextState(parent);
		}
	}
}
