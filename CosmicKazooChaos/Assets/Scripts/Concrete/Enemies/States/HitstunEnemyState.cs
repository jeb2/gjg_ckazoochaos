﻿using CosmicKazooChaos.Abstract.Enemies.States;
using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.States
{
	public class HitstunEnemyState : AbstractEnemyState
	{
		[SerializeField] private float m_HitstunForce = 8.0f;
		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractEnemyState m_TimerState;
		private Vector3 m_Source;

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractEnemyState timerState { get { return m_TimerState; } }

		public override AbstractEnemyState.LocomotionMode locomotionMode
		{
			get { return AbstractEnemyState.LocomotionMode.Locomotor; }
		}

		/// <summary>
		/// 	The origin point of the damage, in world coordinates.
		/// </summary>
		/// <value>The damage source coordinates.</value>
		public Vector3 source { get { return m_Source; } set { m_Source = value; } }

		/// <summary>
		/// 	Gets the force of the hitstun.
		/// </summary>
		/// <value>The force of the hitstun.</value>
		public float hitstunForce { get { return m_HitstunForce; } }

		/// <summary>
		/// 	Gets the timer to transition to another state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			return enemy.stateTimer;
		}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			enemy.stateTimer = new Timer(m_StateTimer);
			
			base.OnEnter(parent);
		}

		public override void OnExit(MonoBehaviour parent)
		{
			base.OnEnter(parent);
		}
	}
}
