﻿using System;
using CosmicKazooChaos.Concrete.Utils;
using Hydra.HydraCommon.Utils;
using UnityEngine;
using CosmicKazooChaos.Abstract.Enemies.States;

namespace CosmicKazooChaos.Concrete.Enemies.States
{
	public class WanderingEnemyState : IdleEnemyState
	{
		[SerializeField] private float m_TrackTimerPausingDistance = 0.1f;
		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
			EnemyController enemy = GetEnemyController(parent);

			enemy.wanderTimer.Reset();
			enemy.currentWanderTarget = NewTarget(parent);
			
			if (enemy.trackFollower.isActive)
				enemy.trackFollower.instructionTimer.Resume();
		}

		public override void OnFixedUpdate(MonoBehaviour parent)
		{
			base.OnFixedUpdate(parent);
			EnemyController enemy = GetEnemyController(parent);

			if (enemy.wanderTimer.complete)
			{
				enemy.wanderTimer.Reset();
				enemy.currentWanderTarget = NewTarget(parent);
			}
			
			if (enemy.trackFollower.isActive)
			{
				Vector3 target = enemy.currentWanderTarget;
				target.y = enemy.transform.position.y;
				enemy.trackFollower.instructionTimer.paused = (target - enemy.transform.position).magnitude > m_TrackTimerPausingDistance;
			}
		}

		protected override Vector3 GetMotionTarget(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			return enemy.currentWanderTarget; // enemy.currentWanderTarget then calls the track follower if necessary.
		}

		/// <summary>
		/// 	Returns a new target towards which this ant should move (to be used in GetMotionTarget.)
		/// </summary>
		/// <returns>The target.</returns>
		/// <param name="parent">Parent.</param>
		private Vector3 NewTarget(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			if (enemy.wanderWithinSphere)
				return enemy.wanderStartPosition + MapToRadius(UnityEngine.Random.insideUnitSphere, enemy);

			Vector2 v2 = UnityEngine.Random.insideUnitCircle;
			Vector3 pointInCircle = new Vector3(v2.x, 0.0f, v2.y);

			return enemy.wanderStartPosition + MapToRadius(pointInCircle, enemy);
		}

		private Vector3 MapToRadius(Vector3 point, EnemyController enemy)
		{
			float magnitude = point.magnitude;
			magnitude = HydraMathUtils.MapRange(0.0f, 1.0f, enemy.minWanderRadius, enemy.maxWanderRadius, point.magnitude);
			return point.normalized * magnitude;
		}
		
		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			// Here, account for transitions mandated by the parent's track follower.
			// Be sure to pause the track follower's timer if necessary.
			return base.GetNextState(parent);
		}
	}

	[Serializable]
	public struct WanderData
	{
		public float maxRadius;
		public float minRadius;
		public Timer timer;
		public bool wanderWithinSphere;
		[HideInInspector] public Vector3 startPoint;
		[HideInInspector] public Vector3 currentTarget;
	}
}
