﻿using CosmicKazooChaos.Abstract.Enemies.States;
using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.States
{
	public class ProjectileShootingEnemyState : AbstractEnemyState
	{
		[SerializeField] private Rigidbody m_ProjectilePrefab;
		[SerializeField] private float m_ProjectileSpeed;
		[SerializeField] private Vector3 m_RelativeProjectileOffset;
		[SerializeField] private float m_WarmUpTime;
		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractEnemyState m_TimerState;

		public override LocomotionMode locomotionMode { get { return LocomotionMode.Locomotor; } }

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractEnemyState timerState { get { return m_TimerState; } }

		/// <summary>
		/// 	Gets the timer to transition to another state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			return enemy.stateTimer;
		}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			enemy.hasFiredProjectile = false;
			enemy.stateTimer = new Timer(m_StateTimer);
			
			base.OnEnter(parent);
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);
			EnemyController enemy = GetEnemyController(parent);
			if (!enemy.hasFiredProjectile && GetStateTimer(parent).remaining < GetStateTimer(parent).maxTime - m_WarmUpTime)
				ShootProjectile(parent);
		}

		/// <summary>
		/// 	Shoots a projectile in the default direction.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void ShootProjectile(MonoBehaviour parent)
		{
			ShootProjectile(parent.transform.forward, parent);
			// TODO: Account for projectiles that arc.
		}

		/// <summary>
		/// 	Shoots a projectile in a specified direction.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="direction">Direction.</param>
		public void ShootProjectile(Vector3 direction, MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			enemy.hasFiredProjectile = true;
			Rigidbody proj = Instantiate(m_ProjectilePrefab, parent.transform.TransformPoint(m_RelativeProjectileOffset), parent.transform.rotation) as Rigidbody;
			proj.AddForce(direction.normalized * m_ProjectileSpeed, ForceMode.Impulse);
		}
	}
}
