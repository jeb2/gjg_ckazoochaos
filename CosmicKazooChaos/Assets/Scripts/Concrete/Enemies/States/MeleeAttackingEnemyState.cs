﻿using CosmicKazooChaos.Abstract.Enemies.States;
using CosmicKazooChaos.Concrete.HitBoxes;
using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.States
{
	public class MeleeAttackingEnemyState : AbstractEnemyState
	{
		public const string CLONE_SUFFIX = "(Clone)";

		[SerializeField] private HitBox m_HitBoxPrefab;
		[SerializeField] private int m_Damage = 1;
		[SerializeField] private float m_WarmUpTime;
		[SerializeField] private float m_AttackDuration;
		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractEnemyState m_TimerState;

		/// <summary>
		/// 	Gets the hit box prefab.
		/// </summary>
		/// <value>The hit box prefab.</value>
		protected virtual HitBox hitBoxPrefab { get { return m_HitBoxPrefab; } }

		/// <summary>
		/// 	Gets the damage.
		/// </summary>
		/// <value>The damage.</value>
		public int damage { get { return m_Damage; } }

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractEnemyState timerState { get { return m_TimerState; } }

		/// <summary>
		/// 	Gets the timer to transition to another state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			return enemy.stateTimer;
		}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			enemy.stateTimer = new Timer(m_StateTimer);
			
			base.OnEnter(parent);
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			if (GetStateTimer(parent).remaining < GetStateTimer(parent).maxTime - m_WarmUpTime)
			{
				if (GetStateTimer(parent).remaining < GetStateTimer(parent).maxTime - (m_WarmUpTime + m_AttackDuration))
					GetHitBox(parent).gameObject.SetActive(false);
				else
					GetHitBox(parent).gameObject.SetActive(true);
			}
		}

		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);
			GetHitBox(parent).gameObject.SetActive(false);
		}

		/// <summary>
		/// 	Gets the hit box. Instantiates it if it does not exist.
		/// </summary>
		/// <returns>The hit box instance.</returns>
		/// <param name="parent">Parent.</param>
		public HitBox GetHitBox(MonoBehaviour parent)
		{
			Transform child = parent.transform.Find(m_HitBoxPrefab.name + CLONE_SUFFIX);
			if (child != null)
				return child.GetComponent<HitBox>();

			Vector3 position = parent.transform.position + parent.transform.rotation * m_HitBoxPrefab.transform.position;
			Quaternion rotation = parent.transform.rotation * m_HitBoxPrefab.transform.rotation;

			HitBox output = Instantiate(m_HitBoxPrefab, position, rotation) as HitBox;
			output.transform.parent = parent.transform;

			return output;
		}
	}
}
