﻿using CosmicKazooChaos.Utils.Time;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.GruntSquad
{
	public class ScramblingGrunt : StateMachineBehaviour
	{
		private const float REORIENT_RATE = 2.0f;

		private float m_NextReorient;
		private NavMeshAgent m_NavMeshAgentCached;
		private Vector3 m_Center;

		// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			m_NavMeshAgentCached = animator.gameObject.GetComponent<NavMeshAgent>();
			m_Center = animator.gameObject.transform.position;
		}

		// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (GameTime.time < m_NextReorient)
				return;

			// Run to random offsets around the point where Grunt entered the scrambling state
			Vector3 dest = m_Center + new Vector3(Random.value * 5, 0, Random.value * 5);
			m_NextReorient = GameTime.time + REORIENT_RATE;
			m_NavMeshAgentCached.destination = dest;
		}
	}
}
