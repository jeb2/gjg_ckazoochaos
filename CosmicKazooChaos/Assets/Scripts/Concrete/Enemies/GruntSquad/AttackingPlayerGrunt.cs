﻿using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.GruntSquad
{
	public class AttackingPlayerGrunt : StateMachineBehaviour
	{
		private GameObject m_PlayerCached;
		private NavMeshAgent m_NavMeshAgentCached;

		// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			m_NavMeshAgentCached = animator.gameObject.GetComponent<NavMeshAgent>();
			m_PlayerCached = GameObject.FindGameObjectWithTag("Player");
		}

		// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			m_NavMeshAgentCached.destination = m_PlayerCached.transform.position;
		}
	}
}
