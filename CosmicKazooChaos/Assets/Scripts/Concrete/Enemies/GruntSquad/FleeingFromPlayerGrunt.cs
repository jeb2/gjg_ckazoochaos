﻿using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.GruntSquad
{
	public class FleeingFromPlayerGrunt : StateMachineBehaviour
	{
		private GameObject m_PlayerCached;
		private NavMeshAgent m_NavMeshAgentCached;

		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			m_NavMeshAgentCached = animator.gameObject.GetComponent<NavMeshAgent>();
			m_PlayerCached = GameObject.FindGameObjectWithTag("Player");
		}

		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			Vector3 position = animator.gameObject.transform.position;
			Vector3 away = (position - m_PlayerCached.transform.position).normalized;
			m_NavMeshAgentCached.destination = position + away;
		}
	}
}
