﻿using CosmicKazooChaos.API.Damageable;
using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.GruntSquad
{
	[RequireComponent(typeof(CharacterController))]
	public class BigGuyBehavior : MonoBehaviour, IDamageable
	{
		[SerializeField] private bool m_IsAlive = true;

		#region Properties

		/// <summary>
		/// 	Gets or sets a value indicating whether this BigGuyBehavior is alive.
		/// </summary>
		/// <value><c>true</c> if is alive; otherwise, <c>false</c>.</value>
		public bool isAlive { get { return m_IsAlive; } set { m_IsAlive = value; } }

		#endregion

		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			isAlive = false;
		}
	}
}
