﻿using CosmicKazooChaos.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Enemies.GruntSquad
{
	public class GruntSpawnerController : HydraMonoBehaviour
	{
		[SerializeField] private BigGuyBehavior m_SquadLeader;
		[SerializeField] private GruntBehavior m_GruntPrefab;
		[SerializeField] private int m_MaxGruntsSpawned;
		[SerializeField] private float m_MaxXSpawnOffset = 5.0f;
		[SerializeField] private float m_MaxZSpawnOffset = 5.0f;
		[SerializeField] private float m_SpawnRateInSeconds = 1.0f;
		private int m_GruntCounter = 0;
		private Timer m_SpawnTimer;

		protected override void Start()
		{
			base.Start();
			m_SpawnTimer = new Timer();
			m_MaxXSpawnOffset = System.Math.Abs(m_MaxXSpawnOffset);
			m_MaxZSpawnOffset = System.Math.Abs(m_MaxZSpawnOffset);
		}

		protected override void Update()
		{
			base.Update();

			if (m_SpawnTimer.elapsed > m_SpawnRateInSeconds && m_GruntCounter < m_MaxGruntsSpawned)
			{
				SpawnGrunt();
				m_SpawnTimer.Reset();
			}
		}

		protected override void OnDrawGizmos()
		{
			base.OnDrawGizmos();

			Gizmos.color = Color.red;
			Gizmos.DrawCube(transform.position, new Vector3(1, 1, 1));
		}

		private void UpdateGruntCounter()
		{
			--m_GruntCounter;
			if (m_GruntCounter < 0)
				m_GruntCounter = 0;
		}

		private void SpawnGrunt()
		{
			float xoffset = Random.Range(-m_MaxXSpawnOffset, m_MaxXSpawnOffset);
			float zoffset = Random.Range(-m_MaxZSpawnOffset, m_MaxZSpawnOffset);
			Vector3 offset = new Vector3(xoffset, 0.0f, zoffset);

			GruntBehavior g = Instantiate(m_GruntPrefab, transform.position + offset, transform.rotation) as GruntBehavior;
			if (m_SquadLeader)
				g.squadLeader = m_SquadLeader;
			g.onDestroyedCallback += UpdateGruntCounter;
			// Probably room for race conditions, I'll need a better way to do this
			++m_GruntCounter;
		}
	}
}
