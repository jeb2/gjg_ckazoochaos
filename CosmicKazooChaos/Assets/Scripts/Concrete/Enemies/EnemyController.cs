﻿using CosmicKazooChaos.API.Damageable;
using CosmicKazooChaos.Concrete.Enemies.States;
using CosmicKazooChaos.Concrete.StateMachine;
using CosmicKazooChaos.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;
using CosmicKazooChaos.Concrete.Platforms;

namespace CosmicKazooChaos.Concrete.Enemies
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(CharacterLocomotor))]
	[RequireComponent(typeof(NavMeshAgent))]
	public class EnemyController : HydraMonoBehaviour, IDamageable
	{
		[SerializeField] private EnemyStateMachine m_StateMachine;
		[SerializeField] private float m_PlayerDetectionRadius;
		[SerializeField] private Timer m_PlayerDetectionTimer;
		[SerializeField] private float m_WalkingSpeed;
		[SerializeField] private float m_RunningSpeed;
		[SerializeField] private float m_Acceleration;
		[SerializeField] private float m_RotateSpeed;
		[SerializeField] private WanderData m_WanderData;
		[SerializeField] private float m_MinimumNMADestinationChange = 0.25f;
		[SerializeField] private bool m_MovesIn3D = false;
		[SerializeField] private TrackFollower m_TrackFollower;

		private bool m_CanSeePlayer = false;
		private Timer m_StateTimer = new Timer();
		private bool m_HasFiredProjectile = false;
		private CharacterLocomotor m_CachedCharacterLocomotor;
		private NavMeshAgent m_CachedNavMeshAgent;

		// should prolly add comments and shit to these things
		public EnemyStateMachine stateMachine { get { return m_StateMachine; } }
		public TrackFollower trackFollower { get { return m_TrackFollower; } }
		public Timer stateTimer { get { return m_StateTimer; } set { m_StateTimer = value; } }
		public float playerDetectionRadius { get { return m_PlayerDetectionRadius; } }
		public Timer playerDetectionTimer { get { return m_PlayerDetectionTimer; } }
		public float walkingSpeed { get { return m_WalkingSpeed; } }
		public float runningSpeed { get { return m_RunningSpeed; } }
		public float acceleration { get { return m_Acceleration; } }
		public float rotateSpeed { get { return m_RotateSpeed; } }
		public float maxWanderRadius { get { return m_WanderData.maxRadius; } }
		public float minWanderRadius { get { return m_WanderData.minRadius; } }
		public Timer wanderTimer { get { return m_WanderData.timer; } }
		public bool wanderWithinSphere { get { return m_WanderData.wanderWithinSphere; } }
		public Vector3 wanderStartPosition { get { return m_WanderData.startPoint; } }

		public Vector3 currentWanderTarget
		{
			get
			{
				return m_TrackFollower.isActive
				? m_TrackFollower.currentInstruction.GetNextPosition(m_TrackFollower)
				: m_WanderData.currentTarget;
			}
			set { m_WanderData.currentTarget = value; }
		}

		public float mimimumNMADesintationChange { get { return m_MinimumNMADestinationChange; } }

		public bool movesIn3D { get { return m_MovesIn3D; } }

		public bool hasFiredProjectile { get { return m_HasFiredProjectile; } set { m_HasFiredProjectile = value; } }

		/// <summary>
		/// Property for determining whether the navmesh agent should perform movement.
		/// Setting this to false allows e.g. enemy states to use their own movement code,
		/// but keeping it in sync with the simulated position falls on the state. 
		/// </summary>
		/// <value><c>true</c> if using navmesh agent; otherwise, <c>false</c>.</value>
		public bool usingNavMeshAgent { get { return navMeshAgent.enabled; } set { navMeshAgent.enabled = value; } }

		/// <summary>
		/// 	Gets the character locomotor.
		/// </summary>
		/// <value>The character locomotor.</value>
		public CharacterLocomotor characterLocomotor
		{
			get { return m_CachedCharacterLocomotor ?? (m_CachedCharacterLocomotor = GetComponent<CharacterLocomotor>()); }
		}

		/// <summary>
		/// Gets the navmesh agent.
		/// </summary>
		/// <value>The navmesh agent.</value>
		public NavMeshAgent navMeshAgent
		{
			get { return m_CachedNavMeshAgent ?? (m_CachedNavMeshAgent = GetComponent<NavMeshAgent>()); }
		}

		/// <summary>
		/// 	Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Enemies.EnemyController"/> can see the player.
		/// </summary>
		/// <value><c>true</c> if can see player; otherwise, <c>false</c>.</value>
		public bool canSeePlayer { get { return m_CanSeePlayer; } }

		/// <summary>
		/// 	Called when the Damageable gets damaged.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			// Temporary invincibility.
			if (m_StateMachine.activeState is HitstunEnemyState)
				return;

			HitstunEnemyState hitstunState = m_StateMachine.activeState.GetHitstunState();
			hitstunState.source = collision.point;
			m_StateMachine.SetActiveState(hitstunState, this);

			Vector3 knockbackDirection = (transform.position - collision.point);
			//Keep the horizontal direction, but angle 30° up
			knockbackDirection.y = 0;
			knockbackDirection.Normalize();

			//On flat surfaces, continue going in same direction
			if (knockbackDirection.magnitude < 0.1f)
				knockbackDirection = characterLocomotor.flatVelocity.normalized;

			knockbackDirection.y = 0.5f; //Add a bit of vertical

			//Ignore pre-existing velocity when you get stunned
			characterLocomotor.velocity = knockbackDirection * hitstunState.hitstunForce;
		}

		/// <summary>
		/// 	Check to see if the player is within visibility range.
		/// </summary>
		/// <returns><c>true</c>, if player was detected, <c>false</c> otherwise.</returns>
		private bool DetectPlayer()
		{
			if (m_TrackFollower.isActive && m_TrackFollower.preventReactionToPlayer)
				return false;
			else
				return Vector3.Distance(GameObject.FindWithTag("Player").transform.position, transform.position) < playerDetectionRadius;
		}

		public bool IsAimedHorizontallyAt(Vector3 position, float leeway)
		{
			position.y = transform.position.y;
			return Vector3.Angle(transform.forward, position - transform.position) < leeway;
		}

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();
			
			m_WanderData.startPoint = transform.position;
			m_StateMachine.OnEnable(this);
			playerDetectionTimer.Reset();
			m_TrackFollower.OnParentEnable();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();
			m_StateMachine.Update(this);
			if (playerDetectionTimer.complete)
			{
				playerDetectionTimer.Reset();
				m_CanSeePlayer = DetectPlayer();
			}
		}

		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();
			m_StateMachine.FixedUpdate(this);
			m_TrackFollower.OnFixedUpdate();
		}

		/// <summary>
		/// 	Called after all Updates have finished.
		/// </summary>
		protected override void LateUpdate()
		{
			base.LateUpdate();
			m_StateMachine.LateUpdate(this);
		}

		/// <summary>
		/// Message to set the navmesh agent to move towards to
		/// </summary>
		/// <param name="target">Target.</param>
		protected virtual void SetNavMeshAgentTarget(Vector3 target)
		{
			navMeshAgent.destination = target;
		}
	}
}
