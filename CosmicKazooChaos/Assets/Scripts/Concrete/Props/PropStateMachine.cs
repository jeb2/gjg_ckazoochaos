using System;
using CosmicKazooChaos.Abstract.Props.States;
using CosmicKazooChaos.Abstract.StateMachine;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Props
{
	[Serializable]
	public class PropStateMachine : AbstractStateMachine<AbstractPropState>
	{
		[SerializeField] private AbstractPropState m_InitialState;

		public override AbstractPropState initialState { get { return m_InitialState; } }
	}
}
