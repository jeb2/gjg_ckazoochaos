﻿using CosmicKazooChaos.Abstract.Props;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Props
{
	/// <summary>
	/// 	Teleporter transports the player within the scene.
	/// </summary>
	public class Teleporter : AbstractTeleporter
	{
		[SerializeField] private Teleporter m_Target;

		/// <summary>
		/// 	Teleports the character to the destination.
		/// </summary>
		/// <param name="character">Character.</param>
		public override void Teleport(CharacterController character)
		{
			m_Target.active = false;
			character.transform.position = m_Target.transform.position;
		}
	}
}
