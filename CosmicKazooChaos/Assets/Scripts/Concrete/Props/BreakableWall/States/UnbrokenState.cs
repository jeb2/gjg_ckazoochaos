using CosmicKazooChaos.Abstract.Props.States;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Props.BreakableWall.States
{
	public class UnbrokenState : AbstractPropState
	{
		[SerializeField] private Material m_UnbrokenMaterial;
		private Renderer m_CachedRenderer;

		public Material material { get { return m_UnbrokenMaterial; } }

		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
			m_CachedRenderer = parent.gameObject.GetComponent<Renderer>();

			m_CachedRenderer.material = material;
		}
	}
}
