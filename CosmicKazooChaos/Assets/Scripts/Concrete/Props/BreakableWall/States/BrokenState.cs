using CosmicKazooChaos.Abstract.Props.States;
using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Props.BreakableWall.States
{
	public class BrokenState : AbstractPropState
	{
		[SerializeField] private UnbrokenState m_UnbrokenState;
		[SerializeField] private Timer m_Timer;
		[SerializeField] private Material m_BrokenMaterial;
		private BoxCollider m_CachedCollider;
		private Renderer m_CachedRenderer;

		public Material material { get { return m_BrokenMaterial; } }

		public override AbstractPropState GetNextState(MonoBehaviour parent)
		{
			if (m_Timer.complete)
				return m_UnbrokenState;

			return base.GetNextState(parent);
		}

		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
			m_Timer.Reset();

			m_CachedCollider = parent.gameObject.GetComponent<BoxCollider>();
			m_CachedRenderer = parent.gameObject.GetComponent<Renderer>();

			m_CachedCollider.enabled = false;
			m_CachedRenderer.material = material;
		}

		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);
			m_CachedCollider.enabled = true;
		}
	}
}
