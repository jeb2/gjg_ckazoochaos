using CosmicKazooChaos.Abstract.Props;
using CosmicKazooChaos.Concrete.Props.BreakableWall.States;
using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Props.BreakableWall
{
	[DisallowMultipleComponent]
	public class BreakableWallController : AbstractPropController
	{
		[SerializeField] private BrokenState m_BrokenState;

		public override void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			base.Damage(damage, collision);

			//TODO: variable in UnbrokenState instead of SetActiveState()
			stateMachine.SetActiveState(m_BrokenState, this);
		}
	}
}
