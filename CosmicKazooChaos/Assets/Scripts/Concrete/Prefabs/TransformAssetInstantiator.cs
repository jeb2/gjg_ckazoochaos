﻿using CosmicKazooChaos.Abstract.Prefabs;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Prefabs
{
	/// <summary>
	/// 	Transform asset instantiator.
	/// </summary>
	public class TransformAssetInstantiator : AbstractAssetInstantiator<Transform> {}
}
