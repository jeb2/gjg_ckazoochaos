﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.BlobShadow
{
	[RequireComponent(typeof(Projector))]
	public class BlobProjectorFixer : HydraMonoBehaviour
	{
		// Used for pre-scaling up the object for proper air distance scaling 
		[SerializeField] private float m_InitialScale = 5f;

		//Used when dividing the pre-scale with the player's distance to the ground for a more lenghty scaling
		[SerializeField] private float m_Addition = 11f;

		//Positive number used to casting shadows on terrains correctly
		[SerializeField] private float m_ShadowDistanceTolerance = 0.5f;

		private Projector m_CachedProjector;

		/// <summary>
		/// 	Gets the projector.
		/// </summary>
		/// <value>The projector.</value>
		public Projector projector { get { return m_CachedProjector ?? (m_CachedProjector = GetComponent<Projector>()); } }

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			Vector3 start = projector.transform.position + projector.transform.forward.normalized * projector.nearClipPlane;

			RaycastHit hit;
			Ray ray = new Ray(start, projector.transform.forward);

			if (!Physics.Raycast(ray, out hit, projector.farClipPlane - projector.nearClipPlane, ~projector.ignoreLayers))
				return;

			if (!FilterHitInfo(hit, ray.direction, false))
				return;

			float currentScale = projector.orthographicSize;
			currentScale = m_InitialScale / (hit.distance + m_Addition);

			projector.orthographicSize = currentScale;

			float distance = hit.distance + projector.nearClipPlane;
			projector.farClipPlane = (distance + m_ShadowDistanceTolerance) * 2;
		}

		/// <summary>
		/// 	Returns false if the RaycastHit should be ignored.
		/// </summary>
		/// <returns><c>true</c>, if hit info is good, <c>false</c> otherwise.</returns>
		/// <param name="hitInfo">Hit info.</param>
		/// <param name="direction">Direction.</param>
		/// <param name="ignoreTriggers">If set to <c>true</c> ignore triggers.</param>
		private bool FilterHitInfo(RaycastHit hitInfo, Vector3 direction, bool ignoreTriggers)
		{
			// Ignore triggers
			if (ignoreTriggers && hitInfo.collider.isTrigger)
				return false;

			return true;
		}
	}
}
