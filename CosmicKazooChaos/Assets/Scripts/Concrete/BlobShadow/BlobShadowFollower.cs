﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.BlobShadow
{
	/// <summary>
	/// 	TODO: ADD RAYS TO SMOOTHEN SURFACE ROTATION
	/// 	TODO: MAKE SURE PARTS OF TEXTURE ARE PROPERLY CUT OFF IN SHADER
	/// </summary>
	public class BlobShadowFollower : HydraMonoBehaviour
	{
		[SerializeField] private Vector3 m_RayOffSet = new Vector3(0, 1, 0);
		[SerializeField] private float m_InitialScale = 1.2f;
		[SerializeField] private float m_Addition = 11f;

		private GameObject m_Parent;

		protected override void OnEnable()
		{
			base.OnEnable();

			m_Parent = transform.parent.gameObject;
		}

		protected override void FixedUpdate()
		{
			base.FixedUpdate();

			CastTheRay();
		}

		/// <summary>
		/// 	Returns false if the RaycastHit should be ignored.
		/// </summary>
		/// <returns><c>true</c>, if hit info was filtered, <c>false</c> otherwise.</returns>
		/// <param name="hitInfo">Hit info.</param>
		/// <param name="direction">Direction.</param>
		/// <param name="ignoreTriggers">If set to <c>true</c> ignore triggers.</param>
		private bool FilterHitInfo(RaycastHit hitInfo, Vector3 direction, bool ignoreTriggers)
		{
			// Ignore collisions with self
			if (hitInfo.collider == m_Parent.GetComponent<Collider>())
				return false;

			// Ignore triggers
			if (ignoreTriggers && hitInfo.collider.isTrigger)
				return false;

			// Ignore normals that face along the cast direction
			if (Vector3.Dot(direction, hitInfo.normal) >= 0.0f)
				return false;

			return true;
		}

		private void CastTheRay()
		{
			RaycastHit hitInfo = new RaycastHit();

			if (!Physics.Raycast(m_Parent.transform.position + m_RayOffSet, Vector3.down, out hitInfo, Mathf.Infinity))
				return;

			if (!FilterHitInfo(hitInfo, Vector3.down, true))
				return;

			transform.position = new Vector3(hitInfo.point.x, hitInfo.point.y + 0.02f, hitInfo.point.z);

			Vector3 surfaceNormal = hitInfo.normal; // Assign the normal of the surface to surfaceNormal
			Vector3 forwardRelativeToSurfaceNormal = Vector3.Cross(transform.right, surfaceNormal);

			// Check for target rotation.
			Quaternion targetRotation = Quaternion.LookRotation(forwardRelativeToSurfaceNormal, surfaceNormal);

			// Make sure rotation is smoothed towards target rotation.
			transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.fixedDeltaTime * 25f);

			Vector3 currentScale = transform.localScale;
			currentScale.x = m_InitialScale / (hitInfo.distance + m_Addition);
			currentScale.z = m_InitialScale / (hitInfo.distance + m_Addition);
			transform.localScale = currentScale;
		}
	}
}
