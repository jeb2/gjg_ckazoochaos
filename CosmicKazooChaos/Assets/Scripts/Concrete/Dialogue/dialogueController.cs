﻿using UnityEngine;
using System.Collections;
using CosmicKazooChaos;
using UnityEngine.UI;

public class dialogueController : MonoBehaviour
{
	private int i = 0;
	private dialogueClip[] Lines;
	public Canvas dialogueCanvas;
	public Text panelText;
	public Camera sceneCamera;

	void Start ()
	{
		Lines = gameObject.GetComponents<dialogueClip> ();
	}

	void hitNext ()
	{
		sceneCamera.transform.LookAt (Lines [i].Character.transform);
		Lines[i].Character.GetComponent<Animator> ().SetTrigger (Lines [i].animationTrigger);
		//Lines [i].Character.GetComponent<AudioSource> ().clip = Lines [i].Dialogue;
		panelText.text = Lines [i].dialogueText;
		i += 1;
	}
}
