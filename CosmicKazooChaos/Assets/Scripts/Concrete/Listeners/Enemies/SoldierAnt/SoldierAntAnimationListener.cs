﻿using CosmicKazooChaos.Abstract.Listeners.Enemies;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Listeners.Enemies.SoldierAnt
{
	[DisallowMultipleComponent]
	public class SoldierAntAnimationListener : AbstractEnemyAnimationListener {}
}
