﻿using CosmicKazooChaos.Abstract.Player.States;
using CosmicKazooChaos.Abstract.StateMachine;
using CosmicKazooChaos.API.Listeners.Player;
using CosmicKazooChaos.Concrete.Utils;
using CosmicKazooChaos.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Listeners.Player
{
	public class JumpMarkerListener : HydraMonoBehaviour, IPlayerListener
	{
		[SerializeField] private GameObject m_MarkerPrefab;

		[SerializeField] private Color m_ColorA;
		[SerializeField] private Color m_ColorB;

		private GameObject m_MarkerInstanceA;
		private GameObject m_MarkerInstanceB;

		private bool useA = true;

		#region Properties

		[Tweakable("Jump")]
		public bool enableJumpMarker
		{
			get { return this.enabled; }
			set
			{
				this.enabled = value;
				if (m_MarkerInstanceA != null)
					m_MarkerInstanceA.SetActive(value);
				if (m_MarkerInstanceB != null)
					m_MarkerInstanceB.SetActive(value);
			}
		}

		public GameObject markerPrefab { get { return m_MarkerPrefab; } set { m_MarkerPrefab = value; } }

		public Color colorA { get { return m_ColorA; } set { m_ColorB = value; } }

		public Color colorB { get { return m_ColorB; } set { m_ColorB = value; } }

		#endregion

		#region Messages

		public void OnJump()
		{
			GameObject marker = GetNextMarker();
			marker.transform.position = this.transform.position;
		}

		public void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo) {}

		public void OnDamage(CharacterLocomotorCollisionData collision) {}

		public void OnSwapEnter() {}

		/// <summary>
		/// 	Called when an animation event is fired.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		public virtual void OnAnimationEvent(AnimationEvent animationEvent) {}

		#endregion

		#region Private Methods

		private void SetColor(GameObject marker, Color color)
		{
			MeshRenderer[] meshes = marker.GetComponentsInChildren<MeshRenderer>();
			foreach (MeshRenderer mesh in meshes)
				mesh.material.color = color;
		}

		private GameObject GetNextMarker()
		{
			GameObject marker;
			if (useA)
			{
				if (m_MarkerInstanceA == null)
				{
					m_MarkerInstanceA = Instantiate<GameObject>(m_MarkerPrefab);
					SetColor(m_MarkerInstanceA, m_ColorA);
				}
				marker = m_MarkerInstanceA;
			}
			else
			{
				if (m_MarkerInstanceB == null)
				{
					m_MarkerInstanceB = Instantiate<GameObject>(m_MarkerPrefab);
					SetColor(m_MarkerInstanceB, m_ColorB);
				}
				marker = m_MarkerInstanceB;
			}
			useA = !useA;
			return marker;
		}

		#endregion
	}
}
