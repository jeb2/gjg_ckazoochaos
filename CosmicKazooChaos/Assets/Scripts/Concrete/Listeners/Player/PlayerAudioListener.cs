﻿using CosmicKazooChaos.Abstract.Audio.Player;
using CosmicKazooChaos.Abstract.Listeners.Player;
using CosmicKazooChaos.API.Listeners.Player;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Listeners.Player
{
	public class PlayerAudioListener : AbstractPlayerAudioListener, IPlayerListener
	{
		[SerializeField] private PlayerSFX m_SFX;
		
		#region Properties
		
		/// <summary>
		/// 	Gets the sound effects.
		/// </summary>
		/// <value>The sound effects.</value>
		public override AbstractPlayerSFX sfx { get { return m_SFX; } }
		
		#endregion
	}
}
