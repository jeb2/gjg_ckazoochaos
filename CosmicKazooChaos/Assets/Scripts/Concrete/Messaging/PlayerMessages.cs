﻿using CosmicKazooChaos.Abstract.Player;
using CosmicKazooChaos.Abstract.Player.States;
using CosmicKazooChaos.Abstract.StateMachine;
using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Messaging
{
	public static class PlayerMessages
	{
		public static void BroadcastOnJump(AbstractPlayerController player)
		{
			player.gameObject.BroadcastMessage("OnJump", SendMessageOptions.RequireReceiver);
		}

		public static void BroadcastOnDamage(AbstractPlayerController player, CharacterLocomotorCollisionData collision)
		{
			player.gameObject.BroadcastMessage("OnDamage", collision, SendMessageOptions.RequireReceiver);
		}

		public static void BroadcastOnStateChange(AbstractPlayerController player,
												  StateChangeInfo<AbstractPlayerState> changeInfo)
		{
			player.gameObject.BroadcastMessage("OnStateChange", changeInfo, SendMessageOptions.RequireReceiver);
		}

		public static void BroadcastOnSwapEnter(AbstractPlayerController player)
		{
			player.gameObject.BroadcastMessage("OnSwapEnter", SendMessageOptions.RequireReceiver);
		}

		public static void BroadcastOnAnimationEvent(AbstractPlayerController player, AnimationEvent animationEvent)
		{
			player.gameObject.BroadcastMessage("OnAnimationEvent", animationEvent, SendMessageOptions.RequireReceiver);
		}
	}
}
