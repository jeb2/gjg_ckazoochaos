using CosmicKazooChaos.Abstract.Props.States;
using CosmicKazooChaos.Abstract.StateMachine;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Messaging
{
	public static class PropMessages
	{
		public static void BroadcastOnStateChange(GameObject gameObject, StateChangeInfo<AbstractPropState> changeInfo)
		{
			gameObject.BroadcastMessage("OnStateChange", changeInfo, SendMessageOptions.RequireReceiver);
		}
	}
}
