﻿using CosmicKazooChaos.Abstract.Enemies.States;
using CosmicKazooChaos.Abstract.StateMachine;
using CosmicKazooChaos.Concrete.Enemies;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Messaging
{
	public static class EnemyMessages
	{
		public static void BroadcastOnStateChange(EnemyController enemy, StateChangeInfo<AbstractEnemyState> changeInfo)
		{
			enemy.BroadcastMessage("OnStateChange", changeInfo, SendMessageOptions.RequireReceiver);
		}
	}
}
