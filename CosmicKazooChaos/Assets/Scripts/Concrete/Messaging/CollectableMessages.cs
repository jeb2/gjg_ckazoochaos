﻿using UnityEngine;

namespace CosmicKazooChaos.Concrete.Messaging
{
	public static class CollectableMessages
	{
		public static void BroadcastOnCollected(GameObject gameObject)
		{
			gameObject.BroadcastMessage("OnCollected", SendMessageOptions.RequireReceiver);
		}
	}
}
