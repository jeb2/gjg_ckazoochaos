﻿using CosmicKazooChaos.Abstract.Configuration;
using CosmicKazooChaos.Concrete.Audio.Player;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Configuration
{
	/// <summary>
	/// 	AudioConfig provides audio configuration details.
	/// </summary>
	public class AudioConfig : AbstractConfig<AudioConfig>
	{
		[SerializeField] private FootstepsSFX m_FootstepsSFX;

		#region Properties

		/// <summary>
		/// 	Gets the footsteps SFX.
		/// </summary>
		/// <value>The footsteps SFX.</value>
		public FootstepsSFX footstepsSFX { get { return m_FootstepsSFX; } }

		#endregion
	}
}
