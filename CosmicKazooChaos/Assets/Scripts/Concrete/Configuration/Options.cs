﻿using UnityEngine;

namespace CosmicKazooChaos.Concrete.Configuration
{
	/// <summary>
	/// 	Options is a wrapper around PlayerPrefs for storing user configuration changes.
	/// </summary>
	public static class Options
	{
		private const string CAMERA_HORIZONTAL_SENSITIVITY_KEY = "CAMERA_HORIZONTAL_SENSITIVITY";
		private const string CAMERA_VERTICAL_SENSITIVITY_KEY = "CAMERA_VERTICAL_SENSITIVITY";

		#region Properties

		public static float cameraHorizontalSensitivity
		{
			get
			{
				float sensitivity = OptionsDefaults.instance.cameraDefaults.horizontalSensitivity;
				return PlayerPrefs.GetFloat(CAMERA_HORIZONTAL_SENSITIVITY_KEY, sensitivity);
			}
			set { PlayerPrefs.SetFloat(CAMERA_HORIZONTAL_SENSITIVITY_KEY, value); }
		}

		public static float cameraVerticalSensitivity
		{
			get
			{
				float sensitivity = OptionsDefaults.instance.cameraDefaults.verticalSensitivity;
				return PlayerPrefs.GetFloat(CAMERA_VERTICAL_SENSITIVITY_KEY, sensitivity);
			}
			set { PlayerPrefs.SetFloat(CAMERA_VERTICAL_SENSITIVITY_KEY, value); }
		}

		#endregion

		#region Methods

		public static void Save()
		{
			PlayerPrefs.Save();
		}

		#endregion
	}
}
