﻿using System;
using System.Collections.Generic;
using Hydra.HydraCommon.PropertyAttributes.InputAttributes;

namespace CosmicKazooChaos.Concrete.Configuration.Controls.Mapping
{
	/// <summary>
	/// 	Input mapping.
	/// </summary>
	public static class InputMapping
	{
		private static IControlScheme s_ControlScheme;
		private static ButtonInputAttribute s_ControlChange;
		private static Dictionary<Type, IControlScheme> s_ControlSchemeCache;

		private static ButtonInputAttribute s_ToggleDebugUI;

		/// <summary>
		/// 	Initializes the <see cref="InputMapping"/> class.
		/// </summary>
		static InputMapping()
		{
			s_ControlScheme = GetControlScheme<KeyboardControlScheme>();
			s_ControlChange = new ButtonInputAttribute("Submit");
			s_ToggleDebugUI = new ButtonInputAttribute("Toggle Debug UI");
		}

		/// <summary>
		/// 	Switch the input controller.
		/// </summary>
		public static void SwitchInputJoystick()
		{
			if (s_ControlScheme is KeyboardControlScheme)
				s_ControlScheme = GetControlScheme<JoystickControlScheme>();
			else
				s_ControlScheme = GetControlScheme<KeyboardControlScheme>();
		}

		#region Common Mappings

		/// <summary>
		/// 	Gets the horizontal input.
		/// </summary>
		/// <value>The horizontal input.</value>
		public static AxisInputAttribute horizontalInput { get { return s_ControlScheme.horizontalInput; } }

		/// <summary>
		/// 	Gets the vertical input.
		/// </summary>
		/// <value>The vertical input.</value>
		public static AxisInputAttribute verticalInput { get { return s_ControlScheme.verticalInput; } }

		/// <summary>
		/// 	Gets the horizontal camera input.
		/// </summary>
		/// <value>The horizontal camera input.</value>
		public static AxisInputAttribute horizontalCameraInput { get { return s_ControlScheme.horizontalCameraInput; } }

		/// <summary>
		/// 	Gets the vertical camera input.
		/// </summary>
		/// <value>The vertical camera input.</value>
		public static AxisInputAttribute verticalCameraInput { get { return s_ControlScheme.verticalCameraInput; } }

		/// <summary>
		/// 	Gets the jump input.
		/// </summary>
		/// <value>The jump input.</value>
		public static ButtonInputAttribute jumpInput { get { return s_ControlScheme.jumpInput; } }

		/// <summary>
		/// 	Gets the switch (???) input.
		/// </summary>
		/// <value>The switch (???) input.</value>
		public static ButtonInputAttribute switchInput { get { return s_ControlScheme.switchInput; } }

		/// <summary>
		/// 	Gets the switch controller input to switch between keyboard and joystick.
		/// </summary>
		/// <value>The switch controller input.</value>
		public static ButtonInputAttribute switchControllerInput { get { return s_ControlChange; } }

		public static ButtonInputAttribute toggleDebugUI { get { return s_ToggleDebugUI; } }

		/// <summary>
		/// 	Gets the Camera Snap input.
		/// </summary>
		/// <value>The Camera Snap input.</value>
		public static ButtonInputAttribute snapCamera { get { return s_ControlScheme.snapCameraInput; } }

		/// <summary>
		/// 	Gets the input to open the in-game pause menu.
		/// </summary>
		/// <value>The input to open the in-game pause menu</value>
		public static ButtonInputAttribute pause { get { return s_ControlScheme.pauseInput; } }

		#endregion

		///<summary>
		/// 	Gets the Dash input.
		/// </summary>
		/// <value>The Dash input.</value>
		public static ButtonInputAttribute dashInput { get { return s_ControlScheme.dashInput; } }

		#region Private Methods

		/// <summary>
		/// 	Gets the control scheme.
		/// </summary>
		/// <returns>The control scheme.</returns>
		/// <typeparam name="T">The control scheme type.</typeparam>
		private static T GetControlScheme<T>() where T : class, IControlScheme, new()
		{
			if (s_ControlSchemeCache == null)
				s_ControlSchemeCache = new Dictionary<Type, IControlScheme>();

			if (!s_ControlSchemeCache.ContainsKey(typeof(T)))
				s_ControlSchemeCache[typeof(T)] = new T();

			return s_ControlSchemeCache[typeof(T)] as T;
		}

		#endregion
	}
}
