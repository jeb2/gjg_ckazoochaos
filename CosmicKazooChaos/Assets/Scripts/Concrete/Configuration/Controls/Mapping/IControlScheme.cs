﻿using Hydra.HydraCommon.PropertyAttributes.InputAttributes;

namespace CosmicKazooChaos.Concrete.Configuration.Controls.Mapping
{
	/// <summary>
	/// 	IControlScheme defines all of the input attributes for a control scheme.
	/// </summary>
	public interface IControlScheme
	{
		AxisInputAttribute horizontalInput { get; }
		AxisInputAttribute verticalInput { get; }
		AxisInputAttribute horizontalCameraInput { get; }
		AxisInputAttribute verticalCameraInput { get; }
		ButtonInputAttribute jumpInput { get; }
		ButtonInputAttribute action1Input { get; }
		ButtonInputAttribute action2Input { get; }
		ButtonInputAttribute action3Input { get; }
		ButtonInputAttribute switchInput { get; }
		ButtonInputAttribute dashInput { get; }
		ButtonInputAttribute snapCameraInput { get; }
		ButtonInputAttribute pauseInput { get; }
	}
}
