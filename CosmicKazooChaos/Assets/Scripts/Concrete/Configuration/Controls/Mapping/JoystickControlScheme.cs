﻿using Hydra.HydraCommon.PropertyAttributes.InputAttributes;

namespace CosmicKazooChaos.Concrete.Configuration.Controls.Mapping
{
	/// <summary>
	/// 	JoystickControlScheme provides the joystick input attributes for a control scheme.
	/// </summary>
	public class JoystickControlScheme : IControlScheme
	{
		private readonly AxisInputAttribute m_HorizontalInput;
		private readonly AxisInputAttribute m_VerticalInput;
		private readonly AxisInputAttribute m_HorizontalCameraInput;
		private readonly AxisInputAttribute m_VerticalCameraInput;
		private readonly ButtonInputAttribute m_JumpInput;
		private readonly ButtonInputAttribute m_Action1Input;
		private readonly ButtonInputAttribute m_Action2Input;
		private readonly ButtonInputAttribute m_Action3Input;
		private readonly ButtonInputAttribute m_SwitchInput;
		private readonly ButtonInputAttribute m_DashInput;
		private readonly ButtonInputAttribute m_SnapCameraInput;
		private readonly ButtonInputAttribute m_PauseInput;

		/// <summary>
		/// 	Initializes a new instance of the JoystickControlScheme class.
		/// </summary>
		public JoystickControlScheme()
		{
			m_HorizontalInput = new AxisInputAttribute("Controller Horizontal");
			m_VerticalInput = new AxisInputAttribute("Controller Vertical");
			m_HorizontalCameraInput = new AxisInputAttribute("Controller Camera x");
			m_VerticalCameraInput = new AxisInputAttribute("Controller Camera y");
			m_JumpInput = new ButtonInputAttribute("Controller Button 0");
			m_Action1Input = new ButtonInputAttribute("Controller Button 1");
			m_Action2Input = new ButtonInputAttribute("Controller Button 2");
			m_Action3Input = new ButtonInputAttribute("Controller Button 3");
			m_SwitchInput = new ButtonInputAttribute("Controller Button 4");
			m_DashInput = new ButtonInputAttribute("Controller Button 5");
			m_SnapCameraInput = new ButtonInputAttribute("Controller Button 6");
			m_PauseInput = new ButtonInputAttribute("Controller Button 9");
		}

		#region Properties

		public AxisInputAttribute horizontalInput { get { return m_HorizontalInput; } }
		public AxisInputAttribute verticalInput { get { return m_VerticalInput; } }
		public AxisInputAttribute horizontalCameraInput { get { return m_HorizontalCameraInput; } }
		public AxisInputAttribute verticalCameraInput { get { return m_VerticalCameraInput; } }
		public ButtonInputAttribute jumpInput { get { return m_JumpInput; } }
		public ButtonInputAttribute action1Input { get { return m_Action1Input; } }
		public ButtonInputAttribute action2Input { get { return m_Action2Input; } }
		public ButtonInputAttribute action3Input { get { return m_Action3Input; } }
		public ButtonInputAttribute switchInput { get { return m_SwitchInput; } }
		public ButtonInputAttribute dashInput { get { return m_DashInput; } }
		public ButtonInputAttribute snapCameraInput { get { return m_SnapCameraInput; } }
		public ButtonInputAttribute pauseInput { get { return m_PauseInput; } }

		#endregion
	}
}
