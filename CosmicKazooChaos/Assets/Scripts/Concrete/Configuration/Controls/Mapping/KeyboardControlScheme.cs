﻿using Hydra.HydraCommon.PropertyAttributes.InputAttributes;

namespace CosmicKazooChaos.Concrete.Configuration.Controls.Mapping
{
	/// <summary>
	/// 	KeyboardControlScheme provides the keyboard input attributes for a control scheme.
	/// </summary>
	public class KeyboardControlScheme : IControlScheme
	{
		private readonly AxisInputAttribute m_HorizontalInput;
		private readonly AxisInputAttribute m_VerticalInput;
		private readonly AxisInputAttribute m_HorizontalCameraInput;
		private readonly AxisInputAttribute m_VerticalCameraInput;
		private readonly ButtonInputAttribute m_JumpInput;
		private readonly ButtonInputAttribute m_Action1Input;
		private readonly ButtonInputAttribute m_Action2Input;
		private readonly ButtonInputAttribute m_Action3Input;
		private readonly ButtonInputAttribute m_SwitchInput;
		private readonly ButtonInputAttribute m_DashInput;
		private readonly ButtonInputAttribute m_SnapCameraInput;
		private readonly ButtonInputAttribute m_PauseInput;

		/// <summary>
		/// 	Initializes a new instance of the KeyboardControlScheme class.
		/// </summary>
		public KeyboardControlScheme()
		{
			m_HorizontalInput = new AxisInputAttribute("Horizontal");
			m_VerticalInput = new AxisInputAttribute("Vertical");
			m_HorizontalCameraInput = new AxisInputAttribute("Mouse X");
			m_VerticalCameraInput = new AxisInputAttribute("Mouse Y");
			m_JumpInput = new ButtonInputAttribute("Jump");
			m_Action1Input = new ButtonInputAttribute("Action 1");
			m_Action2Input = new ButtonInputAttribute("Action 2");
			m_Action3Input = new ButtonInputAttribute("Action 3");
			m_SwitchInput = new ButtonInputAttribute("Switch");
			m_DashInput = new ButtonInputAttribute("Dash");
			m_SnapCameraInput = new ButtonInputAttribute("SnapCamera");
			m_PauseInput = new ButtonInputAttribute("Pause");
		}

		#region Properties

		public AxisInputAttribute horizontalInput { get { return m_HorizontalInput; } }
		public AxisInputAttribute verticalInput { get { return m_VerticalInput; } }
		public AxisInputAttribute horizontalCameraInput { get { return m_HorizontalCameraInput; } }
		public AxisInputAttribute verticalCameraInput { get { return m_VerticalCameraInput; } }
		public ButtonInputAttribute jumpInput { get { return m_JumpInput; } }
		public ButtonInputAttribute action1Input { get { return m_Action1Input; } }
		public ButtonInputAttribute action2Input { get { return m_Action2Input; } }
		public ButtonInputAttribute action3Input { get { return m_Action3Input; } }
		public ButtonInputAttribute switchInput { get { return m_SwitchInput; } }
		public ButtonInputAttribute dashInput { get { return m_DashInput; } }
		public ButtonInputAttribute snapCameraInput { get { return m_SnapCameraInput; } }
		public ButtonInputAttribute pauseInput { get { return m_PauseInput; } }

		#endregion
	}
}
