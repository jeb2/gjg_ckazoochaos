﻿using System;
using CosmicKazooChaos.Abstract.Configuration;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Configuration
{
	/// <summary>
	/// 	Options defaults represents the default states for user configurable options.
	/// </summary>
	public class OptionsDefaults : AbstractConfig<OptionsDefaults>
	{
		[SerializeField] private CameraDefaults m_CameraDefaults;

		public CameraDefaults cameraDefaults { get { return m_CameraDefaults; } }
	}

	[Serializable]
	public class CameraDefaults
	{
		[SerializeField] private float m_SensitivityMin;
		[SerializeField] private float m_SensitivityMax;
		[SerializeField] private float m_HorizontalSensitivity;
		[SerializeField] private float m_VerticalSensitivity;

		public float sensitivityMin { get { return m_SensitivityMin; } }
		public float sensitivityMax { get { return m_SensitivityMax; } }
		public float horizontalSensitivity { get { return m_HorizontalSensitivity; } }
		public float verticalSensitivity { get { return m_VerticalSensitivity; } }
	}
}
