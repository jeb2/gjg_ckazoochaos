using CosmicKazooChaos.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.HitBoxes
{
	/// <summary>
	/// 	Hit box.
	/// </summary>
	[RequireComponent(typeof(Collider))]
	[DisallowMultipleComponent]
	public class HitBox : HydraMonoBehaviour
	{
		#region Messages

		/// <summary>
		/// 	OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);

			Debug.Log("i've hit " + other.name);
		}

		protected virtual void OnCharacterLocomotorCollision(CharacterLocomotorCollisionData collision) {}

		#endregion
	}
}
