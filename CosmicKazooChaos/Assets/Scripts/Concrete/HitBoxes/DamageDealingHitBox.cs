﻿using System.Collections.Generic;
using CosmicKazooChaos.API.Damageable;
using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.HitBoxes
{
	/// <summary>
	/// Hitbox for HazardTest object that damages the player upon touching it
	/// </summary>
	[RequireComponent(typeof(Collider))]
	[DisallowMultipleComponent]
	public class DamageDealingHitBox : HitBox
	{
		[SerializeField] private bool m_IgnorePlayer;
		[SerializeField] private bool m_IgnoreEnemy;

		#region Properties

		/// <summary>
		/// 		True if player collision should be ignored, false if not
		/// </summary>
		public bool ignorePlayer { get { return m_IgnorePlayer; } set { m_IgnorePlayer = value; } }

		/// <summary>
		/// 		True if enemy collision should be ignored, false if not
		/// </summary>
		public bool ignoreEnemy { get { return m_IgnorePlayer; } set { m_IgnorePlayer = value; } }

		#endregion

		#region Messages

		protected override void OnCharacterLocomotorCollision(CharacterLocomotorCollisionData collision)
		{
			if (m_IgnorePlayer)
			{
				if (collision.Controller.gameObject.tag == "Player")
					return;
			}

			if (m_IgnoreEnemy)
			{
				if (collision.Controller.gameObject.tag == "Enemy")
					return;
			}

			base.OnCharacterLocomotorCollision(collision);

			List<IDamageable> damageables = new List<IDamageable>(collision.Controller.gameObject.GetComponents<IDamageable>());
			foreach (IDamageable damageable in damageables)
				damageable.Damage(1, collision);
		}

		protected override void OnTriggerEnter(Collider other)
		{
			if (m_IgnorePlayer)
			{
				if (other.gameObject.tag == "Player")
					return;
			}

			if (m_IgnoreEnemy)
			{
				if (other.gameObject.tag == "Enemy")
					return;
			}

			CharacterLocomotorCollisionData collision = new CharacterLocomotorCollisionData(null,
																							other.ClosestPointOnBounds(transform.position));
			base.OnTriggerEnter(other);

			List<IDamageable> damageables = new List<IDamageable>(other.gameObject.GetComponents<IDamageable>());
			foreach (IDamageable damageable in damageables)
				damageable.Damage(1, collision);
		}

		#endregion
	}
}
