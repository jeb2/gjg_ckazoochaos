﻿using CosmicKazooChaos.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.HitBoxes
{
	[RequireComponent(typeof(Rigidbody))]
	public class Projectile : HydraMonoBehaviour
	{
		[SerializeField] private Timer m_DeletionTimer;
		[SerializeField] private bool m_UseGravity;
		[SerializeField] private Timer m_GravityTimer;

		protected override void OnEnable()
		{
			base.OnEnable();

			m_DeletionTimer.Reset();
			m_GravityTimer.Reset();
		}

		protected override void Update()
		{
			base.Update();

			if (m_DeletionTimer.complete)
				Destroy(gameObject);
			if (m_UseGravity && m_GravityTimer.complete)
				GetComponent<Rigidbody>().useGravity = true;
		}

		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);

			//TODO: MAKE SURE THE BULLETS CAN HIT OTHER ENEMIES TOO, MAYBE? PERHAPS THE BULLETS SHOULD BE CHILDREN TO THE BULLET'S ORIGIN ENEMY
			if (other.tag != "Enemy")
				Destroy(gameObject);
		}
	}
}
