﻿using CosmicKazooChaos.API.Damageable;
using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.HitBoxes
{
	[RequireComponent(typeof(Collider))]
	[DisallowMultipleComponent]
	public class BreakableHitBox : HitBox, IDamageable
	{
		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			this.gameObject.SetActive(false);
		}
	}
}
