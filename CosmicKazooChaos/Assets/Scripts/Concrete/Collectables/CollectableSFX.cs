﻿using System;
using Hydra.HydraCommon.PropertyAttributes;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Collectables
{
	/// <summary>
	/// 	CollectableSFX stores sound effects for game collectables.
	/// </summary>
	[Serializable]
	public class CollectableSFX
	{
		[SerializeField] private SoundEffectAttribute[] m_Collect;

		public SoundEffectAttribute[] collect { get { return m_Collect; } }
	}
}
