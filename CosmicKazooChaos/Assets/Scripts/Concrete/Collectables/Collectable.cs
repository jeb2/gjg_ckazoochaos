using CosmicKazooChaos.Concrete.Listeners.Collectables;
using CosmicKazooChaos.Concrete.Menus.HUD;
using CosmicKazooChaos.Concrete.Messaging;
using CosmicKazooChaos.Utils.Time;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Collectables
{
	[RequireComponent(typeof(CollectableAudioListener))]
	public class Collectable : HydraMonoBehaviour
	{
		[SerializeField] private HUDElement m_HUDElement;
		[SerializeField] private float m_PullSpeed = 1.0f;

		private bool m_Move = false;
		private Vector3 m_PullPoint = Vector3.zero;
		private float m_StartTime = 0;

		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);

			//If the trigger layer doesn't belong to player
			if (other.gameObject.layer != 12)
				return;

			m_HUDElement.count++;
			CollectableMessages.BroadcastOnCollected(gameObject);

			Destroy(gameObject);
		}

		protected override void Update()
		{
			if (!m_Move)
				return;

			// Collectable speeds up over distance.
			float distanceCovered = (GameTime.time - m_StartTime) * m_PullSpeed;
			this.transform.position = Vector3.MoveTowards(this.transform.position, m_PullPoint, distanceCovered);
		}

		public void SetPullPoint(Vector3 point)
		{
			if (!m_Move)
				m_StartTime = GameTime.time;

			m_PullPoint = point;
			m_Move = true;
		}
	}
}
