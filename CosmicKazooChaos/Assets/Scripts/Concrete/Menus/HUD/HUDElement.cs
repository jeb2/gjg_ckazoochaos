﻿using CosmicKazooChaos.Concrete.Utils;
using CosmicKazooChaos.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;
using System.Collections.Generic; //Required for Lists.
using System;

namespace CosmicKazooChaos.Concrete.Menus.HUD
{
	/// <summary>
	/// 	HUD element.
	/// </summary>
	public class HUDElement : HydraMonoBehaviour
	{
		private int m_Count = 0;
        List<Rect> numberRects = new List<Rect>();
		// Exactly what it sounds like. Keeps track of the GO's expected y-position when it's completely hidden or completely displaying.
		private Vector3 m_HiddenPosition;
		[SerializeField] private Vector3 m_DisplayPosition;

        [SerializeField] private int[] m_DisplayNumber; //Holds each digit of the number of held items in each slot
        [SerializeField] private int m_ySlot; //HUD "slot" which determines how low it is on the Y-axis of the screen
        [SerializeField] private float m_xHide; //How far off the X-axis the element needs to slide to be completely hidden
        [SerializeField] private float m_xSlide; //The current slide position on the X-axis
        bool m_toHide;//Should the HUD element be hiding?
        float delayTimer;//Counter to count how long the HUD element has been displayed

        //[SerializeField] private HUDDigit m_Hundreds;
        //[SerializeField] private HUDDigit m_Tens;
        [SerializeField] private HUDDigit m_Ones; //With the changes, only one of these is required
        [SerializeField] private Texture itemImage; //Image of the item type

       [SerializeField] private Timer m_DisplayTimer;
		[SerializeField] private float m_TransitionTime;

		[SerializeField] private Interpolater m_Interpolater;

		#region Properties

		/// <summary>
		/// 	Gets or sets the count.
		/// 
		/// 	Any time the count is changed using this method, the element begins to display.
		/// </summary>
		/// <value>The count.</value>
		public virtual int count
		{
			get { return m_Count; }
			set
			{
				m_Count = value;
				StartDisplaying();
				SetNumbers(m_Count);
			}
		}

		public float transitionDistance { get { return Vector3.Distance(m_HiddenPosition, m_DisplayPosition); } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_Interpolater == null)
				m_Interpolater = new Interpolater(Interpolater.Interpolant.ReverseQuadratic);

			if (m_DisplayTimer == null)
				m_DisplayTimer = new Timer();
		}

        /// <summary>
        /// 	Called before the first Update.
        /// </summary>
        protected override void Start()
		{
			base.Start();

			m_HiddenPosition = transform.position;
			SetNumbers(m_Count);
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();
            
            if(m_toHide)
            {
                if (m_xSlide < m_xHide)
                {
                    m_xSlide = m_xHide;
                }
                else if(m_xSlide != m_xHide)
                {
                    m_xSlide -= Time.deltaTime * Screen.height / 3;
                }
            }
            else
            {
                if (m_xSlide > 0)
                {
                    m_xSlide = 0;
                }
                else if (m_xSlide != 0)
                {
                    m_xSlide += Time.deltaTime * Screen.height;
                }
                else
                {
                    delayTimer += Time.deltaTime;
                    if(delayTimer >= m_TransitionTime)
                    {
                        m_toHide = true;
                    }
                }
            }

            //Vector3 nontarget;
            //Vector3 target;
            //float percent;
            /*if (m_DisplayTimer.complete)
			{
				nontarget = m_DisplayPosition;
				target = m_HiddenPosition;
				percent = Interpolater.CapZeroToOne(m_DisplayTimer.overtime / m_TransitionTime);
			}
			else
			{
				nontarget = m_HiddenPosition;
				target = m_DisplayPosition;
				percent = Interpolater.CapZeroToOne(m_DisplayTimer.elapsed / m_TransitionTime);
			}

			float offset = m_Interpolater.Interpolate(percent, 0, transitionDistance);

			Vector3 interpolatedPosition = Vector3.MoveTowards(nontarget, target, offset);

			if (Vector3.Distance(interpolatedPosition, target) < Vector3.Distance(transform.position, target))
				transform.position = interpolatedPosition;
			else
			{
				m_DisplayTimer.AddSeconds(
										  - m_Interpolater.InterpolateReverse(Vector3.Distance(transform.position, interpolatedPosition), 0,
																			  transitionDistance) * m_TransitionTime);
			}*/

            setNumberRects();
		}

        /// <summary>
        /// Is called 2-8 times per frame because Unity a shit, but allows for drawing things directly to the screen.
        /// </summary>
        protected override void OnGUI()
        {
            base.OnGUI();
            Texture toDisplay;//Could probably be made more efficient by assigning a list of textures in setNumberRect, but would take up more memory.
            for (int i = 0; i < numberRects.Count; i++)
            {
                if (i == 0)
                {
                    if(itemImage != null)
                    {
                        toDisplay = itemImage;
                    }
                    else
                    {
                        toDisplay = Texture2D.whiteTexture;
                    }
                }
                else
                {
                    toDisplay = m_Ones.m_DigitSprites[m_DisplayNumber[i-1]].texture;
                }
                GUI.DrawTexture(numberRects[i], toDisplay);
            }
        }

        /// <summary>
        /// Updates the rects in the numberRects lists
        /// </summary>
        void setNumberRects()
        {
            numberRects.Clear();
            for(int i = 0; i < m_DisplayNumber.Length+1; i++)
            {
                numberRects.Add(new Rect(m_xSlide+(Screen.height/10* i), Screen.height/100+Screen.height/10*m_ySlot, Screen.height/10, Screen.height/10));
            }
            // m_HiddenPosition = new Vector3(-(Screen.width), 0, 0);
            m_xHide = -(Screen.height / 10 * (m_DisplayNumber.Length + 1));
        }

        #endregion

            #region Methods

            /// <summary>
            /// 	Causes this HUDElement to start moving onto the screen for a duration. If no duration is
            /// 	entered, a default is used.
            /// </summary>
        public void StartDisplaying()
		{
			m_DisplayTimer.Reset();
		}

		/// <summary>
		/// 	Causes this element to start leaving the screen.
		/// </summary>
		public void StopDisplaying()
		{
			m_DisplayTimer.End();
		}

		/// <summary>
		/// 	Update the HUDDigits to display the appropriate values.
		/// </summary>
		/// <param name="n">The value to be displayed.</param>
		public void SetNumbers(int n)
		{
            m_toHide = false;
            delayTimer = 0;
            int numberOfDigits = (int)Math.Floor(Math.Log10(n) + 1);
            if (numberOfDigits < 1)
                numberOfDigits = 1;
            m_DisplayNumber = new int[numberOfDigits];
            int tempN = n;
            for(int i = numberOfDigits-1; i >= 0; i--)
            {
                m_DisplayNumber[i] = tempN % 10;
                tempN /= 10;
            }

			/*m_Ones.SetDigit(n);

			if (n >= 10)
				m_Tens.SetDigit(n / 10);
			else
				m_Tens.SetDigit(-1);

			if (n >= 100)
				m_Hundreds.SetDigit(n / 100);
			else
				m_Hundreds.SetDigit(-1);*/
		}

		#endregion
	}
}
