﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace CosmicKazooChaos.Concrete.Menus.HUD
{
	[RequireComponent(typeof(Image))]
	public class SpriteSwitcher : HydraMonoBehaviour
	{
		private Image m_CachedImage;

		#region Properties

		/// <summary>
		/// 	Gets the image.
		/// </summary>
		/// <value>The image.</value>
		public Image image { get { return m_CachedImage ?? (m_CachedImage = GetComponent<Image>()); } }

		#endregion

		#region Methods

		/// <summary>
		/// 	Sets the sprite.
		/// </summary>
		/// <param name="sprite">Sprite.</param>
		public void SetSprite(Sprite sprite)
		{
			UnclearSprite();
			image.sprite = sprite;
		}

		/// <summary>
		/// 	Clears the sprite.
		/// </summary>
		public void ClearSprite()
		{
			image.color = Color.clear;
		}

		/// <summary>
		/// 	Unclears the sprite.
		/// </summary>
		public void UnclearSprite()
		{
			image.color = Color.white;
		}

		#endregion
	}
}
