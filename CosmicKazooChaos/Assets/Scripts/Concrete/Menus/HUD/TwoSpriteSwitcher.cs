using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.HUD
{
	public class TwoSpriteSwitcher : SpriteSwitcher
	{
		[SerializeField] private Sprite m_Sprite0;
		[SerializeField] private Sprite m_Sprite1;

		#region Methods

		/// <summary>
		/// 	Swaps the sprite.
		/// </summary>
		public void SwapSprite()
		{
			image.sprite = (image.sprite == m_Sprite0) ? m_Sprite1 : m_Sprite0;
		}

		#endregion
	}
}
