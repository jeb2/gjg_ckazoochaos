using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.HUD
{
	public class HUDChecker : MonoBehaviour
	{
		public int count;
		public HUDElement element;

		public void OnTriggerEnter()
		{
			element.count = count;
			Debug.Log("Collided.");
		}
	}
}
