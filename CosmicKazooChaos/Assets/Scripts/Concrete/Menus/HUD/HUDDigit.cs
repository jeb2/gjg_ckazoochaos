﻿using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.HUD
{
	public class HUDDigit : SpriteSwitcher
	{
		[SerializeField] public Sprite[] m_DigitSprites = new Sprite[10];//Made public so it can be accessed by HUDElement. Perhaps change this to a Texture[]?

		/// <summary>
		/// 	Sets the digit.
		/// </summary>
		/// <param name="n">The digit to be displayed.</param>
		public void SetDigit(int n)
		{
			if (n >= 0)
				SetSprite(m_DigitSprites[n % m_DigitSprites.Length]);
			else
				ClearSprite();
		}
	}
}
