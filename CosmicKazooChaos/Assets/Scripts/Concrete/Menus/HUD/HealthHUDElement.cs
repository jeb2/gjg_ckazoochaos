using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.HUD
{
	public class HealthHUDElement : HUDElement
	{
		private int m_Max = 3;
		[SerializeField] private TwoSpriteSwitcher[] m_Toasts = new TwoSpriteSwitcher[10];
		[SerializeField] private TwoSpriteSwitcher m_Portraits;

		#region Properties

		/// <summary>
		/// 	Gets or sets the maximum health.
		/// </summary>
		/// <value>The max.</value>
		public int max
		{
			get { return m_Max; }
			set
			{
				m_Max = value;
				// Make visible only a number of toasts equal to the maximum health.
				for (int i = 0; i < m_Max && i < m_Toasts.Length; i++)
					m_Toasts[i].UnclearSprite();
				for (int i = m_Max; i < m_Toasts.Length; i++)
					m_Toasts[i].ClearSprite();
			}
		}

		/// <summary>
		/// 	Gets or sets the count.
		/// 
		/// 	Any time the count is changed using this method, the element begins to display.
		/// </summary>
		/// <value>The count.</value>
		public override int count
		{
			get { return base.count; }
			set
			{
				int c = count;
				base.count = value;
				UpdateToasts(c);
			}
		}

		#endregion

		public void UpdateToasts(int previousCount)
		{
			int upperBound = (int)HydraMathUtils.Min(HydraMathUtils.Max(previousCount, count), m_Toasts.Length);
			for (int i = (int)HydraMathUtils.Min(previousCount, count, 0); i < upperBound; i++)
				m_Toasts[i].SwapSprite();
		}

		public void SwitchPortraits()
		{
			m_Portraits.SwapSprite();
		}
	}
}
