﻿using CosmicKazooChaos.Abstract.Menus.States;

namespace CosmicKazooChaos.Concrete.Menus.States
{
	/// <summary>
	/// 	Loading menu state.
	/// </summary>
	public class LoadingMenuState : AbstractMenuState {}
}
