﻿using CosmicKazooChaos.Abstract.Menus.States;
using CosmicKazooChaos.Concrete.Menus.Panels;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.States
{
	/// <summary>
	/// 	Credits menu state.
	/// </summary>
	public class CreditsMenuState : AbstractMenuState
	{
		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			(panelInstance as CreditsMenuPanel).ResetPosition();
		}
	}
}
