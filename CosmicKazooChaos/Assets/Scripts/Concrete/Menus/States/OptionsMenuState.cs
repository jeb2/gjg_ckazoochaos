﻿using CosmicKazooChaos.Abstract.Menus.States;

namespace CosmicKazooChaos.Concrete.Menus.States
{
	/// <summary>
	/// 	Options menu state.
	/// </summary>
	public class OptionsMenuState : AbstractMenuState {}
}
