﻿using CosmicKazooChaos.Abstract.Menus.States;

namespace CosmicKazooChaos.Concrete.Menus.States
{
	/// <summary>
	/// 	None menu state.
	/// </summary>
	public class NoneMenuState : AbstractMenuState {}
}
