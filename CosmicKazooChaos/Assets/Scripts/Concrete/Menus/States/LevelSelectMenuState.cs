﻿using CosmicKazooChaos.Abstract.Menus.States;

namespace CosmicKazooChaos.Concrete.Menus.States
{
	/// <summary>
	/// 	Level select menu state.
	/// </summary>
	public class LevelSelectMenuState : AbstractMenuState {}
}
