﻿using CosmicKazooChaos.Abstract.Menus.States;

namespace CosmicKazooChaos.Concrete.Menus.States
{
	/// <summary>
	/// 	Start menu state.
	/// </summary>
	public class StartMenuState : AbstractMenuState {}
}
