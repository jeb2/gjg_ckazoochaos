﻿using System;
using CosmicKazooChaos.Abstract.Menus.States;
using CosmicKazooChaos.Abstract.StateMachine;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.States
{
	/// <summary>
	/// 	Menu state machine.
	/// </summary>
	[Serializable]
	public class MenuStateMachine : AbstractStateMachine<AbstractMenuState>
	{
		[SerializeField] private AbstractMenuState m_InitialState;

		#region Properties

		/// <summary>
		/// 	Gets the initial state.
		/// </summary>
		/// <value>The initial state.</value>
		public override AbstractMenuState initialState { get { return m_InitialState; } }

		#endregion
	}
}
