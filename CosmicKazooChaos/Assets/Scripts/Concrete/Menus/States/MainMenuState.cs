﻿using CosmicKazooChaos.Abstract.Menus.States;

namespace CosmicKazooChaos.Concrete.Menus.States
{
	/// <summary>
	/// 	Main menu state.
	/// </summary>
	public class MainMenuState : AbstractMenuState {}
}
