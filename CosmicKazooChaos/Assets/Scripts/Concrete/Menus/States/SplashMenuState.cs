﻿using CosmicKazooChaos.Abstract.Menus.States;
using CosmicKazooChaos.Concrete.Menus.Panels;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.States
{
	/// <summary>
	/// 	Splash menu state.
	/// </summary>
	public class SplashMenuState : AbstractMenuState
	{
		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractMenuState GetNextState(MonoBehaviour parent)
		{
			SplashMenuPanel panel = panelInstance as SplashMenuPanel;

			if (panel.isComplete)
				return panel.startMenuState;

			return base.GetNextState(parent);
		}
	}
}
