﻿using CosmicKazooChaos.Abstract.Menus.States;
using CosmicKazooChaos.Utils.Time;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.States
{
	/// <summary>
	/// 	Pause menu state.
	/// </summary>
	public class PauseMenuState : AbstractMenuState {

		public override void OnPush(UnityEngine.MonoBehaviour parent)
		{
			base.OnEnter(parent);
			GameTime.paused = true;
		}

		public override void OnPop(UnityEngine.MonoBehaviour parent)
		{
			base.OnExit(parent);
			GameTime.paused = false;
		}
	}
}
