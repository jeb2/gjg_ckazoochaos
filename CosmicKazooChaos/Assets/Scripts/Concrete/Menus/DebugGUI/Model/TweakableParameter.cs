﻿using System;
using System.Reflection;

namespace CosmicKazooChaos.Concrete.Menus.DebugGUI.Model
{
	public abstract class TweakableParameter
	{
		protected object owner;
		private string m_Context;

		protected TweakableParameter(object owner, string context)
		{
			this.owner = owner;
			this.m_Context = context;
		}

		public abstract string Name { get; }

		public string Context { get { return m_Context; } }

		public abstract Type Type { get; }

		public abstract object GetValue();

		public abstract T GetValue<T>();

		public abstract void SetValue<T>(T Value);
	}

	/// <summary>
	/// This class pairs a PropertyInfo with a an instance of an object that has that property.
	/// It has somewhat typesafe methods for getting and setting the value.
	/// </summary>
	public class TweakablePropertyParameter : TweakableParameter
	{
		private readonly PropertyInfo m_Property;

		/// <summary>
		/// Creates a new TweakableParameter.  If property does not belong to the type of owner, an ArgumentException is thrown.
		/// </summary>
		/// <param name="property">The property of owner that is to be tweaked.</param>
		/// <param name="owner">The instance of an object whose property is to be tweaked.</param>
		public TweakablePropertyParameter(PropertyInfo property, object owner, string context) : base(owner, context)
		{
			if (!(owner.GetType().IsSubclassOf(property.DeclaringType) || owner.GetType() == property.DeclaringType))
			{
				throw new ArgumentException(String.Format("The type {0} does not have the property {1}", owner.GetType(),
														  property.Name));
			}
			m_Property = property;
		}

		/// <summary>
		/// The Name of the parameter.
		/// </summary>
		public override string Name { get { return m_Property.Name; } }

		public override Type Type { get { return m_Property.PropertyType; } }

		public override object GetValue()
		{
			return m_Property.GetValue(owner, null);
		}

		public override T GetValue<T>()
		{
			if (typeof(T) != m_Property.PropertyType)
			{
				throw new InvalidCastException(String.Format("Cannot get a value of type {0} when the property is of type {1}",
															 typeof(T).Name, m_Property.PropertyType.Name));
			}

			return (T)m_Property.GetValue(owner, null);
		}

		public override void SetValue<T>(T value)
		{
			if (typeof(T) != m_Property.PropertyType)
			{
				throw new InvalidCastException(String.Format("Cannot get a value of type {0} when the property is of type {1}",
															 typeof(T).Name, m_Property.PropertyType.Name));
			}

			m_Property.SetValue(owner, value, null);
		}
	}

	public class TweakableFieldParameter : TweakableParameter
	{
		private readonly FieldInfo m_Field;

		/// <summary>
		/// Creates a new TweakableFieldParameter.  If field does not belong to the type of owner, an ArgumentException is thrown.
		/// </summary>
		/// <param name="field">The field of owner that is to be tweaked.</param>
		/// <param name="owner">The instance of an object whose field is to be tweaked.</param>
		public TweakableFieldParameter(FieldInfo field, object owner, string context) : base(owner, context)
		{
			if (!(owner.GetType().IsSubclassOf(field.DeclaringType) || owner.GetType() == field.DeclaringType))
				throw new ArgumentException(String.Format("The type {0} does not have the field {1}", owner.GetType(), field.Name));

			m_Field = field;
		}

		/// <summary>
		/// The Name of the parameter.
		/// </summary>
		public override string Name { get { return m_Field.Name; } }

		public override Type Type { get { return m_Field.FieldType; } }

		public override object GetValue()
		{
			return m_Field.GetValue(owner);
		}

		public override T GetValue<T>()
		{
			if (typeof(T) != m_Field.FieldType)
			{
				throw new InvalidCastException(String.Format("Cannot get a value of type {0} when the property is of type {1}",
															 typeof(T).Name, m_Field.FieldType.Name));
			}

			return (T)m_Field.GetValue(owner);
		}

		public override void SetValue<T>(T value)
		{
			if (typeof(T) != m_Field.FieldType)
			{
				throw new InvalidCastException(String.Format("Cannot get a value of type {0} when the property is of type {1}",
															 typeof(T).Name, m_Field.FieldType.Name));
			}

			m_Field.SetValue(owner, value);
		}
	}
}
