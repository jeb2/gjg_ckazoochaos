﻿using UnityEngine.UI;

namespace CosmicKazooChaos.Concrete.Menus.DebugGUI.View
{
	/// <summary>
	/// Component that binds a <c ref="TweakableParameter">TweakableParameter</c> to a numeric InputField.
	/// When the Input field's value is changed, it will update the Parameter, and the value in the InputField will be updated when the OnGUI message is received.
	/// </summary>
	public class BoolParameterView : AbstractParameterView<bool>
	{
		public Toggle toggle;

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			toggle.onValueChanged.AddListener(OnValueChanged);
		}

		protected override void Update()
		{
			base.Update();

			if (parameter != null)
				toggle.isOn = parameter.GetValue<bool>();
		}
	}
}
