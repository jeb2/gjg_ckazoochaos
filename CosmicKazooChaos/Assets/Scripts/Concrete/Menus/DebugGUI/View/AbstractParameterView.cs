﻿using System;
using CosmicKazooChaos.Concrete.Menus.DebugGUI.Model;
using Hydra.HydraCommon.Abstract;

namespace CosmicKazooChaos.Concrete.Menus.DebugGUI.View
{
	public class AbstractParameterView<T> : HydraMonoBehaviour
	{
		private TweakableParameter m_Parameter;

		public TweakableParameter parameter
		{
			get { return m_Parameter; }
			set
			{
				if (value.Type != typeof(T))
					throw new ArgumentException(string.Format("Parameter must be a parameter of type {0}", typeof(T).Name));
				m_Parameter = value;
			}
		}

		public void OnValueChanged(T value)
		{
			parameter.SetValue(value);
		}
	}
}
