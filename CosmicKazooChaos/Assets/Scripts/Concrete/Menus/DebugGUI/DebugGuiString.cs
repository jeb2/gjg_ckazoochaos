﻿using System;
using Hydra.HydraCommon.Abstract;
using UnityEngine.Events;
using UnityEngine.UI;

namespace CosmicKazooChaos.Concrete.Menus.DebugGUI
{
	// Non-generic alias so that it will show up in the editor.
	[Serializable]
	public class StringEvent : UnityEvent<string> {}

	public class DebugGuiString : HydraMonoBehaviour
	{
		/// <summary>
		/// The text area that should contain the serialised debug settings
		/// </summary>
		public InputField textField;

		/// <summary>
		/// When the user clicks this button, the settings in the string should be applied to the game.
		/// </summary>
		public Button applyButton;

		/// <summary>
		/// This event fires when the the settings in the string should be applied to the game settings.
		/// </summary>
		public StringEvent applySettingsEvent;

		/// <summary>
		/// Set the serialized setting data so that the text in the textField is updated.
		/// </summary>
		public string String { get { return textField.text; } set { textField.text = value; } }

		protected override void Start()
		{
			base.Start();
			applyButton.onClick.AddListener(() => applySettingsEvent.Invoke(textField.text));
		}
	}
}
