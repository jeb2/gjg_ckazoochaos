﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using CosmicKazooChaos.Concrete.Menus.DebugGUI.Model;
using CosmicKazooChaos.Concrete.Menus.DebugGUI.View;
using CosmicKazooChaos.Concrete.Player;
using CosmicKazooChaos.Utils;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace CosmicKazooChaos.Concrete.Menus.DebugGUI
{
	/// <summary>
	/// 	GUI DEBUG CLASS
	/// </summary>
	public class DebugGuiComponent : HydraMonoBehaviour
	{
		[SerializeField] private string debugString;

		/// <summary>
		/// These objects will be searched for [Tweakable] fields and properties.
		/// </summary>
		[SerializeField] public List<UnityEngine.Object> otherObjects;

		/// <summary>
		/// This is the main container for the debug gui.
		/// </summary>
		public GameObject hScrollBox;

		public GameObject vScrollBoxPrefab;
		public GameObject floatFieldPrefab;
		public GameObject boolFieldPrefab;

		// Cache
		private Dictionary<string, List<TweakableParameter>> tweakableParameters;

		#region public methods

		public bool IsVisible()
		{
			return hScrollBox.activeInHierarchy;
		}

		public void Hide()
		{
			hScrollBox.SetActive(false);
		}

		public void Show()
		{
			hScrollBox.SetActive(true);
		}

		#endregion

		#region Messages

		protected override void Start()
		{
			tweakableParameters = new Dictionary<string, List<TweakableParameter>>();

			foreach (GameObject obj in GameObject.FindObjectsOfType<GameObject>())
			{
				foreach (Component c in obj.GetComponents<Component>())
					AddParameters(c, obj.name);
			}

			foreach (UnityEngine.Object obj in otherObjects)
			{
				if (obj != null)
					AddParameters(obj, obj.name);
			}

			Transform guiContentTransform = hScrollBox.transform.Find("ScrollView/Content");

			foreach (string category in tweakableParameters.Keys)
			{
				GameObject categoryObject = ObjectUtils.Instantiate(vScrollBoxPrefab);

				//Set the category name
				Transform categoryTextTransform = categoryObject.transform.FindChild("Text");
				categoryTextTransform.GetComponent<Text>().text = category;

				//Add the parameters
				Transform categoryContentTransform = categoryObject.transform.Find("ScrollView/Content");
				foreach (TweakableParameter param in tweakableParameters[category])
				{
					GameObject field = MakeField(param);
					field.transform.SetParent(categoryContentTransform);
				}

				categoryObject.transform.SetParent(guiContentTransform);
			}

			hScrollBox.GetComponent<DebugGuiString>().applySettingsEvent.AddListener(LoadSettings);

			OnSettingChanged();

			hScrollBox.SetActive(false);
		}

		/// <summary>
		/// 	Called to draw to the GUI.
		/// </summary>
		protected override void OnGUI()
		{
			base.OnGUI();

			hScrollBox.SetActive(enabled);
		}

		protected override void OnEnable()
		{
			base.OnEnable();

			if (otherObjects == null)
				otherObjects = new List<UnityEngine.Object>();
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Creates an appropriate debug gui editor field, based on the type of the parameter,
		/// and sets the text and value appropriately.
		/// </summary>
		/// <param name="param"></param>
		/// <returns></returns>
		private GameObject MakeField(TweakableParameter param)
		{
			Type paramType = param.Type;

			if (paramType == typeof(float))
			{
				GameObject field = ObjectUtils.Instantiate(floatFieldPrefab);
				Transform textTransform = field.transform.FindChild("Text");
				Text label = textTransform.GetComponent<Text>();
				label.text = param.Context + "." + param.Name.Replace("m_", "");

				Transform editTransform = field.transform.FindChild("FloatEdit");
				label = editTransform.GetComponent<Text>();
				label.text = param.GetValue<float>().ToString();

				FloatParameterView paramComponent = field.GetComponent<FloatParameterView>();
				paramComponent.parameter = param;
				paramComponent.onValueChange.AddListener(OnSettingChanged);

				return field;
			}

			if (paramType == typeof(bool))
			{
				GameObject field = ObjectUtils.Instantiate(boolFieldPrefab);
				Transform textTransform = field.transform.FindChild("Text");
				Text label = textTransform.GetComponent<Text>();
				label.text = param.Context + "." + param.Name.Replace("m_", "");

				Transform editTransform = field.transform.FindChild("Toggle");
				Toggle toggle = editTransform.GetComponent<Toggle>();
				toggle.isOn = param.GetValue<bool>();

				BoolParameterView paramComponent = field.GetComponent<BoolParameterView>();
				paramComponent.parameter = param;

				return field;
			}

			throw new NotImplementedException(string.Format("Type {0} not supported.", param.Type));
		}

		/// <summary>
		/// 	Gets the GUI rect including the text field, in screen space
		/// </summary>
		/// <returns>The extended GUI rect, including text field</returns>
		public Rect GetGUIRect()
		{
			RectTransform rectTransform = hScrollBox.GetComponent<RectTransform>();

			return new Rect(rectTransform.rect.xMin + rectTransform.position.x,
							rectTransform.rect.yMin + rectTransform.position.y, rectTransform.rect.width, rectTransform.rect.height);
		}

		/// <summary>
		/// Adds all the tweakable parameters from the given object to tweakableParameters.
		/// </summary>
		/// <param name="owner">Any object that has tweakable parameters.</param>
		private void AddParameters(object owner, string context)
		{
			PropertyInfo[] properties =
				owner.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static |
											  BindingFlags.Instance);
			foreach (PropertyInfo prop in properties)
			{
				if (prop.IsDefined(typeof(TweakableAttribute), false))
				{
					TweakableAttribute attribute = prop.GetCustomAttributes(typeof(TweakableAttribute), false)[0] as TweakableAttribute;
					TweakableParameter param = new TweakablePropertyParameter(prop, owner, context);
					AddParameter(param, attribute.category);
				}
			}

			FieldInfo[] fields =
				owner.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
			foreach (FieldInfo field in fields)
			{
				if (field.IsDefined(typeof(TweakableAttribute), false))
				{
					TweakableAttribute attribute =
						field.GetCustomAttributes(typeof(TweakableAttribute), false)[0] as TweakableAttribute;
					TweakableParameter param = new TweakableFieldParameter(field, owner, context);
					AddParameter(param, attribute.category);
				}
			}
		}

		/// <summary>
		/// Adds a single tweakable parameter to tweableParameters, handling the creation of a category list properly.
		/// </summary>
		/// <param name="param">The paramter that may be tweaked.</param>
		/// <param name="category">The category of the parameter.</param>
		private void AddParameter(TweakableParameter param, string category)
		{
			if (tweakableParameters.ContainsKey(category))
				tweakableParameters[category].Add(param);
			else
			{
				List<TweakableParameter> l = new List<TweakableParameter>();
				l.Add(param);
				tweakableParameters.Add(category, l);
			}
		}

		private void OnSettingChanged()
		{
			string settings = SaveSettings();
			hScrollBox.GetComponent<DebugGuiString>().String = settings;
		}

		/// <summary>
		/// Loads the settings specified by the given string, and applies the values to the parameters.
		/// </summary>
		/// <param name="settings"></param>
		public void LoadSettings(string settings)
		{
			foreach (SerialisedSetting setting in TokeniseSettings(settings))
				LoadSetting(setting);
		}

		private void LoadSetting(SerialisedSetting setting)
		{
			if (tweakableParameters.ContainsKey(setting.category))
			{
				foreach (TweakableParameter param in tweakableParameters[setting.category])
				{
					if (param.Name.Equals(setting.name) && param.Context.Equals(setting.context))
						TrySetParameterValue(param, setting.value);
				}
			}
		}

		/// <summary>
		/// Attempts to parse and set the value of the parameter, according to its type.
		/// If parsing fails or the parameter is an unknown tpye, return false.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="value"></param>
		private static bool TrySetParameterValue(TweakableParameter param, string value)
		{
			float floatValue;
			bool boolValue;

			if (param.Type == typeof(float) && float.TryParse(value, out floatValue))
			{
				param.SetValue(floatValue);
				return true;
			}

			if (param.Type == typeof(bool) && bool.TryParse(value, out boolValue))
			{
				param.SetValue(boolValue);
				return true;
			}

			return false;
		}

		private struct SerialisedSetting
		{
			public string category;
			public string context;
			public string name;
			public string value;
		}

		private enum TokenState
		{
			Category,
			Context,
			Name,
			Value
		}

		private static IEnumerable<SerialisedSetting> TokeniseSettings(string settings)
		{
			int catStart = 0, catEnd = 0, conStart = 0, conEnd = 0, nameStart = 0, nameEnd = 0, valueStart = 0, valueEnd = 0;
			TokenState state = TokenState.Category;
			for (int i = 0; i < settings.Length; i++)
			{
				switch (state)
				{
					case TokenState.Category:
						if (settings[i] == ':')
						{
							catEnd = i;
							state = TokenState.Context;
							conStart = i + 1;
						}
						break;
					case TokenState.Context:
						if (settings[i] == '.')
						{
							conEnd = i;
							state = TokenState.Name;
							nameStart = i + 1;
						}
						break;
					case TokenState.Name:
						if (settings[i] == '=')
						{
							nameEnd = i;
							state = TokenState.Value;
							valueStart = i + 1;
						}
						break;
					case TokenState.Value:
						if (settings[i] == ';')
						{
							valueEnd = i;
							yield return new SerialisedSetting
							{
								category = settings.Substring(catStart, catEnd - catStart),
								context = settings.Substring(conStart, conEnd - conStart),
								name = settings.Substring(nameStart, nameEnd - nameStart),
								value = settings.Substring(valueStart, valueEnd - valueStart)
							};
							state = TokenState.Category;
							catStart = i + 1;
						}
						break;
				}
			}
		}

		/// <summary>
		/// Serialises the current parameters to a string.
		/// </summary>
		/// <returns>The current parameters.</returns>
		public string SaveSettings()
		{
			StringBuilder sb = new StringBuilder();
			foreach (string category in tweakableParameters.Keys)
			{
				foreach (TweakableParameter p in tweakableParameters[category])
				{
					sb.Append(category);
					sb.Append(':');
					sb.Append(p.Context);
					sb.Append('.');
					sb.Append(p.Name);
					sb.Append('=');
					sb.Append(p.GetValue());
					sb.Append(';');
				}
			}
			return sb.ToString();
		}

		#endregion
	}
}
