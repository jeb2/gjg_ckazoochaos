﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace CosmicKazooChaos.Concrete.Menus
{
	/// <summary>
	/// 	Quits the application when pressed.
	/// </summary>
	[RequireComponent(typeof(Button))]
	public class QuitButton : HydraMonoBehaviour
	{
		public static string WEBPLAYER_QUIT_URL = "http://google.com";

		private Button m_CachedButton;

		/// <summary>
		/// 	Gets the button.
		/// </summary>
		/// <value>The button.</value>
		public Button button { get { return m_CachedButton ?? (m_CachedButton = GetComponent<Button>()); } }

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			button.onClick.AddListener(OnButtonClicked);
		}

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			button.onClick.RemoveListener(OnButtonClicked);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Called when the button is clicked.
		/// </summary>
		private void OnButtonClicked()
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
			Application.OpenURL(WEBPLAYER_QUIT_URL);
#else
			Application.Quit();
#endif
		}

		#endregion
	}
}
