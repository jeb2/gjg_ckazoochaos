﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace CosmicKazooChaos.Concrete.Menus
{
	/// <summary>
	/// 	Provides a UI object for playing MovieTextures.
	/// </summary>
	[RequireComponent(typeof(RawImage))]
	[RequireComponent(typeof(AudioSource))]
	[RequireComponent(typeof(AspectRatioFitter))]
	public class UiMovie : HydraMonoBehaviour
	{
		[SerializeField] private MovieTexture m_Movie;

		private RawImage m_CachedRawImage;
		private AudioSource m_CachedAudioSource;
		private AspectRatioFitter m_CachedAspectRatioFitter;

		#region Properties

		/// <summary>
		/// 	Gets the raw image.
		/// </summary>
		/// <value>The raw image.</value>
		public RawImage rawImage { get { return m_CachedRawImage ?? (m_CachedRawImage = GetComponent<RawImage>()); } }

		/// <summary>
		/// 	Gets the audio source.
		/// </summary>
		/// <value>The audio source.</value>
		public AudioSource audioSource
		{
			get { return m_CachedAudioSource ?? (m_CachedAudioSource = GetComponent<AudioSource>()); }
		}

		/// <summary>
		/// 	Gets the aspect ratio fitter.
		/// </summary>
		/// <value>The aspect ratio fitter.</value>
		public AspectRatioFitter aspectRatioFitter
		{
			get { return m_CachedAspectRatioFitter ?? (m_CachedAspectRatioFitter = GetComponent<AspectRatioFitter>()); }
		}

		/// <summary>
		/// 	Gets or sets the movie.
		/// </summary>
		/// <value>The movie.</value>
		public MovieTexture movie
		{
			get { return m_Movie; }
			set
			{
				m_Movie = value;
				UpdateComponents();
			}
		}

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			UpdateComponents();
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Stop the movie and audio.
		/// </summary>
		public void Stop()
		{
			m_Movie.Stop();
			audioSource.Stop();
		}

		/// <summary>
		/// 	Play the movie and audio.
		/// </summary>
		public void Play()
		{
			m_Movie.Play();
			audioSource.Play();
		}

		/// <summary>
		/// 	Updates the components.
		/// </summary>
		private void UpdateComponents()
		{
			if (m_Movie != null)
			{
				rawImage.texture = m_Movie;
				rawImage.color = Color.white;

				audioSource.clip = m_Movie.audioClip;
				aspectRatioFitter.aspectRatio = (float)m_Movie.width / m_Movie.height;
			}
			else
			{
				rawImage.texture = null;
				rawImage.color = Color.black;

				audioSource.clip = null;
				aspectRatioFitter.aspectRatio = 1;
			}
		}

		#endregion
	}
}
