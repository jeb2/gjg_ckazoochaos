﻿using CosmicKazooChaos.Abstract.Menus.Panels;
using CosmicKazooChaos.Abstract.Menus.States;
using CosmicKazooChaos.Concrete.Configuration.Controls.Mapping;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.Panels
{
	/// <summary>
	/// 	Pause menu panel.
	/// </summary>
	public class PauseMenuPanel : AbstractMenuPanel {

		[SerializeField] private AbstractMenuState m_OptionsMenu;

		protected override void Update()
		{
			base.Update();

			if (InputMapping.pause.GetButtonDown())
			{
				PreviousMenu();
			}
		}

		public void OnResumeClick()
		{
			PreviousMenu();
		}

		public void OnOptionsClick()
		{
			NextMenu(m_OptionsMenu);
		}

	}
}
