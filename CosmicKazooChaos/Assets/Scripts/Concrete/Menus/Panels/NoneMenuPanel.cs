﻿using CosmicKazooChaos.Abstract.Menus.Panels;

namespace CosmicKazooChaos.Concrete.Menus.Panels
{
	/// <summary>
	/// 	None menu panel.
	/// </summary>
	public class NoneMenuPanel : AbstractMenuPanel {}
}
