using CosmicKazooChaos.Abstract.Menus.Panels;
using CosmicKazooChaos.Concrete.Menus.States;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.Panels
{
	/// <summary>
	/// 	Splash menu panel.
	/// </summary>
	public class SplashMenuPanel : AbstractMenuPanel
	{
		[SerializeField] private StartMenuState m_StartMenuState;
		[SerializeField] private UiMovie m_UiMovie;
		[SerializeField] private MovieTexture[] m_Movies;

		private int m_Index;

		#region Properties

		/// <summary>
		/// 	Gets the start menu state.
		/// </summary>
		/// <value>The start menu state.</value>
		public StartMenuState startMenuState { get { return m_StartMenuState; } }

		/// <summary>
		/// 	Gets a value indicating whether this SplashMenuPanel is complete.
		/// </summary>
		/// <value><c>true</c> if is complete; otherwise, <c>false</c>.</value>
		public bool isComplete { get { return m_Index >= m_Movies.Length; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			Play();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (Input.anyKeyDown)
				Skip();

			if (!isComplete)
			{
				if (m_Movies[m_Index] == null || !m_Movies[m_Index].isPlaying)
					Skip();
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Skip the current splash screen.
		/// </summary>
		private void Skip()
		{
			Stop();
			Play();
		}

		/// <summary>
		/// 	Stop the current splash screen, queues the next splash screen.
		/// </summary>
		private void Stop()
		{
			if (isComplete)
				return;

			if (m_UiMovie.movie != null)
				m_UiMovie.Stop();

			m_Index++;

			if (isComplete)
				NextMenu(m_StartMenuState);
		}

		/// <summary>
		/// 	Play the next queued splash screen.
		/// </summary>
		private void Play()
		{
			if (isComplete)
				return;

			m_UiMovie.movie = m_Movies[m_Index];

			if (m_UiMovie.movie == null)
				Debug.LogError("Trying to play null movie.");
			else
				m_UiMovie.Play();
		}

		#endregion
	}
}
