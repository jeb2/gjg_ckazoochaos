﻿using System;
using CosmicKazooChaos.Abstract.Menus.Panels;
using CosmicKazooChaos.Concrete.Configuration;
using CosmicKazooChaos.Concrete.Utils.UI;
using Hydra.HydraCommon.EventArguments;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.Panels.Options
{
	/// <summary>
	/// 	Options menu panel.
	/// </summary>
	public class OptionsMenuPanel : AbstractMenuPanel
	{
		[SerializeField] private OptionsSlider m_OptionsSliderPrefab;
		[SerializeField] private RectTransform m_Layout;
		[SerializeField] private ButtonWrapper m_BackButton;

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_Layout.childCount == 0)
				PopulateOptions();

			m_BackButton.onClickedCallback += OnBackButtonClicked;
		}

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			m_BackButton.onClickedCallback -= OnBackButtonClicked;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Populates the options.
		/// </summary>
		private void PopulateOptions()
		{
			float min = OptionsDefaults.instance.cameraDefaults.sensitivityMin;
			float max = OptionsDefaults.instance.cameraDefaults.sensitivityMax;
			float horizontal = Configuration.Options.cameraHorizontalSensitivity;
			float vertical = Configuration.Options.cameraVerticalSensitivity;

			BuildSlider("Camera Horizontal Sensitivity", min, max, horizontal, OnCameraHorizontalSliderChanged);
			BuildSlider("Camera Vertical Sensitivity", min, max, vertical, OnCameraVerticalSliderChanged);
		}

		/// <summary>
		/// 	Builds a slider.
		/// </summary>
		/// <returns>The slider.</returns>
		/// <param name="name">Name.</param>
		/// <param name="min">Minimum.</param>
		/// <param name="max">Max.</param>
		/// <param name="sliderValue">Slider value.</param>
		/// <param name="callback">Callback.</param>
		private OptionsSlider BuildSlider(string name, float min, float max, float sliderValue,
										  EventHandler<EventArg<float>> callback)
		{
			OptionsSlider instance = ObjectUtils.Instantiate(m_OptionsSliderPrefab);

			RectTransform instanceTransform = instance.transform as RectTransform;
			instanceTransform.SetParent(m_Layout, false);

			instance.title = name;
			instance.min = min;
			instance.max = max;
			instance.sliderValue = sliderValue;

			instance.onSliderValueChanged += callback;

			return instance;
		}

		private void OnCameraHorizontalSliderChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.cameraHorizontalSensitivity = eventArgs.data;
		}

		private void OnCameraVerticalSliderChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.cameraVerticalSensitivity = eventArgs.data;
		}

		/// <summary>
		/// 	Called when the back button is clicked.
		/// </summary>
		private void OnBackButtonClicked(object sender, EventArgs eventArgs)
		{
			Configuration.Options.Save();

			PreviousMenu();
		}

		#endregion
	}
}
