﻿using System;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.EventArguments;
using UnityEngine;
using UnityEngine.UI;

namespace CosmicKazooChaos.Concrete.Menus.Panels.Options
{
	/// <summary>
	/// 	Options slider represents a UI slider with title and value labels.
	/// </summary>
	public class OptionsSlider : HydraMonoBehaviour
	{
		public event EventHandler<EventArg<float>> onSliderValueChanged;

		[SerializeField] private Text m_TitleLabel;
		[SerializeField] private Slider m_Slider;
		[SerializeField] private Text m_ValueLabel;

		public string title { get { return m_TitleLabel.text; } set { m_TitleLabel.text = value; } }
		public float min { get { return m_Slider.minValue; } set { m_Slider.minValue = value; } }
		public float max { get { return m_Slider.maxValue; } set { m_Slider.maxValue = value; } }
		public float sliderValue { get { return m_Slider.value; } set { m_Slider.value = value; } }

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			Subscribe();
		}

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			Unsubscribe();
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Subscribe to slider events.
		/// </summary>
		private void Subscribe()
		{
			m_Slider.onValueChanged.AddListener(OnSliderValueChanged);
		}

		/// <summary>
		/// 	Unsubscribe from slider events.
		/// </summary>
		private void Unsubscribe()
		{
			m_Slider.onValueChanged.RemoveListener(OnSliderValueChanged);
		}

		/// <summary>
		/// 	Called when the slider value changes.
		/// </summary>
		/// <param name="newValue">New value.</param>
		private void OnSliderValueChanged(float newValue)
		{
			m_ValueLabel.text = newValue.ToString("0.00");

			EventHandler<EventArg<float>> handler = onSliderValueChanged;
			if (handler != null)
				handler(this, new EventArg<float>(newValue));
		}

		#endregion
	}
}
