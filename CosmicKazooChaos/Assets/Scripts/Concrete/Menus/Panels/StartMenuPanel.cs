using CosmicKazooChaos.Abstract.Menus.Panels;
using CosmicKazooChaos.Concrete.Menus.States;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Menus.Panels
{
	/// <summary>
	/// 	Start menu panel.
	/// </summary>
	public class StartMenuPanel : AbstractMenuPanel
	{
		[SerializeField] private MainMenuState m_MainMenuState;

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (Input.anyKeyDown)
				NextMenu(m_MainMenuState);
		}
	}
}
