﻿using System.Collections.Generic;
using CosmicKazooChaos.Abstract.Menus.States;
using CosmicKazooChaos.Concrete.Menus.States;
using Hydra.HydraCommon.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace CosmicKazooChaos.Concrete.Menus
{
	/// <summary>
	/// 	MenuController is responsible for setting the current active menu.
	/// </summary>
	[RequireComponent(typeof(Canvas))]
	[RequireComponent(typeof(CanvasScaler))]
	[RequireComponent(typeof(GraphicRaycaster))]
	[RequireComponent(typeof(AudioSource))]
	public class MenuController : SingletonHydraMonoBehaviour<MenuController>
	{
		[SerializeField] private MenuStateMachine m_StateMachine;

		private AudioSource m_CachedAudioSource;
		private Stack<AbstractMenuState> m_MenuStack;

		#region Properties

		/// <summary>
		/// 	Gets the audio source.
		/// </summary>
		/// <value>The audio source.</value>
		public AudioSource audioSource
		{
			get { return m_CachedAudioSource ?? (m_CachedAudioSource = GetComponent<AudioSource>()); }
		}

		#endregion

		#region Messages

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			m_StateMachine.Update(this);
		}

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_StateMachine == null)
				m_StateMachine = new MenuStateMachine();

			if (m_MenuStack == null)
				m_MenuStack = new Stack<AbstractMenuState>();

			m_StateMachine.OnEnable(this);
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Returns to the previous menu.
		/// </summary>
		public void PreviousMenu()
		{
			// Remove the current menu.
			m_MenuStack.Pop().OnPop(this);

			// Reapply the previous menu.
			AbstractMenuState next = (m_MenuStack.Count > 0) ? m_MenuStack.Pop() : m_StateMachine.initialState;
			NextMenu(next);
		}

		/// <summary>
		/// 	Proceeds to the next menu.
		/// </summary>
		/// <param name="state">State.</param>
		public void NextMenu(AbstractMenuState state)
		{
			m_MenuStack.Push(state);
			m_StateMachine.SetActiveState(state, this);
			state.OnPush(this);
		}

		#endregion
	}
}
