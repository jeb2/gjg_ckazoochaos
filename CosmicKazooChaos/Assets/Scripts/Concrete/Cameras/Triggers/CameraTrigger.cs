﻿using CosmicKazooChaos.Abstract.Cameras.Triggers;

namespace CosmicKazooChaos.Concrete.Cameras.Triggers
{
	/// <summary>
	/// 	Camera trigger lets us set up sections where the camera mode changes
	/// 	when the player enters an area.
	/// </summary>
	public class CameraTrigger : AbstractTemplateCameraTrigger {}
}
