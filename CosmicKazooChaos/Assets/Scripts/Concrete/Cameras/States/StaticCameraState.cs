﻿using CosmicKazooChaos.Abstract.Cameras.States;

namespace CosmicKazooChaos.Concrete.Cameras.States
{
	/// <summary>
	/// 	Camera does not move.
	/// </summary>
	public class StaticCameraState : AbstractTemplateCameraState {}
}
