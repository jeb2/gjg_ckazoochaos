﻿using CosmicKazooChaos.Abstract.Cameras.States;
using CosmicKazooChaos.Utils.Time;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Cameras.States
{
	/// <summary>
	/// 	Camera follows the rotation of the target.
	/// </summary>
	public class ActiveCameraState : AbstractCameraState
	{
		/// <summary>
		/// 	Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void Orient(MonoBehaviour parent)
		{
			CameraController cameraController = GetCameraController(parent);

			float yaw = cameraController.target.transform.eulerAngles.y;

			Quaternion current = parent.transform.rotation;
			Quaternion target = Quaternion.Euler(cameraController.orbitY, yaw, 0.0f);

			parent.transform.rotation = Quaternion.Slerp(current, target, GameTime.fixedDeltaTime);
			parent.transform.position = GetPositionFromRotation(parent);
		}
	}
}
