﻿using System;
using System.Collections.Generic;
using CosmicKazooChaos.Abstract.Cameras.States;
using CosmicKazooChaos.Abstract.StateMachine;
using Hydra.HydraCommon.Extensions;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Cameras.States
{
	/// <summary>
	/// 	CameraStateMachine is the state machine for the CameraController.
	/// </summary>
	[Serializable]
	public class CameraStateMachine : AbstractStateMachine<AbstractCameraState>
	{
		[SerializeField] private AbstractCameraState[] m_States;
		[SerializeField] private AbstractCameraState m_InitialState;

		private static readonly Dictionary<CameraController.Mode, Type> s_ModeMap = new Dictionary
			<CameraController.Mode, Type>
		{
			{CameraController.Mode.Active, typeof(ActiveCameraState)},
			{CameraController.Mode.Passive, typeof(PassiveCameraState)},
			{CameraController.Mode.Fixed, typeof(FixedCameraState)},
			{CameraController.Mode.Static, typeof(StaticCameraState)},
			{CameraController.Mode.Free, typeof(FreeCameraState)},
			{CameraController.Mode.Auto, typeof(AutoCameraState)},
			{CameraController.Mode.Predictative2DCameraState, typeof(Predictative2DCameraState)}
		};

		#region Properties

		/// <summary>
		/// 	Gets the initial state.
		/// </summary>
		/// <value>The initial state.</value>
		public override AbstractCameraState initialState { get { return m_InitialState; } }

		#endregion

		#region Methods

		/// <summary>
		/// 	Sets the active state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="mode">Mode.</param>
		public AbstractCameraState SetActiveState(CameraController.Mode mode, MonoBehaviour parent)
		{
			AbstractCameraState state = GetStateByMode(mode);
			return SetActiveState(state, parent);
		}

		/// <summary>
		/// 	Gets the mode.
		/// </summary>
		/// <returns>The mode.</returns>
		public CameraController.Mode GetMode()
		{
			Type type = activeState.GetType();
			return s_ModeMap.GetKeyForValue(type);
		}

		/// <summary>
		/// 	Gets the state by mode.
		/// </summary>
		/// <returns>The state.</returns>
		/// <param name="mode">Mode.</param>
		public AbstractCameraState GetStateByMode(CameraController.Mode mode)
		{
			Type stateType = GetStateTypeByMode(mode);

			for (int index = 0; index < m_States.Length; index++)
			{
				AbstractCameraState state = m_States[index];
				if (state.GetType() == stateType)
					return state;
			}

			throw new ArgumentOutOfRangeException();
		}

		#endregion

		/// <summary>
		/// 	Gets the state type by mode.
		/// </summary>
		/// <returns>The state type by mode.</returns>
		/// <param name="mode">Mode.</param>
		public static Type GetStateTypeByMode(CameraController.Mode mode)
		{
			return s_ModeMap[mode];
		}
	}
}
