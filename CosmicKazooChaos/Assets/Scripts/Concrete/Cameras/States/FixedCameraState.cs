﻿using CosmicKazooChaos.Abstract.Cameras.States;
using CosmicKazooChaos.Utils.Time;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Cameras.States
{
	/// <summary>
	/// 	Camera rotates on the spot to face the target.
	/// </summary>
	public class FixedCameraState : AbstractTemplateCameraState
	{
		/// <summary>
		/// 	Orients the rotation.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void OrientRotation(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);

			Vector3 targetOffsetPosition = GetOffsetPosition(parent);
			Vector3 toTarget = targetOffsetPosition - controller.transform.position;

			Quaternion rotation = controller.transform.rotation;
			Quaternion finalRotation = Quaternion.LookRotation(toTarget);

			controller.transform.rotation = Quaternion.Slerp(rotation, finalRotation, GameTime.fixedDeltaTime);
		}
	}
}
