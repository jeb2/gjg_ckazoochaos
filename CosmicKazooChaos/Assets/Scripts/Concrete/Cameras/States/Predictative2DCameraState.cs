﻿using UnityEngine;
using System.Collections;
using CosmicKazooChaos.Abstract.Cameras.States;
using CosmicKazooChaos.Concrete.Cameras;
using CosmicKazooChaos.Concrete.Configuration.Controls.Mapping;
using CosmicKazooChaos.Utils.Time;
using CosmicKazooChaos.Abstract.Player;
using Hydra.HydraCommon.Utils;

namespace CosmicKazooChaos.Concrete.Cameras.States
{
	public class Predictative2DCameraState : AbstractCameraState
	{
		//Vector3 OriginalPosition;
		[SerializeField] private float actualDistance = 11f;
		
		/// <summary>
		/// 	Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void Orient(MonoBehaviour parent)
		{
			CameraController cameraController = GetCameraController(parent);
			
			Transform transform = cameraController.transform;
			Transform target = cameraController.target.transform;

			//float distance = 5f;

			//AbstractPlayerController player = cameraController.target;

			//Vector3 playerVelocity = new Vector3(player.characterLocomotor.velocity.x, 0, 0);
			//Vector3 pivotOffSet = cameraController.pivotOffset;

			//Debug.Log (playerVelocity);

			//pivotOffSet = Vector3.Lerp(pivotOffSet, pivotOffSet + playerVelocity, 2f * GameTime.deltaTime);

			//cameraController.pivotOffset = pivotOffSet;

			//transform.position = Vector3.Slerp (transform.position, transform.position + playerVelocity, 26f * GameTime.deltaTime);

			//transform.rotation = Quaternion.Euler(cameraController.orbitY, cameraController.orbitX, 0);

			//GameTime.deltaTime
			//transform.position = Vector3.Lerp (transform.position, transform.position + playerVelocity, 2f * GameTime.deltaTime);
			//transform.position = transform.position + playerVelocity;
			//transform.rotation = Quaternion.RotateTowards

			transform.position = GetPositionFromRotation(parent);

			//transform.position =

			//transform.rotation = target.rotation;
			//Vector3.
		}

		/// <summary>
		/// 	Gets the offset position for the target.
		/// </summary>
		/// <returns>The offset position.</returns>
		protected override Vector3 GetOffsetPosition(MonoBehaviour parent)
		{
			CameraController cameraController = GetCameraController(parent);
			Transform target = cameraController.target.transform;

			AbstractPlayerController player = cameraController.target;
			Vector3 output;
			Vector3 currentPlayerVel = Vector3.zero;

			Vector3 playerVelocity = Vector3.zero;

			Vector3 pivotOffset = cameraController.pivotOffset;
			//Vector3 playerVelocity = new Vector3(player.characterLocomotor.velocity.x, 0, 0);
			//player.characterLocomotor.

			//float horizontalSmooth = Mathf.Clamp(InputMapping.horizontalInput.GetAxis() + player.characterLocomotor.characterController.velocity.y, 0f, 1f);
			float horizontalSmooth = InputMapping.horizontalInput.GetAxis ();
			float horizontal = InputMapping.horizontalInput.GetAxisRaw ();

			float distance = (actualDistance + (1f * Mathf.Abs(horizontalSmooth)));
			cameraController.distance = distance;
			//horizontal = Mathf.Lerp (horizontal, horizontal, 2f * GameTime.deltaTime);

			//Vector3 playerVelocity = new Vector3(0, 0, (player.characterLocomotor.velocity.x * -horizontal) * 0.3f);
			//playerVelocity = new Vector3(0, 0, (player.characterLocomotor.velocity.x * player.transform.forward.x) * 0.3f);
			//pivotOffset.z = Mathf.Lerp (pivotOffset.z, (pivotOffset.z + playerVelocity.x), 2f * GameTime.deltaTime);
			//Vector3.Lerp (Vector3.zero, playerVelocity, 2f * GameTime.deltaTime)
			//float horizontal = InputMapping.horizontalInput.GetAxis();

			//if (horizontal
			//playerVelocity.y = Mathf.Lerp (0f, player.characterLocomotor.velocity.y, 4f * GameTime.deltaTime);
			//playerVelocity.y = Mathf.Lerp(playerVelocity.y, player.characterLocomotor.characterController.velocity.y * 0.07f, 100f * GameTime.deltaTime);
			//playerVelocity.y = player.characterLocomotor.characterController.velocity.y;

			if (horizontal != 0f)
			{
				/*playerVelocity = new Vector3(0,
				                             (player.characterLocomotor.characterController.velocity.y) * 0.1f,
				                             (player.characterLocomotor.velocity.x * player.transform.forward.x) * 0.14f);*/

				playerVelocity.z = (player.characterLocomotor.velocity.x * player.transform.forward.x) * 0.14f;

				//output = target.position + target.TransformDirection (pivotOffset + playerVelocity);
			}

			else
			{
				playerVelocity.z = Mathf.Lerp((player.characterLocomotor.velocity.x * player.transform.forward.x) * 0.14f,
				                              0f,
				                              Mathf.SmoothStep(0f, 0.2f, 2f * GameTime.deltaTime));

				/*playerVelocity = Vector3.Lerp (new Vector3(0,
				                                           (player.characterLocomotor.characterController.velocity.y) * 0.1f, 
				                                           (player.characterLocomotor.velocity.x * player.transform.forward.x) * 0.14f),
				                               Vector3.zero,
				                               Mathf.SmoothStep(0f, 0.2f, 2f * GameTime.deltaTime));*/

				/*playerVelocity = Vector3.Lerp (new Vector3(0, 0, (-5f * player.transform.forward.x)),
				                               Vector3.zero,
				                               2f * GameTime.deltaTime);*/

				//playerVelocity.z = Mathf.Lerp(currentPlayerVel.z, 0f, 0.001f * GameTime.deltaTime);
				//if (playerVelocity =)
				//playerVelocity.z = Mathf.Lerp(playerVelocity.z, 0f, 0.001f * GameTime.deltaTime);
				//playerVelocity.z = Mathf.Lerp(player.transform.forward.x * 5f, 0f, 1f * GameTime.deltaTime);

				//output = Vector3.Slerp(target.position + target.TransformDirection(pivotOffset + playerVelocity), 
				                       //target.position + target.TransformDirection(pivotOffset), 
				                       //2f * GameTime.deltaTime);

				/*output = Vector3.Slerp((target.position + target.TransformDirection(pivotOffset + currentPlayerVel)), 
										 (target.position + target.TransformDirection(pivotOffset)), 
										 0.001f * GameTime.deltaTime);*/

				//Debug.Log (horizontal);
			}

			//Debug.Log (player.characterLocomotor.characterController.velocity.y);
			//Debug.Log (playerVelocity);
			//Debug.Log (horizontal);
			//Debug.Log (currentPlayerVel);
			//output = target.position + target.TransformDirection (pivotOffset + currentPlayerVel);
			output = target.position + target.TransformDirection (pivotOffset + playerVelocity);

			return output;
		}

		/// <summary>
		/// 	Gets the position from rotation.
		/// </summary>
		/// <returns>The position from rotation.</returns>
		/// <param name="parent">Parent.</param>
		/// <param name="rotation">Rotation.</param>
		/*protected Vector3 GetPositionFromRotation(MonoBehaviour parent, Quaternion rotation)
		{
			CameraController cameraController = GetCameraController(parent);
			float distance = cameraController.distance;
			
			Vector3 offsetPosition = GetOffsetPosition(parent);
			Vector3 back = rotation * Vector3.back;
			
			return back * distance + offsetPosition;
		}*/
	}
}