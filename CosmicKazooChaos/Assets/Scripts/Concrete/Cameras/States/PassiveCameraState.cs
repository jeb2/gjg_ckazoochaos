﻿using CosmicKazooChaos.Abstract.Cameras.States;
using CosmicKazooChaos.Concrete.Configuration.Controls.Mapping;
using CosmicKazooChaos.Utils.Time;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Cameras.States
{
	/// <summary>
	/// 	Camera is rotated based on mouse input.
	/// </summary>
	public class PassiveCameraState : AbstractCameraState
	{
		[SerializeField] private bool m_CameraSnapHoldTrueToggleFalse;
		[SerializeField] private bool m_CameraRotationHold;
		[SerializeField] private bool m_ToggleBack;

		/// <summary>
		/// 	Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void Orient(MonoBehaviour parent)
		{
			CameraController cameraController = GetCameraController(parent);

			Transform transform = cameraController.transform;
			Transform target = cameraController.target.transform;

			Quaternion quarter = Quaternion.Euler(target.rotation.eulerAngles.x, target.rotation.eulerAngles.y, 0.0f);

			if (!CameraController.HasUserInput())
			{
				if (m_CameraSnapHoldTrueToggleFalse)
				{
					if (m_CameraRotationHold && InputMapping.snapCamera.GetButton())
						transform.rotation = Quaternion.Slerp(transform.rotation, quarter, 5f * GameTime.deltaTime);

					if (!m_CameraRotationHold && InputMapping.snapCamera.GetButtonDown())
					{
						if (m_ToggleBack != true)
							m_ToggleBack = true;
					}
				}

				if (!m_CameraSnapHoldTrueToggleFalse)
				{
					if (!m_CameraRotationHold && InputMapping.snapCamera.GetButtonDown())
						transform.rotation = quarter;

					if (m_CameraRotationHold && InputMapping.snapCamera.GetButton())
						transform.rotation = Quaternion.Slerp(transform.rotation, quarter, 5f * GameTime.deltaTime);
				}

				if (m_ToggleBack == true)
					transform.rotation = Quaternion.Slerp(transform.rotation, quarter, 5f * GameTime.deltaTime);
			}

			else
			{
				transform.rotation = Quaternion.Euler(cameraController.orbitY, cameraController.orbitX, 0);

				if (m_ToggleBack == true)
					m_ToggleBack = false;
			}

			transform.position = GetPositionFromRotation(parent);
		}
	}
}
