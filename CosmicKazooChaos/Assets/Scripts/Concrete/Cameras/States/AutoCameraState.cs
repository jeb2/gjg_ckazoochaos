﻿using CosmicKazooChaos.Abstract.Cameras.States;
using CosmicKazooChaos.Abstract.Player;
using CosmicKazooChaos.Utils;
using CosmicKazooChaos.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Cameras.States
{
	/// <summary>
	/// 	The Auto camera will offset from the pivot slightly in the direction the
	/// 	player is facing. It will gradually pan behind the player until facing
	/// 	along the player's forward.
	/// 	
	/// 	The Auto camera will transition to the Free camera when the player starts
	/// 	to pan the camera manually.
	/// </summary>
	public class AutoCameraState : AbstractPredictiveCameraState
	{
		[SerializeField] private FreeCameraState m_FreeCameraState;

		[Tweakable("Camera")] [SerializeField] private float m_PredictionDistanceSideways;
		[Tweakable("Camera")] [SerializeField] private float m_PredictionDistanceForward;
		[Tweakable("Camera")] [SerializeField] private float m_PredictionDistanceBackwards;

		/// <summary>
		/// 	Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void Orient(MonoBehaviour parent)
		{
			base.Orient(parent);

			CameraController cameraController = GetCameraController(parent);

			Quaternion orbitRotation = Quaternion.Euler(cameraController.orbitY, cameraController.orbitX, 0);

			float targetYaw = cameraController.target.transform.rotation.eulerAngles.y;
			Quaternion targetRotation = Quaternion.Euler(orbitRotation.x, targetYaw, 0.0f);

			cameraController.transform.rotation = Quaternion.RotateTowards(orbitRotation, targetRotation,
																		   GameTime.deltaTime * 5.0f);
			cameraController.transform.position = GetPositionFromRotation(parent);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractCameraState GetNextState(MonoBehaviour parent)
		{
			if (CameraController.HasUserInput())
				return m_FreeCameraState;

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Updates the prediction offset.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void UpdatePredictionOffset(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);
			AbstractPlayerController target = controller.target;

			Vector3 cameraForward = controller.transform.forward;
			Vector3 playerForward = target.transform.forward;
			cameraForward.y = 0.0f;
			playerForward.y = 0.0f;

			float dot = Vector3.Dot(cameraForward, playerForward);
			float distance;

			if (dot >= 0.0f)
				distance = HydraMathUtils.MapRange(0.0f, 1.0f, m_PredictionDistanceSideways, m_PredictionDistanceForward, dot);
			else
				distance = HydraMathUtils.MapRange(-1.0f, 0.0f, m_PredictionDistanceBackwards, m_PredictionDistanceSideways, dot);

			Quaternion rotation = target.transform.rotation;

			Vector3 offset = rotation * (Vector3.forward * distance);
			Vector3 delta = offset - controller.predictionOffset;

			controller.predictionOffset += delta * predictionSpeed * GameTime.fixedDeltaTime;
		}
	}
}
