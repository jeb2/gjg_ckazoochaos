﻿using System;
using System.Collections.Generic;
using CosmicKazooChaos.Abstract.Cameras.States;
using CosmicKazooChaos.Abstract.Cameras.Triggers;
using CosmicKazooChaos.Abstract.Player;
using CosmicKazooChaos.Abstract.Player.States;
using CosmicKazooChaos.Concrete.Cameras.States;
using CosmicKazooChaos.Concrete.Configuration;
using CosmicKazooChaos.Concrete.Configuration.Controls.Mapping;
using CosmicKazooChaos.Utils;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Cameras
{
	[RequireComponent(typeof(Camera))]
	public class CameraController : HydraMonoBehaviour
	{
		public enum Mode
		{
			Passive,
			Active,
			Fixed,
			Static,
			Auto,
			Free,
			Predictative2DCameraState
		}

		[SerializeField] private CameraStateMachine m_StateMachine;
		[SerializeField] private AbstractPlayerController m_Target;
		[Tweakable("Camera")] [SerializeField] private float m_Distance = 5.0f;
		[Tweakable("Camera")] [SerializeField] private float m_YMinLimit = -20.0f;
		[Tweakable("Camera")] [SerializeField] private float m_YMaxLimit = 80.0f;

		[SerializeField] private Vector3 m_PivotOffset;

		private bool m_OrbitEnabled = true;
		[SerializeField] private bool m_ClampCamera;

		private float m_X = 0.0f;
		private float m_Y = 0.0f;

		private Camera m_Template;

		private List<AbstractTemplateCameraTrigger> m_Triggers;

		private Vector3 m_PredictionOffset;

		#region Properties

		/// <summary>
		/// 	Gets or sets the target.
		/// </summary>
		/// <value>The target.</value>
		public AbstractPlayerController target { get { return m_Target; } set { m_Target = value; } }

		/// <summary>
		/// 	Gets the distance.
		/// </summary>
		/// <value>The distance.</value>
		public float distance { get { return m_Distance; } set { m_Distance = value; } }

		/// <summary>
		/// 	Gets the pivot offset.
		/// </summary>
		/// <value>The pivot offset.</value>
		public Vector3 pivotOffset { get { return m_PivotOffset; } }

		/// <summary>
		/// 	Gets or sets the prediction offset.
		/// </summary>
		/// <value>The prediction offset.</value>
		public Vector3 predictionOffset { get { return m_PredictionOffset; } set { m_PredictionOffset = value; } }

		/// <summary>
		/// 	Gets or sets a value indicating whether orbit is enabled.
		/// </summary>
		/// <value><c>true</c> if orbit enabled; otherwise, <c>false</c>.</value>
		public bool orbitEnabled { get { return m_OrbitEnabled; } set { m_OrbitEnabled = value; } }

		/// <summary>
		/// 	Gets or sets the orientation x.
		/// </summary>
		/// <value>The orientation x.</value>
		public float orbitX { get { return m_X; } set { m_X = value; } }

		/// <summary>
		/// 	Gets or sets the orientation y.
		/// </summary>
		/// <value>The orientation y.</value>
		public float orbitY { get { return m_Y; } set { m_Y = value; } }

		/// <summary>
		/// 	Gets the minimum Y limit.
		/// </summary>
		/// <value>The minimum Y limit.</value>
		public float minYLimit { get { return m_YMinLimit; } }

		/// <summary>
		/// 	Gets the max Y limit.
		/// </summary>
		/// <value>The max Y limit.</value>
		public float maxYLimit { get { return m_YMaxLimit; } }

		/// <summary>
		/// 	Gets the template.
		/// </summary>
		/// <value>The template.</value>
		public Camera template { get { return m_Template; } }

		/// <summary>
		/// 	Gets the camera velocity.
		/// </summary>
		/// <value>The template.</value>
		public Vector2 speed
		{
			get { return new Vector2(Options.cameraHorizontalSensitivity, Options.cameraVerticalSensitivity); }
		}

		/// <summary>
		/// 	Gets or sets a value indicating whether the camera is clamped or not
		/// </summary>
		/// <value><c>true</c> if clamp enabled; otherwise, <c>false</c>.</value>
		public bool clampEnabled { get { return m_ClampCamera; } set { m_ClampCamera = value; } }

		/// <summary>
		/// 	Gets or sets the camera state machine
		/// </summary>
		/// <value>The camera state machine.</value>
		public CameraStateMachine stateMachine { get { return m_StateMachine; } set { m_StateMachine = value; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_Triggers == null)
				m_Triggers = new List<AbstractTemplateCameraTrigger>();
		}

		/// <summary>
		/// 	Called after all Updates have finished.
		/// </summary>
		protected override void LateUpdate()
		{
			base.LateUpdate();

			m_StateMachine.LateUpdate(this);
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Sets the mode.
		/// </summary>
		/// <param name="mode">Mode.</param>
		public void SetMode(Mode mode)
		{
			SetMode(mode, null);
		}

		/// <summary>
		/// 	Sets the mode.
		/// </summary>
		/// <param name="mode">Mode.</param>
		/// <param name="template">Template.</param>
		public void SetMode(Mode mode, Camera template)
		{
			m_StateMachine.SetActiveState(mode, this);
			m_Template = template;
		}

		/// <summary>
		/// 	Gets the mode.
		/// </summary>
		public Mode GetMode()
		{
			return m_StateMachine.GetMode();
		}

		/// <summary>
		/// 	Adds the trigger.
		/// </summary>
		/// <param name="trigger">Trigger.</param>
		public void AddTrigger(AbstractTemplateCameraTrigger trigger)
		{
			RemoveTrigger(trigger);
			m_Triggers.Add(trigger);

			SetMode(trigger.mode, trigger.template);
		}

		/// <summary>
		/// 	Removes the trigger.
		/// </summary>
		/// <param name="trigger">Trigger.</param>
		public void RemoveTrigger(AbstractTemplateCameraTrigger trigger)
		{
			m_Triggers.Remove(trigger);

			AbstractTemplateCameraTrigger active = GetActiveTrigger();

			if (active != null)
			{
				SetMode(active.mode, active.template);
				return;
			}

			AbstractPlayerController player = m_Target.GetComponent<AbstractPlayerController>();
			if (player == null)
				return;

			AbstractPlayerState state = player.stateMachine.activeState;
			SetMode(state.cameraMode);
		}

		/// <summary>
		/// 	Gets the active trigger.
		/// </summary>
		/// <returns>The active trigger.</returns>
		public AbstractTemplateCameraTrigger GetActiveTrigger()
		{
			if (m_Triggers.Count > 0)
				return m_Triggers[m_Triggers.Count - 1];
			return null;
		}

		#endregion

		#region Static Methods

		/// <summary>
		/// 	Returns true if template should be used for the given mode.
		/// </summary>
		/// <returns><c>true</c>, if template is used, <c>false</c> otherwise.</returns>
		/// <param name="mode">Mode.</param>
		public static bool UseTemplate(Mode mode)
		{
			Type stateType = CameraStateMachine.GetStateTypeByMode(mode);

			return typeof(AbstractTemplateCameraState).IsAssignableFrom(stateType);
		}

		/// <summary>
		/// 	Returns true when the user is adjusting the camera manually.
		/// </summary>
		/// <returns><c>true</c> if this instance has user input; otherwise, <c>false</c>.</returns>
		public static bool HasUserInput()
		{
			float horizontal = InputMapping.horizontalCameraInput.GetAxis();
			float vertical = InputMapping.verticalCameraInput.GetAxis();

			if (!HydraMathUtils.Approximately(horizontal, 0.0f))
				return true;

			if (!HydraMathUtils.Approximately(vertical, 0.0f))
				return true;

			return false;
		}

		/// <summary>
		/// 	Checks if the camera is within limits.
		/// </summary>
		/// <value><c>true</c> if the movement keys/joystick isn't used; otherwise, <c>false</c>.</value>
		public bool IsWithinCameraClamp()
		{
			if (m_Y < m_YMaxLimit || m_Y > m_YMinLimit)
				return true;

			return false;
		}

		#endregion

        //		private void RenderUnderwaterFog()
        //		{
        //			RenderSettings.fog = true;
        //			RenderSettings.fogDensity = 0.05f;
        //			RenderSettings.fogColor = new Color (0.5f, 0.6f, 0.8f, 0.517f);
        //		}
        //
        //		private void RenderNormalFog()
        //		{
        //			RenderSettings.fogColor = Color.clear;
        //			RenderSettings.fogDensity = 0.01f;
        //			RenderSettings.fog = false;
        //		}

        /// <summary>
        /// 	OnTriggerEnter is called when the Collider other enters the trigger.
        /// </summary>
        /// <param name="other">Other.</param>
        //		protected override void OnTriggerEnter(Collider other)
        //		{
        //			base.OnTriggerEnter(other);
        //
        //			Debug.Log ("ISINWATER");
        //
        //			WaterTrigger waterTrigger = other.GetComponent<WaterTrigger> ();
        //			if (waterTrigger == null) 
        //				return;
        //
        //			if (RenderSettings.fog != true)
        //				RenderUnderwaterFog();
        //		}
        //		
        //		/// <summary>
        //		/// 	OnTriggerExit is called when the Collider other has stopped touching the trigger.
        //		/// </summary>
        //		/// <param name="other">Other.</param>
        //		protected override void OnTriggerExit(Collider other)
        //		{
        //			base.OnTriggerExit(other);
        //			
        //			WaterTrigger waterTrigger = other.GetComponent<WaterTrigger> ();
        //			if (waterTrigger != null) 
        //				return;
        //
        //			if (RenderSettings.fog == true)
        //				RenderNormalFog();
        //		}

        //		private float UpsideFloat(float verticalfloat)
        //		{
        //			float mx = verticalfloat;
        //			mx += InputMapping.horizontalCameraInput.GetAxis () * m_Speed.x * m_Distance * Time.fixedDeltaTime;
        //			return mx;
        //		}
        //
        //		private float DownsideFloat(float verticalfloat)
        //		{
        //			float mx = verticalfloat;
        //			mx -= InputMapping.horizontalCameraInput.GetAxis () * m_Speed.x * m_Distance * Time.fixedDeltaTime;
        //			return mx;
        //		}
	}
}
