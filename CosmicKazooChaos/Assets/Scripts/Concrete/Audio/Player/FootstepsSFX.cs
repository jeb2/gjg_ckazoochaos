﻿using System;
using Hydra.HydraCommon.PropertyAttributes;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Audio.Player
{
	/// <summary>
	/// 	FootstepsSFX contains references to footstep audio clips.
	/// </summary>
	[Serializable]
	public class FootstepsSFX
	{
		[SerializeField] private SoundEffectAttribute[] m_Stone;
		[SerializeField] private SoundEffectAttribute[] m_Dirt;
		[SerializeField] private SoundEffectAttribute[] m_Sand;

		/// <summary>
		/// 	Gets the SFX.
		/// </summary>
		/// <returns>The SFX.</returns>
		/// <param name="surfaceType">Surface type.</param>
		public SoundEffectAttribute[] GetSFX(SurfaceInfo.SurfaceType surfaceType)
		{
			switch (surfaceType)
			{
				case SurfaceInfo.SurfaceType.Stone:
					return m_Stone;

				case SurfaceInfo.SurfaceType.Dirt:
					return m_Dirt;

				case SurfaceInfo.SurfaceType.Sand:
					return m_Sand;

				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}
