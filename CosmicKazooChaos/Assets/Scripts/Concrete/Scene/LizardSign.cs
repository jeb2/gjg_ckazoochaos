﻿using System.Collections;
using CosmicKazooChaos.Abstract.Player;
using CosmicKazooChaos.Utils.Time;
using Hydra.HydraCommon.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace CosmicKazooChaos.Concrete.Scene
{
	public class LizardSign : MonoBehaviour
	{
		[SerializeField] private string m_TextContent;

		private float m_MovTime = 1.0f;

		private RectTransform m_PanelTransform;
		private Text m_TextSign;

		private Vector2 m_ShowHeight = new Vector2(0f, -65f);
		private Vector2 m_HideHeight = new Vector2(0f, 100f);

		#region Properties

		public string textContent { get { return m_TextContent; } }

		#endregion

		#region Messages

		private void Awake()
		{
			m_PanelTransform = GameObject.Find("SignTextPanel").GetComponent<RectTransform>();
			m_TextSign = GameObject.Find("SignTextText").GetComponent<Text>();
		}

		private void OnTriggerEnter(Collider col)
		{
			AbstractPlayerController player = col.GetComponent<AbstractPlayerController>();
			if (player == null)
				return;

			m_TextSign.text = m_TextContent;
			this.StopCoroutine(HidePanel);
			this.StartCoroutine(ShowPanel);
		}

		private void OnTriggerExit(Collider col)
		{
			AbstractPlayerController player = col.GetComponent<AbstractPlayerController>();
			if (player == null)
				return;

			this.StopCoroutine(ShowPanel);
			this.StartCoroutine(HidePanel);
		}

		#endregion

		#region Private Methods

		private IEnumerator ShowPanel()
		{
			float startTime = GameTime.time;

			while (GameTime.time < startTime + m_MovTime)
			{
				AnimatePanel(GameTime.time - startTime, m_ShowHeight);
				yield return new WaitForEndOfFrame();
			}

			yield return 0;
		}

		private IEnumerator HidePanel()
		{
			float startTime = GameTime.time;

			while (GameTime.time < startTime + m_MovTime)
			{
				AnimatePanel(GameTime.time - startTime, m_HideHeight);
				yield return new WaitForEndOfFrame();
			}

			yield return 0;
		}

		private void AnimatePanel(float elapsedTime, Vector2 position)
		{
			m_PanelTransform.anchoredPosition = Vector2.Lerp(m_PanelTransform.anchoredPosition, position,
															 (elapsedTime / m_MovTime));
		}

		#endregion
	}
}
