﻿using System;
using CosmicKazooChaos.Abstract.Player;
using CosmicKazooChaos.Concrete.Cameras;
using CosmicKazooChaos.Concrete.Player;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Extensions;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Scene
{
	/// <summary>
	/// 	Scene manager prefabs.
	/// </summary>
	[Serializable]
	public class SceneManagerPrefabs
	{
		[SerializeField] private CameraController m_PlayerCameraPrefab;
		[SerializeField] private SpawnPoint m_SpawnPointPrefab;
		[SerializeField] private PlayerHandler m_PlayerHandlerPrefab;

		public CameraController playerCameraPrefab { get { return m_PlayerCameraPrefab; } }
		public SpawnPoint spawnPointPrefab { get { return m_SpawnPointPrefab; } }
		public PlayerHandler playerHandlerPrefab { get { return m_PlayerHandlerPrefab; } }
	}

	/// <summary>
	/// 	SceneManager is responsible for handling the player-related entities in the scene, including the camera, spawn point and swapper.
	/// </summary>
	[ExecuteInEditMode]
	public class SceneManager : SingletonHydraMonoBehaviour<SceneManager>
	{
		[SerializeField] private SceneManagerPrefabs m_Prefabs;
		[SerializeField] private CameraController m_PlayerCamera;
		[SerializeField] private SpawnPoint m_SpawnPoint;
		[SerializeField] private PlayerHandler m_PlayerHandler;

		#region Properties

		/// <summary>
		/// 	Gets the prefab factory.
		/// </summary>
		/// <value>The prefab factory.</value>
		public SceneManagerPrefabs prefabs { get { return m_Prefabs ?? (m_Prefabs = new SceneManagerPrefabs()); } }

		/// <summary>
		/// 	Gets the player camera.
		/// </summary>
		/// <value>The player camera.</value>
		public CameraController playerCamera { get { return m_PlayerCamera; } }

		/// <summary>
		/// 	Gets the spawn point.
		/// </summary>
		/// <value>The spawn point.</value>
		public SpawnPoint spawnPoint { get { return m_SpawnPoint; } }

		/// <summary>
		/// 	Gets the spawn point.
		/// </summary>
		/// <value>The spawn point.</value>
		public PlayerHandler playerHandler { get { return m_PlayerHandler; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			InstantiateDependencies();

			if (!Application.isPlaying)
				return;

			m_PlayerHandler.playerCamera = m_PlayerCamera;
			//NOTODO: GET PLAYER INSTANCE FROM ELSEWHERE
			AbstractPlayerController player = m_PlayerHandler.GetActivePlayer();
			player.transform.Copy(m_SpawnPoint);
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Instantiates the dependencies.
		/// </summary>
		public void InstantiateDependencies()
		{
			Camera mainCamera = Camera.main;

			if (ObjectUtils.LazyInstantiateOrFind(prefabs.playerCameraPrefab, ref m_PlayerCamera))
			{
				if (mainCamera != m_PlayerCamera && mainCamera != null)
				{
					Debug.Log(string.Format("Removing {0}", mainCamera.name));
					ObjectUtils.SafeDestroyGameObject(mainCamera);
				}
			}

			ObjectUtils.LazyInstantiateOrFind(prefabs.spawnPointPrefab, ref m_SpawnPoint);
			ObjectUtils.LazyInstantiateOrFind(prefabs.playerHandlerPrefab, ref m_PlayerHandler);
		}

		#endregion
	}
}
