﻿using UnityEngine;
using CosmicKazooChaos.Abstract.Platforms;
using System.Collections.Generic;
using CosmicKazooChaos.Concrete.Utils;

namespace CosmicKazooChaos.Concrete.Platforms
{
	public class TrackFollowerPlatform : MovingPlatform
	{
		[SerializeField] private TrackFollower m_TrackFollower;
		
		protected override void OnEnable()
		{
			base.OnEnable();
			m_TrackFollower.OnParentEnable();
		}
		
		protected override void FixedUpdate()
		{
			base.FixedUpdate();
			
			if (!m_TrackFollower.isActive)
				return;
			
			m_TrackFollower.OnFixedUpdate();
			m_TrackFollower.playerIsInPosition = isSupportingPlayer;
			
			Vector3 movement = m_TrackFollower.currentInstruction.GetNextPosition(m_TrackFollower) - transform.position;
			Quaternion nextRotation = m_TrackFollower.currentInstruction.GetNextRotation(m_TrackFollower);
			Quaternion rotation = Quaternion.FromToRotation(transform.rotation * Vector3.forward, nextRotation * Vector3.forward);
			
			HandleMovement(movement, rotation);
		}
	}
}