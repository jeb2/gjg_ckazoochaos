﻿using CosmicKazooChaos.Abstract.Platforms;
using CosmicKazooChaos.Utils.Time;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Platforms
{
	public class TutorialRaft : MovingPlatform
	{
		[SerializeField] private Vector3 m_EndPosition;
		[SerializeField] private float m_ForwardSpeed;
		[SerializeField] private float m_BackwardSpeed;

		private Vector3 m_StartPosition;

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();
			m_StartPosition = transform.position;
		}

		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();
			Vector3 movement = isSupportingPlayer
								   ? Vector3.ClampMagnitude(
														    (m_EndPosition - transform.position).normalized * m_ForwardSpeed * GameTime.deltaTime,
															Vector3.Distance(m_EndPosition, transform.position))
								   : Vector3.ClampMagnitude(
														    (m_StartPosition - transform.position).normalized * m_BackwardSpeed * GameTime.deltaTime,
															Vector3.Distance(m_StartPosition, transform.position));
			HandleMovement(movement, Quaternion.identity);
		}
	}
}
