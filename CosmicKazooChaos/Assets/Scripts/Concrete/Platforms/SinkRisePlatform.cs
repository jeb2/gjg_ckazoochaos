﻿using CosmicKazooChaos.Abstract.Platforms;
using CosmicKazooChaos.Utils.Time;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Platforms
{
	public class SinkRisePlatform : MovingPlatform
	{
		[SerializeField] private Vector3 m_SinkVelocity;
		[SerializeField] private float m_RiseSpeed;

		private Vector3 m_StartPosition;

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();
			m_StartPosition = transform.position;
		}

		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();
			Vector3 movement = isSupportingPlayer
								   ? m_SinkVelocity * GameTime.deltaTime
								   : Vector3.ClampMagnitude(
														    (m_StartPosition - transform.position).normalized * m_RiseSpeed * GameTime.deltaTime,
															Vector3.Distance(m_StartPosition, transform.position));
			HandleMovement(movement, Quaternion.identity);
		}
	}
}
