﻿using CosmicKazooChaos.Abstract.Platforms;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Platforms
{
	public class WaitForPlayerTrackInstruction : WaitTrackInstruction
	{
		[SerializeField] private bool m_Invert;
		/// <summary>
		/// 	Adjusts a follower without changing its position, rotation, or scale.
		/// </summary>
		/// <param name="follower">Follower.</param>
		public override void UpdateFollower(TrackFollower follower)
		{
			if (follower.playerIsInPosition ^ m_Invert)
				follower.instructionTimer.End();
			else
				follower.instructionTimer.Reset();
				
		}
	}
}