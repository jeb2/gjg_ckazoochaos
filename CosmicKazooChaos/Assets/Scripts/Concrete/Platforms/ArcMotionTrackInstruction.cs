﻿using CosmicKazooChaos.Abstract.Platforms;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Platforms
{
	public class ArcMotionTrackInstruction : MoveTrackInstruction
	{
		[SerializeField] private Vector3 m_ArcCenter;
		[SerializeField] private bool m_UseLargerMotionArc;

		/// <summary>
		/// 	Gets the position that a track follower should be moving towards.
		/// </summary>
		/// <returns>The next position.</returns>
		/// <param name="follower">Follower.</param>
		public override Vector3 GetNextPosition(TrackFollower follower)
		{
			return interpolater.InterpolateArc(follower.instructionTimer.elapsedAsPercentage, transform.position, m_ArcCenter,
											   nextInstructionObject.transform.position, m_UseLargerMotionArc);
		}
	}
}
