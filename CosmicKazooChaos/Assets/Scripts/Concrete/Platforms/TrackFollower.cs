﻿using System;
using CosmicKazooChaos.Concrete.Utils;
using CosmicKazooChaos.Abstract.Platforms;
using System.Collections.Generic;
using UnityEngine;

namespace CosmicKazooChaos.Concrete.Platforms
{
	[Serializable]
	public class TrackFollower
	{
		[SerializeField] private AbstractTrackInstruction m_InitialInstructionObject;
		[SerializeField] private bool m_PreventReactionToPlayer = true; // Specifically for enemies.
		
		private Timer m_InstructionTimer = new Timer();
		private bool m_PlayerIsInPosition;
		private Queue<AbstractTrackInstruction> m_Instructions = new Queue<AbstractTrackInstruction>();
		
		public Timer instructionTimer { get { return m_InstructionTimer; } }
		public bool playerIsInPosition { get { return m_PlayerIsInPosition; } set { m_PlayerIsInPosition = value;} }
		public bool preventReactionToPlayer { get { return m_PreventReactionToPlayer; } }
		
		/// <summary>
		/// 	Gets the instruction this follower is currently obeying.
		/// </summary>
		/// <value>The current instruction.</value>
		public AbstractTrackInstruction currentInstruction { get { return m_Instructions.Peek(); } }
		
		public bool isActive { get { return !(m_Instructions.Count == 0); } }
		
		/// <summary>
		/// 	Called when the parent is enabled.
		/// </summary>
		public void OnParentEnable()
		{
			if (m_InitialInstructionObject == null)
				return;
			
			Add(m_InitialInstructionObject.instructionSet);
			m_InstructionTimer = currentInstruction.timer;
			m_InstructionTimer.Reset();
		}
		
		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		public void OnFixedUpdate()
		{
			if (!isActive)
				return;
			
			if (m_InstructionTimer.complete)
				NextInstruction();
			
			currentInstruction.UpdateFollower(this);
		}
		
		/// <summary>
		/// 	Adds the track instruction to this follower's instruction queue.
		/// </summary>
		/// <param name="instruction">Instruction.</param>
		public void Add(AbstractTrackInstruction instruction)
		{
			if (instruction != null)
				m_Instructions.Enqueue(instruction);
		}
		
		/// <summary>
		/// 	Adds multiple track instructions to this follower's instruction queue.
		/// </summary>
		/// <param name="instructions">Instructions.</param>
		public void Add(AbstractTrackInstruction[] instructions)
		{
			if (instructions != null)
				for (int index = 0; index < instructions.Length; index++)
					Add(instructions[index]);
		}
		
		/// <summary>
		/// 	Causes this follower to obey the next instruction in its queue.
		/// </summary>
		private void NextInstruction()
		{
			if (!isActive)
				return;
			
			Add(m_Instructions.Dequeue().nextInstructionSet);
			m_InstructionTimer = currentInstruction.timer;
			m_InstructionTimer.Reset();
		}
	}
}