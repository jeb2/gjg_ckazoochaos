﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerShootingMechanics : MonoBehaviour 
{
	public GameObject LaserObject;
	public GameObject PlatformObject;

	public int MaxAmountOfEacH = 5;
	public float MaxLaserTime = 0.25f;

	public GameObject[] _laserPool;
	public GameObject[] _platformPool;

	public GameObject Crosshair;
	private GameObject _objCrosshair;

	//private Vector3 _vec3NewInstPos;

	private int _laserInt = 0;
	private int _platformInt = 0;

	private float _timer = 0;
	private bool isTimerUsed = false;

	private float _mouseX, _mouseY;

	void Start () 
	{
		_objCrosshair = Instantiate (Crosshair, transform.position, Quaternion.identity) as GameObject;
		_objCrosshair.transform.parent = transform;
		Vector3 _vectorToUse;
		_objCrosshair.transform.localPosition = Vector3.zero;
		_vectorToUse = _objCrosshair.transform.localPosition;
		_objCrosshair.transform.parent = Camera.main.transform;
		_objCrosshair.transform.position = _vectorToUse + new Vector3 (0f, 0f, 1.6f);


		//Instantiate LaserObjs and PlatObjs at start 
		//place them into their respective lists/arrays and in a point of game space were they don't bother
		for (int i = 0; i < MaxAmountOfEacH; i++)
		{
			_laserPool[i] = Instantiate (LaserObject, transform.position, Quaternion.identity) as GameObject;
			_laserPool[i].SetActive (false);
			_platformPool[i] = Instantiate (PlatformObject, transform.position, Quaternion.identity) as GameObject;
			_platformPool[i].SetActive (false);
		}	
	}

	void Update () 
	{
		_mouseX = Input.GetAxis("Mouse X");
		_mouseY = Input.GetAxis("Mouse Y");

		_objCrosshair.transform.Translate (new Vector3(-_mouseX, _mouseY), Space.World);

		if (Input.GetButtonDown ("Action1")) 
		{
			isTimerUsed = true;
		}

		if (Input.GetButtonUp("Action1"))
		{
			if (_timer <= MaxLaserTime)
			{
				ShootLaser();
			}

			else
			{
				ShootPLatform();
			}

			_timer = 0;
			isTimerUsed = false;
		}

		if (isTimerUsed)
			_timer += Time.deltaTime;

		/*if (Input.GetButtonDown("Action2"))
		{
			ShootPLatform();
		}*/

		//Debug stuff
		if (Input.GetKeyDown(KeyCode.Q))
		{
			//Debug.Log ("Available Lasers = " + _ListAvailableLasers.Count + ", Active lasers = " + _ListUnavailableLasers.Count + ", Available platforms = " + _ListAvailablePlatforms.Count + ", Active platforms = " + _ListUnavailablePlatforms.Count);
		}
	}

	void ShootLaser ()
	{
		if (_laserInt >= MaxAmountOfEacH)
		{
			_laserInt = 0;
		}

		if (_laserPool[_laserInt].activeSelf == false)
		{
			_laserPool[_laserInt].SetActive (true);
			//for now it sets the position at the gameobject with the script, refining comes later
			_laserPool[_laserInt].transform.position = transform.position;
			_laserPool[_laserInt].SendMessage ("SetDirection", _objCrosshair.transform);
			_laserInt++;
		}	
	}

	void ShootPLatform ()
	{
		if (_platformInt >= MaxAmountOfEacH)
		{
			_platformInt = 0;
		}
		
		if (_platformPool[_platformInt].activeSelf == false)
		{
			_platformPool[_platformInt].SetActive (true);
			//for now it sets the position at the gameobject with the script, refining comes later
			_platformPool[_platformInt].transform.position = transform.position;
			//_laserPool[_laserInt].SendMessage ("SetDirection", _objCrosshair.transform);
			_platformInt++;
		}
	}

	void RestoreLaser ()
	{
		
	}
}