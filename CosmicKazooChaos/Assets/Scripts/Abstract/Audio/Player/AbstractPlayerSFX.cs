using System;
using Hydra.HydraCommon.PropertyAttributes;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Audio.Player
{
	/// <summary>
	/// 	AbstractPlayerSFX stores sound effects for audiable player actions.
	/// </summary>
	public abstract class AbstractPlayerSFX
	{
		[SerializeField] private SoundEffectAttribute[] m_Attack;
		[SerializeField] private SoundEffectAttribute[] m_Crash;
		[SerializeField] private SoundEffectAttribute[] m_Fall;
		[SerializeField] private SoundEffectAttribute[] m_Hurt;
		[SerializeField] private SoundEffectAttribute[] m_Idle;
		[SerializeField] private SoundEffectAttribute[] m_Jump;
		[SerializeField] private SoundEffectAttribute[] m_Push;

		public SoundEffectAttribute[] attack { get { return m_Attack; } }
		public SoundEffectAttribute[] crash { get { return m_Crash; } }
		public SoundEffectAttribute[] fall { get { return m_Fall; } }
		public SoundEffectAttribute[] idle { get { return m_Idle; } }
		public SoundEffectAttribute[] hurt { get { return m_Hurt; } }
		public SoundEffectAttribute[] jump { get { return m_Jump; } }
		public SoundEffectAttribute[] push { get { return m_Push; } }
	}

	/// <summary>
	/// 	PlayerSFX stores sound effects for the PC.
	/// </summary>
	[Serializable]
	public class PlayerSFX : AbstractPlayerSFX
	{
		[SerializeField] private SoundEffectAttribute[] m_GunShootie;

		public SoundEffectAttribute[] lazerFX { get { return m_GunShootie; } }
	}
}
