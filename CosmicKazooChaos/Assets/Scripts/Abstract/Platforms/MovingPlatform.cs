﻿using System.Collections.Generic;
using CosmicKazooChaos.Abstract.Player;
using CosmicKazooChaos.Concrete;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Platforms
{
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(Collider))]
	public abstract class MovingPlatform : HydraMonoBehaviour
	{
		private bool m_IsSupportingPlayer;
		private List<CharacterLocomotor> m_CarriedCharacters;

		public bool isSupportingPlayer
		{
			get { return m_IsSupportingPlayer; }
			set
			{
				if (value == m_IsSupportingPlayer)
					return;
				if (value)
					OnPlayerLanding();
				else
					OnPlayerLeaving();
				m_IsSupportingPlayer = value;
			}
		}

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();
			m_CarriedCharacters = new List<CharacterLocomotor>();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();
			List<CharacterLocomotor> removeThese = new List<CharacterLocomotor>();
			foreach (CharacterLocomotor cl in m_CarriedCharacters)
			{
				if (cl.GroundedCast().collider != collider)
				{
					removeThese.Add(cl);
					if (cl.GetComponent<AbstractPlayerController>() != null)
						isSupportingPlayer = false;
				}
			}
			foreach (CharacterLocomotor cl in removeThese)
				m_CarriedCharacters.Remove(cl);
		}

		protected void HandleMovement(Vector3 movement, Quaternion rotation)
		{
			RaycastHit hitInfo;
			if (collider.attachedRigidbody.SweepTest(movement, out hitInfo, movement.magnitude))
				return;

			foreach (CharacterLocomotor cl in m_CarriedCharacters)
				cl.transform.position = transform.InverseTransformPoint(cl.transform.position);

			transform.position += movement;
			transform.rotation *= rotation;

			foreach (CharacterLocomotor cl in m_CarriedCharacters)
			{
				cl.transform.position = transform.TransformPoint(cl.transform.position);
				cl.transform.rotation *= rotation;
			}
		}

		/// <summary>
		/// 	Add the specified CharacterLocomotor to the list of CLs that move alongside this platform.
		/// </summary>
		/// <param name="cl">The CL to add.</param>
		public bool Add(CharacterLocomotor cl)
		{
			bool contained = m_CarriedCharacters.Contains(cl);
			if (!contained)
			{
				m_CarriedCharacters.Add(cl);
				if (cl.GetComponent<AbstractPlayerController>() != null)
					isSupportingPlayer = true;
			}
			return contained;
		}

		public virtual void OnPlayerLanding() {}

		public virtual void OnPlayerLeaving() {}
	}
}
