﻿using System;
using CosmicKazooChaos.Abstract.Player;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.EventArguments;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Cameras.Triggers
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Collider))]
	public abstract class AbstractCameraTrigger : HydraMonoBehaviour
	{
		public event EventHandler<EventArg<AbstractPlayerController>> onEnter;
		public event EventHandler<EventArg<AbstractPlayerController>> onExit;

		#region Messages

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			ConfigureCollider();
		}

		/// <summary>
		/// 	OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);

			AbstractPlayerController player = other.GetComponent<AbstractPlayerController>();
			if (player == null)
				return;

			OnEnter(player);
		}

		/// <summary>
		/// 	OnTriggerExit is called when the Collider other has stopped touching the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerExit(Collider other)
		{
			base.OnTriggerExit(other);

			AbstractPlayerController player = other.GetComponent<AbstractPlayerController>();
			if (player == null)
				return;

			OnExit(player);
		}

		#endregion

		/// <summary>
		/// 	Called when the player enters the trigger.
		/// </summary>
		/// <param name="player">Player.</param>
		protected virtual void OnEnter(AbstractPlayerController player)
		{
			EventHandler<EventArg<AbstractPlayerController>> handler = onEnter;
			if (handler != null)
				handler(this, new EventArg<AbstractPlayerController>(player));
		}

		/// <summary>
		/// 	Called when the player exits the trigger.
		/// </summary>
		/// <param name="player">Player.</param>
		protected virtual void OnExit(AbstractPlayerController player)
		{
			EventHandler<EventArg<AbstractPlayerController>> handler = onExit;
			if (handler != null)
				handler(this, new EventArg<AbstractPlayerController>(player));
		}

		/// <summary>
		/// 	Configures the collider.
		/// </summary>
		private void ConfigureCollider()
		{
			collider.isTrigger = true;
		}
	}
}
