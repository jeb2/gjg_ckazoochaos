﻿using CosmicKazooChaos.Concrete.Cameras;
using CosmicKazooChaos.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Cameras.States
{
	/// <summary>
	/// 	Abstract predictive camera state.
	/// </summary>
	public abstract class AbstractPredictiveCameraState : AbstractCameraState
	{
		[Tweakable("Camera")] [SerializeField] private float m_PredictionSpeed;

		/// <summary>
		/// 	Gets the prediction speed.
		/// </summary>
		/// <value>The prediction speed.</value>
		public float predictionSpeed { get { return m_PredictionSpeed; } }

		protected override void Orient(MonoBehaviour parent)
		{
			UpdatePredictionOffset(parent);
		}

		/// <summary>
		/// 	Gets the offset position for the target.
		/// </summary>
		/// <returns>The offset position.</returns>
		/// <param name="parent">Parent.</param>
		protected override Vector3 GetOffsetPosition(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);

			return base.GetOffsetPosition(parent) + controller.predictionOffset;
		}

		/// <summary>
		/// 	Updates the prediction offset.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected abstract void UpdatePredictionOffset(MonoBehaviour parent);
	}
}
