﻿using System;
using CosmicKazooChaos.Abstract.Menus.Panels;
using CosmicKazooChaos.Abstract.StateMachine;
using CosmicKazooChaos.Concrete.Menus;
using Hydra.HydraCommon.EventArguments;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Menus.States
{
	/// <summary>
	/// 	Abstract menu state.
	/// </summary>
	public abstract class AbstractMenuState : FiniteState<AbstractMenuState>
	{
		[SerializeField] private AudioClip m_Music;
		[SerializeField] private AbstractMenuPanel m_PanelPrefab;

		private AbstractMenuPanel m_PanelInstance;

		#region Properties

		/// <summary>
		/// 	Gets the panel instance.
		/// </summary>
		/// <value>The panel instance.</value>
		public AbstractMenuPanel panelInstance { get { return m_PanelInstance; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the object is loaded.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			Subscribe();
		}

		/// <summary>
		/// 	Called when the object goes out of scope.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			Unsubscribe();
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			ShowPanel(parent);

			AudioSource audioSource = GetMenuController(parent).audioSource;

			audioSource.clip = m_Music;
			if (!audioSource.isPlaying)
				audioSource.Play();
		}

		/// <summary>
		/// 	Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			HidePanel(parent);
		}

		/// <summary>
		/// Called when the menu is entered from a higher level menu. Not called when the menu becomes active after exiting a deeper menu.
		/// This is called after OnEnter.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void OnPush(MonoBehaviour parent)
		{
		}


		/// <summary>
		/// Called when the menu is exited to return to a higher level menu.  Not called when entering a deeper menu from this menu.
		/// This is called before OnExit.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void OnPop(MonoBehaviour parent)
		{
		}

		#endregion

		/// <summary>
		/// 	Gets the menu controller.
		/// </summary>
		/// <returns>The menu controller.</returns>
		/// <param name="parent">Parent.</param>
		protected MenuController GetMenuController(MonoBehaviour parent)
		{
			return parent as MenuController;
		}

		#region Private Methods

		/// <summary>
		/// 	Shows the panel.
		/// </summary>
		/// <param name="parent">Parent.</param>
		private void ShowPanel(MonoBehaviour parent)
		{
			if (m_PanelInstance == null)
				CreatePanel(parent);

			m_PanelInstance.gameObject.SetActive(true);
		}

		/// <summary>
		/// 	Hides the panel.
		/// </summary>
		/// <param name="parent">Parent.</param>
		private void HidePanel(MonoBehaviour parent)
		{
			if (m_PanelInstance != null)
				m_PanelInstance.gameObject.SetActive(false);
		}

		/// <summary>
		/// 	Creates the panel.
		/// </summary>
		/// <param name="parent">Parent.</param>
		private void CreatePanel(MonoBehaviour parent)
		{
			if (!ObjectUtils.LazyInstantiate(m_PanelPrefab, ref m_PanelInstance))
				return;

			m_PanelInstance.transform.SetParent(parent.transform, false);

			Subscribe();
		}

		/// <summary>
		/// 	Subscribe to panel events.
		/// </summary>
		private void Subscribe()
		{
			if (m_PanelInstance == null)
				return;

			m_PanelInstance.onPreviousMenuCallback += OnPreviousMenu;
			m_PanelInstance.onNextMenuCallback += OnNextMenu;
		}

		/// <summary>
		/// 	Unsubscribe from panel events.
		/// </summary>
		private void Unsubscribe()
		{
			if (m_PanelInstance == null)
				return;

			m_PanelInstance.onPreviousMenuCallback -= OnPreviousMenu;
			m_PanelInstance.onNextMenuCallback -= OnNextMenu;
		}

		/// <summary>
		/// 	Called when the panel wants to return to the previous menu.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnPreviousMenu(object sender, EventArgs eventArgs)
		{
			MenuController controller = MenuController.instance;
			controller.PreviousMenu();
		}

		/// <summary>
		/// 	Called when the panel wants to change to a different menu.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnNextMenu(object sender, EventArg<AbstractMenuState> eventArgs)
		{
			MenuController controller = MenuController.instance;
			controller.NextMenu(eventArgs.data);
		}

		#endregion
	}
}
