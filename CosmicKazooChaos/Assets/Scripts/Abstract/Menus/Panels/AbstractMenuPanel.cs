﻿using System;
using CosmicKazooChaos.Abstract.Menus.States;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.EventArguments;

namespace CosmicKazooChaos.Abstract.Menus.Panels
{
	/// <summary>
	/// 	Abstract menu panel.
	/// </summary>
	public abstract class AbstractMenuPanel : HydraMonoBehaviour
	{
		public event EventHandler onPreviousMenuCallback;
		public event EventHandler<EventArg<AbstractMenuState>> onNextMenuCallback;

		/// <summary>
		/// 	Called to change the current menu.
		/// </summary>
		public void PreviousMenu()
		{
			EventHandler handler = onPreviousMenuCallback;
			if (handler != null)
				handler(this, EventArgs.Empty);
		}

		/// <summary>
		/// 	Called to change the current menu.
		/// </summary>
		/// <param name="menuState">Menu state.</param>
		public void NextMenu(AbstractMenuState menuState)
		{
			EventHandler<EventArg<AbstractMenuState>> handler = onNextMenuCallback;
			if (handler != null)
				handler(this, new EventArg<AbstractMenuState>(menuState));
		}
	}
}
