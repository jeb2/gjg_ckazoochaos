﻿using CosmicKazooChaos.Utils;
using CosmicKazooChaos.Utils.Time;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Player.States
{
	/// <summary>
	/// 	AbstractDashingState is used while the player is dashing. 
	/// </summary>
	public abstract class AbstractDashingState : AbstractAttackState
	{
		[Tweakable("Dash")] protected bool m_AutoRun = true;
		[SerializeField] [Tweakable("Dash")] protected float m_RotateMomentumFactor = 0.2f;
		[SerializeField] [Tweakable("Dash")] protected float m_RotateAcceleration = 0.2f;

		private Quaternion m_RotateMomentum = Quaternion.identity;

		#region Override Methods

		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
			m_RotateMomentum = Quaternion.identity;
		}

		protected override Vector3 GetInputVector(AbstractPlayerController player)
		{
			Vector3 inputVector = base.GetInputVector(player);

			if (m_AutoRun)
			{
				Vector3 fullBlast = inputVector.normalized;
				if (inputVector.magnitude < float.Epsilon)
				{
					Vector3 flatCamera = new Vector3(player.playerCamera.camera.transform.forward.x, 0,
													 player.playerCamera.camera.transform.forward.z);
					fullBlast = Quaternion.Inverse(Quaternion.LookRotation(flatCamera)) * player.transform.forward;
				}

				return fullBlast;
			}
			else
				return inputVector;
		}

		/// <summary>
		/// 	Rotate towards the desired direction, maintaining the same speed.
		/// </summary>
		/// <param name="player">Player.</param>
		/// <param name="desiredMovement">Desired movement.</param>
		protected override Vector3 ResolveVelocityDelta(AbstractPlayerController player, Vector3 desiredV, Vector3 currentV)
		{
			currentV.y = 0;

			Vector3 newHeading = Vector3.RotateTowards(currentV, desiredV,
													   player.rotateSpeed * Mathf.Deg2Rad * rotateSpeedMultiplier * GameTime.deltaTime,
													   player.acceleration * accelerationMultiplier * GameTime.deltaTime);

			newHeading = m_RotateMomentum * newHeading;
			Quaternion maxRotateMomentum = Quaternion.Lerp(Quaternion.identity, Quaternion.FromToRotation(currentV, newHeading),
														   m_RotateMomentumFactor);
			m_RotateMomentum = Quaternion.Lerp(m_RotateMomentum, maxRotateMomentum, m_RotateAcceleration);

			Vector3 deltaV = newHeading - currentV;

			return deltaV;
		}

		/// <summary>
		/// Face towards the current velocity heading, instead of that frame's input.
		/// </summary>
		protected override void ResolveRotation(AbstractPlayerController player, Vector3 movement)
		{
			RotateTowardsVelocityVector(player);
		}

		#endregion
	}
}
