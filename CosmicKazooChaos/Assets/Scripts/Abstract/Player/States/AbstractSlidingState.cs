using CosmicKazooChaos.Utils;
using CosmicKazooChaos.Utils.Time;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Player.States
{
	public abstract class AbstractSlidingState : AbstractPlayerState
	{
		[SerializeField] private AbstractStandingState m_StandingState;
		[SerializeField] private AbstractMidairState m_MidairState;

		[SerializeField] [Tweakable("Slide")] private float m_SlideAccelerationModifier = 1.0f;
		[SerializeField] [Tweakable("Slide")] private float m_SlideMaxSpeedModifier = 1.0f;
		[SerializeField] [Tweakable("Slide")] private float m_SlideTransitionSpeed = 1.0f;
		[SerializeField] [Tweakable("Slide")] private float m_Slipperiness = 1.0f;

		[SerializeField] [Tweakable("Slide")] private static float m_SlopeLimit = 40.0f;

		private float m_SlideSmoothing = 1.0f;

		protected override float accelerationMultiplier { get { return m_SlideAccelerationModifier; } }

		protected override float maxSpeedMultiplier { get { return m_SlideMaxSpeedModifier; } }

		#region Override Methods

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (!player.characterLocomotor.isSoftGrounded)
				return m_MidairState;

			if (!ShouldStartSliding(parent) && player.characterLocomotor.velocity.magnitude <= m_SlideTransitionSpeed)
				return m_StandingState;

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			if (CosmicKazooChaos.Concrete.Configuration.Controls.Mapping.InputMapping.jumpInput.GetButtonDown())
			{
				RaycastHit hitInfo = player.characterLocomotor.GroundedCast();
				Jump(hitInfo.normal, player);
				m_SlideSmoothing = 0.0f;
			}
		}

		protected override Vector3 ResolveVelocityDelta(AbstractPlayerController player, Vector3 desiredV, Vector3 currentV)
		{
			RaycastHit hitInfo = player.characterLocomotor.GroundedCast();

			Vector3 slideVector = Vector3.ProjectOnPlane(player.characterLocomotor.gravity * m_Slipperiness + desiredV,
														 hitInfo.normal);
			Vector3 correctedV = Vector3.ProjectOnPlane(currentV, hitInfo.normal);

			Vector3 deltaV = slideVector - correctedV;

			Vector3 slideDelta = Vector3.ClampMagnitude(deltaV, player.acceleration * accelerationMultiplier * GameTime.deltaTime);
			Vector3 correctionDelta = correctedV - currentV;

			return slideDelta + (correctionDelta * m_SlideSmoothing);

			/*
			if (Vector3.Angle(hitInfo.normal, Vector3.up) < player.characterLocomotor.characterController.slopeLimit)
				return;
			 */
		}

		/// <summary>
		/// 	Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnFixedUpdate(MonoBehaviour parent)
		{
			base.OnFixedUpdate(parent);
		}

		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
			StartFreeFloating(parent);
			m_SlideSmoothing = 1;
		}

		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);
			StopFloating(parent);
		}

		#endregion

		#region Static Methods

		/// <summary>
		/// 	Gets the slide vector.
		/// </summary>
		/// <returns>The slide vector.</returns>
		/// <param name="surfaceNormal">Surface normal.</param>
		public static Vector3 GetSlideVector(Vector3 surfaceNormal)
		{
			return Vector3.RotateTowards(surfaceNormal, Vector3.down, Mathf.PI / 2.0f, 1.0f);
		}

		/// <summary>
		/// 	Returns true if the parent is in a position to start sliding.
		/// </summary>
		/// <returns><c>true</c>, if can start sliding, <c>false</c> otherwise.</returns>
		/// <param name="parent">Parent.</param>
		public static bool ShouldStartSliding(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);
			RaycastHit hitInfo = player.characterLocomotor.GroundedCast();

			if (hitInfo.collider == null)
				return false;

			float surfaceAngle = Vector3.Angle(hitInfo.normal, Vector3.up);

			if (surfaceAngle < m_SlopeLimit)
				return false;

			return true;
		}

		#endregion
	}
}
