﻿using CosmicKazooChaos.Concrete.HitBoxes;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Player.States
{
	/// <summary>
	/// 	Abstract attack state.
	/// </summary>
	public abstract class AbstractAttackState : AbstractPlayerState
	{
		public const string CLONE_SUFFIX = "(Clone)";

		[SerializeField] private HitBox m_HitBoxPrefab;
		[SerializeField] private int m_Damage = 2;

		#region Properties

		/// <summary>
		/// 	Gets or sets the hit box prefab.
		/// </summary>
		/// <value>The hit box prefab.</value>
		public HitBox hitBoxPrefab { get { return m_HitBoxPrefab; } set { m_HitBoxPrefab = value; } }

		/// <summary>
		/// 	Gets or sets the damage.
		/// </summary>
		/// <value>The damage.</value>
		public int damage { get { return m_Damage; } set { m_Damage = value; } }

		#endregion

		#region Methods

		/// <summary>
		/// 	Gets the hit box. Instantiates it if it does not exist.
		/// </summary>
		/// <returns>The hit box instance.</returns>
		/// <param name="parent">Parent.</param>
		public HitBox GetHitBox(MonoBehaviour parent)
		{
			Transform child = parent.transform.Find(m_HitBoxPrefab.name + CLONE_SUFFIX);
			if (child != null)
				return child.GetComponent<HitBox>();

			Vector3 position = parent.transform.position + parent.transform.rotation * m_HitBoxPrefab.transform.position;
			Quaternion rotation = parent.transform.rotation * m_HitBoxPrefab.transform.rotation;

			HitBox output = Instantiate(m_HitBoxPrefab, position, rotation) as HitBox;
			output.transform.parent = parent.transform;

			return output;
		}

		#endregion
	}
}
