﻿using CosmicKazooChaos.Concrete.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Player.States.Hitstun
{
	public abstract class AbstractHitstunState : AbstractPlayerState
	{
		private Vector3 m_Source;

		[SerializeField] private float m_HitstunForce = 8.0f;
		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractPlayerState m_TimerState;

		/// <summary>
		/// 	The origin point of the damage, in world coordinates.
		/// </summary>
		/// <value>The damage source coordinates.</value>
		public Vector3 source { get { return m_Source; } set { m_Source = value; } }

		/// <summary>
		/// 	Gets the force of the hitstun.
		/// </summary>
		/// <value>The force of the hitstun.</value>
		public float hitstunForce { get { return m_HitstunForce; } }

		/// <summary>
		/// 	Gets the timer to transition to another state.
		/// </summary>
		/// <value>The state timer.</value>
		protected override Timer stateTimer { get { return m_StateTimer; } }

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractPlayerState timerState { get { return m_TimerState; } }
	}
}
