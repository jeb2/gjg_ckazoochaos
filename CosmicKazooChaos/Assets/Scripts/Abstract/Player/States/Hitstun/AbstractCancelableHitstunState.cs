﻿using UnityEngine;

namespace CosmicKazooChaos.Abstract.Player.States.Hitstun
{
	public class AbstractCancelableHitstunState : AbstractHitstunState
	{
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			if (CosmicKazooChaos.Concrete.Configuration.Controls.Mapping.InputMapping.jumpInput.GetButtonDown())
				return timerState;

			return base.GetNextState(parent);
		}
	}
}
