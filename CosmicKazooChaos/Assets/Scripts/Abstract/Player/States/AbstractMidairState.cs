using CosmicKazooChaos.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Player.States
{
	/// <summary>
	/// 	Abstract midair state.
	/// </summary>
	public abstract class AbstractMidairState : AbstractPlayerState
	{
		[SerializeField] private AbstractStandingState m_StandingState;
		[SerializeField] private AbstractSwimmingState m_SwimmingState;
		[Tweakable("Midair")] [SerializeField] private float m_AccelerationMultiplier = 0.2f;
		[Tweakable("Midair")] [SerializeField] private float m_JumpHeightMultiplier = 1.0f;
		[SerializeField] private float m_JumpCancelDivisor = 2.0f;

		#region Properties

		/// <summary>
		/// 	Gets the acceleration multiplier.
		/// </summary>
		/// <value>The acceleration multiplier.</value>
		protected override float accelerationMultiplier { get { return m_AccelerationMultiplier; } }

		/// <summary>
		/// 	Gets the jump force multiplier.
		/// </summary>
		/// <value>The jump force multiplier.</value>
		protected override float jumpHeightMultiplier { get { return m_JumpHeightMultiplier; } }

		#endregion

		#region Methods

		/// <summary>
		/// 	Handles the air jump.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public void HandleAirJump(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (!CosmicKazooChaos.Concrete.Configuration.Controls.Mapping.InputMapping.jumpInput.GetButtonDown())
				return;

			if (!player.airTracker.canAirJump)
				return;

			Jump(player);

			player.airTracker.canAirJump = false;
		}

		public void HandleJumpCancel(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (CosmicKazooChaos.Concrete.Configuration.Controls.Mapping.InputMapping.jumpInput.GetButton())
				return;

			if (!player.airTracker.canJumpCancel)
				return;

			Vector3 vel = player.characterLocomotor.velocity;

			if (vel.y <= 0.0f)
				return;

			vel.y = vel.y / m_JumpCancelDivisor;
			player.characterLocomotor.velocity = vel;

			player.airTracker.canJumpCancel = false;
		}

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			HandleAirJump(parent);
			HandleJumpCancel(parent);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (player.characterLocomotor.isGrounded)
				return m_StandingState;

			if (player.isInWater)
				return m_SwimmingState;

			return base.GetNextState(parent);
		}

		#endregion
	}
}
