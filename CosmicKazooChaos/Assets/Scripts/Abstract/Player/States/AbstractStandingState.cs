﻿using UnityEngine;

namespace CosmicKazooChaos.Abstract.Player.States
{
	/// <summary>
	/// 	Abstract standing state.
	/// </summary>
	public abstract class AbstractStandingState : AbstractPlayerState
	{
		[SerializeField] private AbstractMidairState m_MidairState;
		[SerializeField] private AbstractSlidingState m_SlidingState;
		[SerializeField] private AbstractSwimmingState m_SwimmingState;

		#region Override Methods

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (!player.characterLocomotor.isSoftGrounded || player.isInWater)
			{
				if (!player.isInWater)
					return m_MidairState;

				//if (player.waterTrigger.AllowFloatState(player.collider, Vector3.down, 1.2f, 2f, 12))
				return m_SwimmingState;
			}

			if (AbstractSlidingState.ShouldStartSliding(parent))
				return m_SlidingState;

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
			AbstractPlayerController player = GetPlayerController(parent);
			player.airTracker.Reset();
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			AbstractPlayerController player = GetPlayerController(parent);
			fixSkinWidthOverlap(player);

			if (CosmicKazooChaos.Concrete.Configuration.Controls.Mapping.InputMapping.jumpInput.GetButtonDown())
				Jump(player);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Due to CharacterController.skinWidth, characters will sometimes appear to be floating up to skinWidth distance above the ground.
		/// To fix this, first we adjust the size and vertical position of the collider to account for the skinWidth.
		/// This makes it so most of the time the character is standing on the ground, but sometimes they will sink in a bit.
		/// Then, we have to call this method to correct the sinkage.
		/// 
		/// See: http://answers.unity3d.com/questions/25334/charactercontrollerskinwidth-and-vertical-displace.html
		/// </summary>
		private void fixSkinWidthOverlap(AbstractPlayerController player)
		{
			RaycastHit hit = player.characterLocomotor.GroundedCast();
			float displacement = player.characterLocomotor.skinWidth - hit.distance;
			if (hit.collider != null && displacement > 0 &&
				displacement > player.characterLocomotor.characterController.contactOffset)
				player.characterLocomotor.characterController.Move(Vector3.up * displacement);
		}

		#endregion
	}
}
