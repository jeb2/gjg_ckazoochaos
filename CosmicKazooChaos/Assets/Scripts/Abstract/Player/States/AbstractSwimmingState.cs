﻿//using CosmicKazooChaos.Abstract.Player.States.Underwater;
using CosmicKazooChaos.Concrete.Utils;
using CosmicKazooChaos.Utils;
using UnityEngine;
using CosmicKazooChaos.Concrete.Forces;
using CosmicKazooChaos.Concrete.HitBoxes;

namespace CosmicKazooChaos.Abstract.Player.States
{
	/// <summary>
	/// 	Abstract swimming state.
	/// </summary>
	public abstract class AbstractSwimmingState : AbstractPlayerState
	{
		[SerializeField] private AbstractStandingState m_StandingState;
		[SerializeField] private AbstractMidairState m_MidairState;

		[Tweakable("Swim")] [SerializeField] private float m_MaxSpeedMultiplier = 0.5f;
		[Tweakable("Swim")] [SerializeField] private float m_JumpHeightMultiplier = 0.5f;
		[Tweakable("Swim")] [SerializeField] private float m_RotateSpeedMultiplier = 0.5f;

		[Tweakable("Swim")] [SerializeField] private float m_DesiredJumpHeight = 10.2f;
		[Tweakable("Swim")] [SerializeField] private float m_OriginalJumpHeightOffset = 0f;
		[Tweakable("Swim")] [SerializeField] private float m_JumpDistanceOffset = 0.8f;
		[Tweakable("Swim")] [SerializeField] private float m_RayCheckJumpOffset = 1f;

		private bool m_HasSwimJumped;
		private bool m_HasSwimmed;

		private float m_OriginalFriction;

		private Vector3 remainingVelocity = Vector3.zero;
		private Vector3 originalVelocity = Vector3.zero;
		private float momentaryInputMagnitude;

		private float m_OriginalJumpHeight;

		#region Properties

		/// <summary>
		/// 	Gets the max speed multiplier.
		/// </summary>
		/// <value>The max speed multiplier.</value>
		protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }

		/// <summary>
		/// 	Gets the jump force multiplier.
		/// </summary>
		/// <value>The jump force multiplier.</value>
		protected override float jumpHeightMultiplier { get { return m_JumpHeightMultiplier; } }

		/// <summary>
		/// 	Gets the rotate speed multiplier.
		/// </summary>
		/// <value>The rotate speed multiplier.</value>
		protected override float rotateSpeedMultiplier { get { return m_RotateSpeedMultiplier; } }

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
			m_HasSwimJumped = false;
			AbstractPlayerController player = GetPlayerController(parent);

			player.airTracker.Reset();
			player.airTracker.canAirJump = false;

			if (m_OriginalJumpHeight != null)
			m_OriginalJumpHeight = player.jumpHeight + m_OriginalJumpHeightOffset;

			player.characterLocomotor.useGravity = false;
		}

		public override void OnExit(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);
			player.characterLocomotor.useGravity = true;
			player.jumpHeight = m_OriginalJumpHeight;
			base.OnExit(parent);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (!player.isInWater)
			{
				if (player.characterLocomotor.isGrounded)
					return m_StandingState;

				return m_MidairState;
			}

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			// If the player tries to swim jump very high by pressing the jump button twice before leaving the water,
			// it shouldn't work. When the player jumps, they can't jump again until the player begins moving downwards.
			if (player.characterLocomotor.velocity.y < 0)
				m_HasSwimJumped = false;

			if (CosmicKazooChaos.Concrete.Configuration.Controls.Mapping.InputMapping.jumpInput.GetButtonDown() && !m_HasSwimJumped)
			{
				Jump(player);
				m_HasSwimJumped = true;
			}

			if (CanJumpUp (player, 1.0f) || CanJumpUp (player, 1.4f) || CanJumpUp (player, 2.2f)) 
			{
				if (player.waterTrigger.interpolateWaterVelocity)
					player.jumpHeight = m_DesiredJumpHeight + 1f;

				if (!player.waterTrigger.interpolateWaterVelocity)
					player.jumpHeight = m_DesiredJumpHeight;
			}

			else
				player.jumpHeight = m_OriginalJumpHeight;
		}

		#endregion

		private bool CanJumpUp(AbstractPlayerController player, float forwardDist)
		{
			RaycastHit hitInfo1;
			RaycastHit hitInfo2;
			
			Vector3[] rayPositions = new Vector3[2];

			rayPositions[0] = new Vector3(player.transform.position.x, player.transform.position.y + player.collider.bounds.size.y + m_RayCheckJumpOffset,
			                              (player.transform.position.z));
			rayPositions[1] = new Vector3(player.transform.position.x, player.transform.position.y - m_RayCheckJumpOffset,
			                              (player.transform.position.z));

			Vector3[] directions = new Vector3[2];
			directions [0] = Vector3.down;
			directions [1] = Vector3.up;

			float distance = player.collider.bounds.size.y + m_JumpDistanceOffset;

			LayerMask layerToIgnore = 1 << 4; //IGNORE WATER LAYER
			layerToIgnore |= 1 << 12; //IGNORE PLAYER LAYER
			//TODO: ADD OTHER LAYERS TO BE IGNORED

			bool canDo1 = Physics.Raycast((rayPositions [0] + (player.transform.forward * forwardDist)), directions[0], out hitInfo1, distance, ~layerToIgnore);
			bool canDo2 = Physics.Raycast((rayPositions [1] + (player.transform.forward * forwardDist)), directions[1], out hitInfo2, distance, ~layerToIgnore);

			if ((canDo1 && canDo2) || canDo1)
				return true;

			return false;
		}
	}
}
