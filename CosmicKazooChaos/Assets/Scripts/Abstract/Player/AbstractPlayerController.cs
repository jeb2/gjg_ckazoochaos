﻿using System.Collections.Generic;
using CosmicKazooChaos.Abstract.Player.States.Hitstun;
using CosmicKazooChaos.API.Damageable;
using CosmicKazooChaos.Concrete;
using CosmicKazooChaos.Concrete.Cameras;
using CosmicKazooChaos.Concrete.Collectables;
using CosmicKazooChaos.Concrete.Configuration.Controls.Mapping;
using CosmicKazooChaos.Concrete.Forces;
using CosmicKazooChaos.Concrete.Messaging;
using CosmicKazooChaos.Concrete.Player;
using CosmicKazooChaos.Concrete.StateMachine;
using CosmicKazooChaos.Concrete.Utils;
using CosmicKazooChaos.Utils;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Extensions;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Player
{
	/// <summary>
	/// 	AbstractPlayerController is the base class for all player controllers.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(CharacterLocomotor))]
	public abstract class AbstractPlayerController : HydraMonoBehaviour, IDamageable
	{
		[SerializeField] private PlayerStateMachine m_StateMachine;

		[SerializeField] private float m_Acceleration;
		[SerializeField] private float m_MaxSpeed;
		[SerializeField] private float m_JumpHeight = 2.0f;
		[SerializeField] private float m_bounceHeight = 1.5f;
		[SerializeField] private float m_RotateSpeed;
		[SerializeField] private float m_RunThreshold;

		[SerializeField] private float m_CollectablePullRadius = 1.5f;

		private CharacterLocomotor m_CachedCharacterLocomotor;
		private List<Renderer> m_CachedChildRenderers;
		private List<Projector> m_CachedChildProjectors;

		private bool m_IsInWater;
		private AerialActionTracker m_AirTracker;
		private CameraController m_PlayerCamera;
		private WaterTrigger m_WaterTrigger;

		private List<Collectable> m_SearchCollectables;

		private static bool m_ShowDebugUI = true;

		#region Properties

		/// <summary>
		/// 	Gets the state machine.
		/// </summary>
		/// <value>The state machine.</value>
		public PlayerStateMachine stateMachine { get { return m_StateMachine; } }

		/// <summary>
		/// 	Gets the character locomotor.
		/// </summary>
		/// <value>The character locomotor.</value>
		public CharacterLocomotor characterLocomotor
		{
			get { return m_CachedCharacterLocomotor ?? (m_CachedCharacterLocomotor = GetComponent<CharacterLocomotor>()); }
		}

		/// <summary>
		/// 	Gets or sets the player camera.
		/// </summary>
		/// <value>The player camera.</value>
		public CameraController playerCamera { get { return m_PlayerCamera; } set { m_PlayerCamera = value; } }

		/// <summary>
		/// 	Gets or sets the water trigger script(Set if the player is colliding with water).
		/// </summary>
		/// <value>The water trigger script.</value>
		public WaterTrigger waterTrigger { get { return m_WaterTrigger; } set { m_WaterTrigger = value; } }

		/// <summary>
		/// 	Gets the air tracker.
		/// </summary>
		/// <value>The air tracker.</value>
		public AerialActionTracker airTracker { get { return m_AirTracker; } }

		/// <summary>
		/// 	Gets or sets the acceleration.
		/// </summary>
		/// <value>The acceleration.</value>
		[Tweakable("Movement")]
		public float acceleration { get { return m_Acceleration; } set { m_Acceleration = value; } }

		/// <summary>
		/// 	Gets or sets the max speed.
		/// </summary>
		/// <value>The max speed.</value>
		[Tweakable("Movement")]
		public float maxSpeed { get { return m_MaxSpeed; } set { m_MaxSpeed = value; } }

		/// <summary>
		/// 	Gets or sets the height of the jump.
		/// </summary>
		/// <value>The height of the jump.</value>
		[Tweakable("Jump")]
		public float jumpHeight { get { return m_JumpHeight; } set { m_JumpHeight = value; } }

		/// <summary>
		/// 	Gets or sets the height of the bounce.
		/// </summary>
		/// <value>The height of the bounce.</value>
		[Tweakable("Bounce")]
		public float bounceHeight { get { return m_bounceHeight; } set { m_bounceHeight = value; } }

		/// <summary>
		/// 	Gets or sets the rotate speed, in degrees.
		/// </summary>
		/// <value>The rotate speed.</value>
		[Tweakable("Movement")]
		public float rotateSpeed { get { return m_RotateSpeed; } set { m_RotateSpeed = value; } }

		/// <summary>
		/// 	Gets or sets a value indicating whether this AbstractPlayerController is in water.
		/// </summary>
		/// <value><c>true</c> if is in water; otherwise, <c>false</c>.</value>
		public bool isInWater { get { return m_IsInWater; } set { m_IsInWater = value; } }

		/// <summary>
		/// A value between 0.0 and 1.0, representing at what magnitude of the input vector should the player start running.
		/// It's mirrored to the (-1.0;1.0) range
		/// This is to aid turning in place.
		/// </summary>
		/// <value>The run threshold.</value>
		[Tweakable("Movement")]
		public float runThreshold { get { return m_RunThreshold; } set { m_RunThreshold = value; } }

		/// <summary>
		/// 	Gets the centroid.
		/// </summary>
		/// <value>The centroid.</value>
		public Vector3 centroid { get { return transform.TransformPoint(characterLocomotor.characterController.center); } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_AirTracker == null)
				m_AirTracker = new AerialActionTracker();

			if (m_SearchCollectables == null)
				m_SearchCollectables = new List<Collectable>();

			if (m_CachedChildRenderers == null)
				m_CachedChildRenderers = new List<Renderer>();

			if (m_CachedChildProjectors == null)
				m_CachedChildProjectors = new List<Projector>();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			m_StateMachine.Update(this);

			if (CosmicKazooChaos.Concrete.Configuration.Controls.Mapping.InputMapping.toggleDebugUI.GetButtonDown())
				m_ShowDebugUI = !m_ShowDebugUI;

			if (CosmicKazooChaos.Concrete.Configuration.Controls.Mapping.InputMapping.switchControllerInput.GetButtonDown())
				CosmicKazooChaos.Concrete.Configuration.Controls.Mapping.InputMapping.SwitchInputJoystick();
		}

		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();

			m_StateMachine.FixedUpdate(this);

			PullCollectables();

			CustomWaterEnabler();
		}

		/// <summary>
		/// 	Called after all Updates have finished.
		/// </summary>
		protected override void LateUpdate()
		{
			base.LateUpdate();

			m_StateMachine.LateUpdate(this);
		}

		/// <summary>
		/// 	OnControllerColliderHit is called when the CharacterController hits a collider while performing a Move.
		/// </summary>
		/// <param name="hit">Hit.</param>
		protected override void OnControllerColliderHit(ControllerColliderHit hit)
		{
			base.OnControllerColliderHit(hit);

			m_StateMachine.OnControllerColliderHit(hit, this);

			if (hit.gameObject.tag == "Mushroom") {
				m_CachedCharacterLocomotor.velocity = hit.transform.TransformDirection (Vector3.forward * Mathf.Clamp (m_CachedCharacterLocomotor.velocity.magnitude * m_bounceHeight, 5, 20));
				hit.gameObject.GetComponent<Animator>().SetTrigger ("Touch");
			}
			else
				return;
		}

		/// <summary>
		/// 	OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);

			if (other.GetComponent<WaterTrigger>() != null)
				m_WaterTrigger = other.GetComponent<WaterTrigger>();
		}

		/// <summary>
		/// 	OnTriggerExit is called when the Collider other has stopped touching the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerExit(Collider other)
		{
			base.OnTriggerExit(other);

			if (other.GetComponent<WaterTrigger>() != null)
				m_WaterTrigger = null;
		}

		/// <summary>
		/// 	OnCollisionEnter is called
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnCollisionEnter(Collision collision)
		{
			print("Now in contact with " + collision.transform.name);

			if (characterLocomotor.rigidy != null)
			{
				characterLocomotor.rigidy.velocity = Vector3.zero;
			}
		}

		private void OnCollisionStay(Collision collision)
		{
			if (characterLocomotor.rigidy != null)
			{
				characterLocomotor.rigidy.velocity = Vector3.zero;
				characterLocomotor.rigidy.velocity = characterLocomotor.rigidy.velocity * 0.0001f;
			}
		}

		private void OnCollisionExit(Collision collision)
		{
			print("No longer in contact with " + collision.transform.name);

			if (characterLocomotor.rigidy != null)
			{
				characterLocomotor.rigidy.velocity = Vector3.zero;
			}
		}

		/// <summary>
		/// 	Called to draw to the GUI.
		/// </summary>
		protected override void OnGUI()
		{
			base.OnGUI();

			if (!m_ShowDebugUI)
				return;

			Vector3 screenPosition = Camera.main.WorldToScreenPoint(centroid);
			GUI.Label(new Rect(screenPosition.x, Screen.height - screenPosition.y, 256, 32), stateMachine.activeState.name);

			Color oldColor = GUI.color;
			Color groundedColor = characterLocomotor.isGrounded ? Color.green : Color.red;
			Color softGroundedColor = characterLocomotor.isSoftGrounded ? Color.green : Color.red;

			GUI.color = groundedColor;
			GUI.Label(new Rect(screenPosition.x, (Screen.height - screenPosition.y) + 16, 256, 32), "isGrounded");

			GUI.color = softGroundedColor;
			GUI.Label(new Rect(screenPosition.x, (Screen.height - screenPosition.y) + 32, 256, 32), "isSoftGrounded");

			GUI.color = oldColor;
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Shows the instance.
		/// </summary>
		public void Show()
		{
			Show(true);
		}

		/// <summary>
		/// 	Hides the instance.
		/// </summary>
		public void Hide()
		{
			Show(false);
		}

		/// <summary>
		/// 	Sets the state of any visual components.
		/// </summary>
		/// <param name="show">If set to <c>true</c> show.</param>
		public void Show(bool show)
		{
			transform.FindAllRecursive(m_CachedChildRenderers);
			transform.FindAllRecursive(m_CachedChildProjectors);

			for (int index = 0; index < m_CachedChildRenderers.Count; index++)
				m_CachedChildRenderers[index].enabled = show;

			for (int index = 0; index < m_CachedChildProjectors.Count; index++)
				m_CachedChildProjectors[index].enabled = show;
		}

		/// <summary>
		/// 	Returns true when the user is using movement input.
		/// </summary>
		/// <returns><c>true</c> if this instance has user input; otherwise, <c>false</c>.</returns>
		public static bool HasUserInput()
		{
			float horizontal = InputMapping.horizontalInput.GetAxis();
			float vertical = InputMapping.verticalInput.GetAxis();

			if (!HydraMathUtils.Approximately(horizontal, 0.0f))
				return true;

			if (!HydraMathUtils.Approximately(vertical, 0.0f))
				return true;

			return false;
		}

		/// <summary>
		/// 	Called when the Damageable gets damaged.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision data.</param>
		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			// Temporary invincibility.
			if (m_StateMachine.activeState is AbstractHitstunState)
				return;

			AbstractHitstunState hitstunState = m_StateMachine.GetHitstunState();
			hitstunState.source = collision.point;

			m_StateMachine.SetActiveState(hitstunState, this);

			Vector3 knockbackDirection = (centroid - collision.point);
			//Keep the horizontal direction, but angle 30° up
			knockbackDirection.y = 0;
			knockbackDirection.Normalize();

			//On flat surfaces, continue going in same direction
			if (knockbackDirection.magnitude < 0.1f)
				knockbackDirection = characterLocomotor.flatVelocity.normalized;

			knockbackDirection.y = 0.5f; //Add a bit of vertical

			//Ignore pre-existing velocity when you get stunned
			characterLocomotor.velocity = knockbackDirection * hitstunState.hitstunForce;

			PlayerMessages.BroadcastOnDamage(this, collision);
		}

		/// <summary>
		/// 	Calculates the jump force necessary for this character to reach a certain jump height.
		/// </summary>
		/// <returns>The necessary force.</returns>
		/// <param name="jumpHeight">Jump height.</param>
		public float JumpForce(float jumpHeight)
		{
			return Mathf.Sqrt(jumpHeight * -characterLocomotor.gravity.y * 2.0f);
		}
		

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Pulls the nearby collectables.
		/// </summary>
		private void PullCollectables()
		{
			m_SearchCollectables.Clear();
			FindCollectables(centroid, m_CollectablePullRadius, m_SearchCollectables);

			for (int index = 0; index < m_SearchCollectables.Count; index++)
				m_SearchCollectables[index].SetPullPoint(centroid);
		}

		/// <summary>
		/// 	Checks if the player should transfer to swimming state or return to original state.
		/// </summary>
		private void CustomWaterEnabler()
		{
			if (m_WaterTrigger != null)
			{
				if (!m_WaterTrigger.AllowFloatState (this.collider, Vector3.up, Mathf.Infinity, 1f))
					m_IsInWater = false;
				
				else
					m_IsInWater = true;
			}

			if (m_WaterTrigger == null)
				m_IsInWater = false;
		}

		/// <summary>
		/// 	Finds the collectables.
		/// </summary>
		/// <param name="origin">Origin.</param>
		/// <param name="radius">Radius.</param>
		/// <param name="output">Output.</param>
		private void FindCollectables(Vector3 origin, float radius, List<Collectable> output)
		{
			Collider[] colliders = Physics.OverlapSphere(origin, radius);

			for (int index = 0; index < colliders.Length; index++)
			{
				Collider collider = colliders[index];

				Collectable collectible = collider.GetComponent<Collectable>();
				if (collectible == null)
					continue;

				output.Add(collectible);
			}
		}

		#endregion
	}
}
