﻿using CosmicKazooChaos.Abstract.StateMachine;

namespace CosmicKazooChaos.Abstract.Props.States
{
	public abstract class AbstractPropState : FiniteState<AbstractPropState> {}
}
