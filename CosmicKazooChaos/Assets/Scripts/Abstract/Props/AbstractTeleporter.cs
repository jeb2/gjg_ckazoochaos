﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Props
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Collider))]
	public abstract class AbstractTeleporter : HydraMonoBehaviour
	{
		public bool active { get; set; }

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			active = true;
			collider.isTrigger = true;
		}

		/// <summary>
		/// 	OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);

			if (!active)
				return;

			CharacterController characterController = other.GetComponent<CharacterController>();
			if (characterController == null)
				return;
            Time.timeScale = 0;
            TransitionSystem.transitionSystem.transition(this, characterController);
			//Teleport(characterController);
		}

		/// <summary>
		/// 	OnTriggerExit is called when the Collider other has stopped touching the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerExit(Collider other)
		{
			base.OnTriggerExit(other);

			CharacterController characterController = other.GetComponent<CharacterController>();
			if (characterController == null)
				return;

			active = true;
		}

		#endregion

		/// <summary>
		/// 	Teleports the character to the destination.
		/// </summary>
		/// <param name="character">Character.</param>
		public abstract void Teleport(CharacterController character);
	}
}
