﻿using CosmicKazooChaos.API.Damageable;
using CosmicKazooChaos.Concrete.Props;
using CosmicKazooChaos.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Props
{
	// So far all this duplicates AbstractPlayerController
	[DisallowMultipleComponent]
	public abstract class AbstractPropController : HydraMonoBehaviour, IDamageable
	{
		[SerializeField] private PropStateMachine m_StateMachine;

		public PropStateMachine stateMachine { get { return m_StateMachine; } }

		protected override void OnEnable()
		{
			base.OnEnable();
		}

		protected override void Update()
		{
			base.Update();
			stateMachine.Update(this);
		}

		protected override void FixedUpdate()
		{
			base.FixedUpdate();
			stateMachine.FixedUpdate(this);
		}

		protected override void LateUpdate()
		{
			base.LateUpdate();
			stateMachine.LateUpdate(this);
		}

		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision) {}
	}
}
