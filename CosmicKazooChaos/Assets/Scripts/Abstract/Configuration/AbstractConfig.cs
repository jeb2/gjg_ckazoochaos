﻿using Hydra.HydraCommon.Abstract;

namespace CosmicKazooChaos.Abstract.Configuration
{
	/// <summary>
	/// 	AbstractConfig is the base class for all AntonCoolpecker configs.
	/// </summary>
	public abstract class AbstractConfig<T> : SingletonHydraScriptableObject<T>
		where T : AbstractConfig<T>
	{
		private const string MODULE_NAME = "CosmicKazooChaos";
		private const string SUBDIR_NAME = "Configuration";

		/// <summary>
		/// 	Gets the name of the module.
		/// </summary>
		/// <value>The name of the module.</value>
		private static string moduleName { get { return MODULE_NAME; } }

		/// <summary>
		/// 	Gets the name of the sub directory.
		/// </summary>
		/// <value>The name of the sub directory.</value>
		private static string subDirectoryName { get { return SUBDIR_NAME; } }
	}
}
