﻿using System;

namespace CosmicKazooChaos.Abstract.StateMachine
{
	/// <summary>
	/// 	StateChangeInfo simply contains the previous and current state after
	/// 	a state change.
	/// </summary>
	public class StateChangeInfo<T> : EventArgs
		where T : FiniteState<T>
	{
		private readonly T m_Previous;
		private readonly T m_Current;

		#region Properties

		/// <summary>
		/// 	Gets the previous state.
		/// </summary>
		/// <value>The previous state.</value>
		public T previous { get { return m_Previous; } }

		/// <summary>
		/// 	Gets the current state.
		/// </summary>
		/// <value>The current state.</value>
		public T current { get { return m_Current; } }

		#endregion

		/// <summary>
		/// 	Initializes a new instance of the StateChangeInfo class.
		/// </summary>
		/// <param name="previous">Previous.</param>
		/// <param name="current">Current.</param>
		public StateChangeInfo(T previous, T current)
		{
			m_Previous = previous;
			m_Current = current;
		}
	}
}
