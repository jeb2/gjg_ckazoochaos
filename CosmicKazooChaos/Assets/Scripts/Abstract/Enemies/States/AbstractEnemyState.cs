﻿using System;
using CosmicKazooChaos.Abstract.StateMachine;
using CosmicKazooChaos.Concrete.Enemies;
using CosmicKazooChaos.Concrete.Enemies.States;
using CosmicKazooChaos.Utils.Time;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Enemies.States
{
	public abstract class AbstractEnemyState : TimableFiniteState<AbstractEnemyState>
	{
		[SerializeField] private bool m_UseRunningSpeed = false;
		[SerializeField] private string m_AnimatorState;
		[SerializeField] private HitstunEnemyState m_HitstunState;

		private GameObject m_CachedPlayer;

		public enum LocomotionMode
		{
			NMAgent,
			Locomotor
		}

		/// <summary>
		/// 	The animator state to use for this EnemyState. (=~ The animation to play)
		/// </summary>
		/// <value>The state of the animator.</value>
		public string animatorState { get { return m_AnimatorState; } }

		/// <summary>
		/// Tells the controller whether to use CharacterLocomotor or NavMeshAgent for movement in the given state
		/// </summary>
		/// <value>The locomotion mode.</value>
		public virtual LocomotionMode locomotionMode { get { return LocomotionMode.NMAgent; } }

		/// <summary>
		/// 	Gets the player.
		/// </summary>
		/// <value>The player.</value>
		public GameObject player
		{
			get
			{
				return (m_CachedPlayer && m_CachedPlayer.activeInHierarchy)
						   ? m_CachedPlayer
						   : m_CachedPlayer = GameObject.FindWithTag("Player");
			}
		}

		/// <summary>
		/// 	Returns the hitstun state associated with this state.
		/// </summary>
		/// <returns>The hitstun state.</returns>
		public virtual HitstunEnemyState GetHitstunState()
		{
			if (m_HitstunState)
				return m_HitstunState;

			throw new NullReferenceException("Hitstun state not set.");
		}

		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
			EnemyController enemy = GetEnemyController(parent);
			enemy.navMeshAgent.speed = GetSpeed(parent);
			enemy.navMeshAgent.acceleration = enemy.acceleration;
			enemy.navMeshAgent.angularSpeed = enemy.rotateSpeed;
			switch (locomotionMode)
			{
				case AbstractEnemyState.LocomotionMode.Locomotor:
					enemy.usingNavMeshAgent = false;
					break;

				case AbstractEnemyState.LocomotionMode.NMAgent:
					enemy.usingNavMeshAgent = !enemy.movesIn3D && enemy.characterLocomotor.isGrounded;
					break;

				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		/// <summary>
		/// 	Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnFixedUpdate(MonoBehaviour parent)
		{
			base.OnFixedUpdate(parent);
			HandleMovement(parent);
		}

		public virtual void HandleMovement(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			switch (locomotionMode)
			{
				case LocomotionMode.Locomotor:
					HandleLocomotorMovement(parent);
					break;
				case LocomotionMode.NMAgent:
					if (enemy.usingNavMeshAgent)
					{
						Vector3 target = GetMotionTarget(parent);
						if ((target - enemy.navMeshAgent.destination).magnitude > enemy.mimimumNMADesintationChange)
							enemy.navMeshAgent.SetDestination(target);
					}
					else
						HandleLocomotorMovement(parent);
					break;

				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
		public void HandleLocomotorMovement(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			Vector3 movement = GetMotion(parent);
			ResolveMovement(enemy, movement);
			ResolveRotation(enemy, movement);
		}

		/// <summary>
		/// 	Gets the movement vector used to reach the current motion target.
		/// </summary>
		/// <returns>The movement vector.</returns>
		/// <param name="parent">Parent.</param>
		private Vector3 GetMotion(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			Vector3 targetpos = GetMotionTarget(parent) - enemy.transform.position;
			if (!enemy.movesIn3D)
				targetpos.y = 0;
			if (targetpos.magnitude < GetSpeed(parent) * GameTime.deltaTime)
				return Vector3.zero;

			Vector3 motion = targetpos.normalized;
			motion = motion * GetSpeed(parent) * GameTime.deltaTime;

			return motion;
		}

		protected virtual void ResolveMovement(EnemyController enemy, Vector3 movement)
		{
			movement /= GameTime.deltaTime;
			Vector3 currentVelocity = enemy.movesIn3D ? enemy.characterLocomotor.velocity : enemy.characterLocomotor.flatVelocity;
			Vector3 deltaVelocity = Vector3.ClampMagnitude(movement - currentVelocity, enemy.acceleration * GameTime.deltaTime);
			enemy.characterLocomotor.velocity += deltaVelocity;
		}

		protected virtual void ResolveRotation(EnemyController enemy, Vector3 movement)
		{
			movement.y = 0.0f;
			RotateTowardsVector(enemy, movement);
		}

		/// <summary>
		/// 	Rotates the enemy towards a certain vector.
		/// </summary>
		/// <param name="enemy">Enemy.</param>
		/// <param name="vector">Vector.</param>
		protected void RotateTowardsVector(EnemyController enemy, Vector3 vector)
		{
			Quaternion lookRotation = Quaternion.LookRotation(enemy.transform.forward);

			if (vector == Vector3.zero)
				return;

			Quaternion targetRotation = Quaternion.LookRotation(vector);
			float delta = enemy.rotateSpeed * GameTime.deltaTime;

			enemy.transform.rotation = Quaternion.RotateTowards(lookRotation, targetRotation, delta);
		}

		/// <summary>
		/// 	Gets the target towards which this enemy is trying to move.
		/// </summary>
		/// <returns>The motion target.</returns>
		/// <param name="parent">Parent.</param>
		protected virtual Vector3 GetMotionTarget(MonoBehaviour parent)
		{
			return parent.transform.position;
		}

		public float GetSpeed(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			return m_UseRunningSpeed ? enemy.runningSpeed : enemy.walkingSpeed;
		}

		public static EnemyController GetEnemyController(MonoBehaviour parent)
		{
			return parent as EnemyController;
		}
	}
}
