﻿using CosmicKazooChaos.Abstract.Enemies.States;
using CosmicKazooChaos.Abstract.StateMachine;
using CosmicKazooChaos.API.Listeners.Enemies;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Listeners.Enemies
{
	[DisallowMultipleComponent]
	public class AbstractEnemyAnimationListener : HydraMonoBehaviour, IEnemyListener
	{
		[SerializeField] private Animator m_AnimatorAsset;
		[SerializeField] private RuntimeAnimatorController m_AnimatorController;

		private Animator m_AnimatorInstance;

		/// <summary>
		/// 	Gets the Animator instance.
		/// </summary>
		/// <value>The Animator instance.</value>
		/// 
		/// TODO: investigate if animator needs to be instantiated dynamically
		/// Until then, just use the animator asset
		public Animator animator { get { return m_AnimatorInstance ?? (m_AnimatorInstance = m_AnimatorAsset); } }

		public virtual void OnStateChange(StateChangeInfo<AbstractEnemyState> changeInfo)
		{
			PlayAnimation(changeInfo.current.animatorState, false);
		}

		public virtual void OnAnimationEvent(AnimationEvent animationEvent) {}

		//TODO: AbstractAnimationListener for stuff shared between AbstractPlayerAnimationListener and AbstractEnemyAnimationListener?
		/// <summary>
		/// 	Plays the animation.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="immediate">If set to <c>true</c> immediate.</param>
		public void PlayAnimation(string name, bool immediate)
		{
			if (string.IsNullOrEmpty(name))
				return;

			if (immediate)
				animator.CrossFade(name, 0.0f, -1, 0.0f);
			else
				animator.CrossFade(name, 0.0f);
		}
	}
}
