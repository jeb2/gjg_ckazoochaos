﻿using CosmicKazooChaos.Abstract.Player;
using CosmicKazooChaos.Abstract.Player.States;
using CosmicKazooChaos.Abstract.StateMachine;
using CosmicKazooChaos.API.Listeners.Player;
using CosmicKazooChaos.Concrete;
using CosmicKazooChaos.Concrete.Messaging;
using CosmicKazooChaos.Concrete.Utils;
using CosmicKazooChaos.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.EventArguments;
using Hydra.HydraCommon.Extensions;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Listeners.Player
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(AbstractPlayerController))]
	public class AbstractPlayerAnimationListener : HydraMonoBehaviour, IPlayerListener
	{
		[SerializeField] private Animator m_AnimatorAsset;
		[SerializeField] private RuntimeAnimatorController m_AnimatorController;

		private Animator m_AnimatorInstance;
		private AbstractPlayerController m_CachedPlayerController;
		private AnimationEventReceiver m_EventReceiver;

		private bool m_Init;

		#region Properties

		/// <summary>
		/// 	Gets the Animator instance.
		/// </summary>
		/// <value>The Animator instance.</value>
		public Animator animator { get { return m_AnimatorInstance; } }

		/// <summary>
		/// 	Gets the player controller.
		/// </summary>
		/// <value>The player controller.</value>
		public AbstractPlayerController player
		{
			get { return m_CachedPlayerController ?? (m_CachedPlayerController = GetComponent<AbstractPlayerController>()); }
		}

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_AnimatorInstance == null)
				InstantiateAnimator();

			Subscribe();
		}

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			Unsubscribe();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (m_Init == false)
			{
				m_Init = true;
				return;
			}

			Vector3 movementVector = player.stateMachine.activeState.GetMovementVector(player);
			//TODO: FIX THIS SECTION
			animator.SetFloat("InputMagnitude", movementVector.magnitude);

			if (player.characterLocomotor.isGrounded)
				animator.SetTrigger("GroundedTrigger");
		}

		/// <summary>
		/// 	Called when the object is destroyed.
		/// </summary>
		protected override void OnDestroy()
		{
			base.OnDestroy();

			DestroyAnimator();
		}

		/// <summary>
		/// 	Called when the player jumps.
		/// </summary>
		public virtual void OnJump()
		{
			PlayAnimation("Jump", true);
		}

		/// <summary>
		/// 	Called when the state changes.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		public virtual void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo)
		{
			PlayAnimation(changeInfo.current.animatorState, false);
		}

		/// <summary>
		/// 	Called when the player is damaged.
		/// </summary>
		/// <param name="collision">The collision info.</param>
		public virtual void OnDamage(CharacterLocomotorCollisionData collision)
		{
			//TODO: there will be a damaged animation
			PlayAnimation("Midair", false);
		}

		/// <summary>
		/// 	Called when we swap to the player.
		/// </summary>
		public virtual void OnSwapEnter()
		{
			PlayAnimation(player.stateMachine.activeState.animatorState, true);
		}

		/// <summary>
		/// 	Called when an animation event is fired.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		public virtual void OnAnimationEvent(AnimationEvent animationEvent)
		{
			if (animationEvent.stringParameter == "Footstep")
				Footstep();
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Plays the animation.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="immediate">If set to <c>true</c> immediate.</param>
		public void PlayAnimation(string name, bool immediate)
		{
			if (string.IsNullOrEmpty(name))
				return;

			// Don't play falling animation if we jumped.
			if (player.stateMachine.activeState is AbstractMidairState && IsPlayingAnimation("Jump"))
				return;

			if (immediate)
				animator.CrossFade(name, 0.0f, -1, 0.0f);
			else
				animator.CrossFade(name, 0.0f);
		}

		/// <summary>
		/// 	Determines whether this instance is playing the specified animation.
		/// </summary>
		/// <returns><c>true</c> if this instance is playing the specified animation; otherwise, <c>false</c>.</returns>
		/// <param name="name">Name.</param>
		public bool IsPlayingAnimation(string name)
		{
			return IsPlayingAnimation(0, name);
		}

		/// <summary>
		/// 	Determines whether this instance is playing the specified animation.
		/// </summary>
		/// <returns><c>true</c> if this instance is playing the specified animation; otherwise, <c>false</c>.</returns>
		/// <param name="layer">Layer.</param>
		/// <param name="name">Name.</param>
		public bool IsPlayingAnimation(int layer, string name)
		{
			AnimatorClipInfo[] clipInfos = animator.GetCurrentAnimatorClipInfo(layer);

			for (int index = 0; index < clipInfos.Length; index++)
			{
				AnimatorClipInfo clipInfo = clipInfos[index];
				if (clipInfo.clip.name == name)
					return true;
			}

			return false;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Makes a footstep.
		/// </summary>
		private void Footstep()
		{
			RaycastHit hit = player.characterLocomotor.GroundedCast();
			if (hit.collider == null)
				return;

			SurfaceInfo.Step(hit.collider.gameObject, gameObject);
		}

		/// <summary>
		/// 	Subscribes to the animation events.
		/// </summary>
		private void Subscribe()
		{
			m_EventReceiver.onAnimationEventCallback += OnAnimationEvent;
		}

		/// <summary>
		/// 	Unsubscribes from the animation events.
		/// </summary>
		private void Unsubscribe()
		{
			m_EventReceiver.onAnimationEventCallback -= OnAnimationEvent;
		}

		/// <summary>
		/// 	Called when an animation event is fired.
		/// </summary>
		private void OnAnimationEvent(object sender, EventArg<AnimationEvent> args)
		{
			PlayerMessages.BroadcastOnAnimationEvent(player, args.data);
		}

		/// <summary>
		/// 	Instantiates the animator.
		/// </summary>
		private void InstantiateAnimator()
		{
			if (!ObjectUtils.LazyInstantiate(m_AnimatorAsset, ref m_AnimatorInstance))
				return;

			m_AnimatorInstance.gameObject.SetLayerRecursive(gameObject.layer);

			m_Init = false;

			m_AnimatorInstance.transform.parent = transform;
			m_AnimatorInstance.runtimeAnimatorController = m_AnimatorController;

			m_EventReceiver = m_AnimatorInstance.gameObject.AddComponent<AnimationEventReceiver>();

			// Stops the animation when the game pauses
			m_AnimatorInstance.gameObject.AddComponent<AnimatorGameTimeSubscriber>();
		}

		/// <summary>
		/// 	Destroys the animator.
		/// </summary>
		private void DestroyAnimator()
		{
			m_AnimatorInstance = ObjectUtils.SafeDestroyGameObject(m_AnimatorInstance);
			m_EventReceiver = ObjectUtils.SafeDestroyGameObject(m_EventReceiver);
		}

		#endregion
	}
}
