﻿using CosmicKazooChaos.Abstract.Audio.Player;
using CosmicKazooChaos.Abstract.Player;
using CosmicKazooChaos.Abstract.Player.States;
using CosmicKazooChaos.Abstract.StateMachine;
using CosmicKazooChaos.API.Listeners.Player;
using CosmicKazooChaos.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.PropertyAttributes;
using Hydra.HydraCommon.Utils.Audio;
using UnityEngine;

namespace CosmicKazooChaos.Abstract.Listeners.Player
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(AbstractPlayerController))]
	public abstract class AbstractPlayerAudioListener : HydraMonoBehaviour, IPlayerListener
	{
		[SerializeField] private int m_LowerSemitone;
		[SerializeField] private int m_UpperSemitone;

		private AbstractPlayerController m_CachedPlayerController;

		#region Properties

		/// <summary>
		/// 	Gets the player controller.
		/// </summary>
		/// <value>The player controller.</value>
		public AbstractPlayerController player
		{
			get { return m_CachedPlayerController ?? (m_CachedPlayerController = GetComponent<AbstractPlayerController>()); }
		}

		/// <summary>
		/// 	Gets the sound effects.
		/// </summary>
		/// <value>The sound effects.</value>
		public abstract AbstractPlayerSFX sfx { get; }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the player jumps.
		/// </summary>
		public virtual void OnJump()
		{
			PlayOneShot(sfx.jump);
		}

		/// <summary>
		/// 	Called when the state changes.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		public virtual void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo) {}

		/// <summary>
		/// 	Called when the player is damaged.
		/// </summary>
		/// <param name="collision">The collision info.</param>
		public virtual void OnDamage(CharacterLocomotorCollisionData collision)
		{
			PlayOneShot(sfx.hurt);
		}

		/// <summary>
		/// 	Called when we swap to the player.
		/// </summary>
		public virtual void OnSwapEnter() {}

		/// <summary>
		/// 	Called when an animation event is fired.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		public virtual void OnAnimationEvent(AnimationEvent animationEvent) {}

		#endregion

		#region Methods

		/// <summary>
		/// 	OneShot a random clip from the array.
		/// </summary>
		/// <param name="extends">Extends.</param>
		/// <param name="clips">Clips.</param>
		/// <param name="volumeScale">Volume scale.</param>
		public void PlayOneShot(SoundEffectAttribute[] soundEffects)
		{
			int index = Random.Range(0, (soundEffects.Length - 1));
			SoundEffectAttribute soundEffect = soundEffects[index];

			float pitch = AudioSourceUtils.IncreaseSemitones(1.0f, Random.Range(m_LowerSemitone, m_UpperSemitone));

			AudioSource source = AudioSourcePool.PlayOneShot(gameObject, soundEffect.audioClip, soundEffect.volumeScale, pitch);

			source.spatialBlend = 1.0f;
		}

		#endregion
	}
}
